<?php

namespace IdeaInYou\SyncToContentful\Model\SizeChart;


use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\SyncToContentful\Api\SyncSizeChartToContentfulInterface;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\ContentfulData;
use Magento\Store\Model\StoreManagerInterface;

class SizeChartToContentful implements SyncSizeChartToContentfulInterface
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var ContentfulData
     */
    private ContentfulData $contentfulData;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;


    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param ContentfulData $contentfulData
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        ContentfulData              $contentfulData,
        StoreManagerInterface       $storeManager
    ) {
        $this->validateContentful = $validateContentful;
        $this->contentfulData = $contentfulData;
        $this->storeManager = $storeManager;
    }

    /**
     * @param $type
     * @param $object
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function addSizeChartToContentful($type, $object)
    {
        $fieldsQuery = 'entity_id';
        $entityId = $object->getId();
        $customEntry = $this->validateContentful->validateContentfulEntries($type, $fieldsQuery, $entityId);
        if (!$customEntry) return;
        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        if ($entries->getTotal() > 0) {
            $entry = $this->contentfulData->getEntry($entries, $environment);
            try {
                $this->checkSizeChartStoreView($entry, $object);
                $entry->update();
            } catch (\Exception $e) {
            }
        } else {
            if (!$this->contentfulData->isCreateEntryEnable($object, $type)) return;
            $entry = new \Contentful\Management\Resource\Entry($type);
            try {
                $this->checkSizeChartStoreView($entry, $object);
                $environment->create($entry);
            } catch (\Exception $e) {
            }
        }
        $entry->publish();
    }

    /**
     * @param $type
     * @param $object
     * @return void
     */
    public function deleteSizeChartFromContentful($type, $object)
    {
        $fieldsQuery = 'entity_id';
        $entityId = $object->getId();
        $customEntry = $this->validateContentful->validateContentfulEntries($type, $fieldsQuery, $entityId);
        if (!$customEntry) return;

        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        if ($entries->getTotal() == 0) {
            return;
        } else {
            $entry = $this->contentfulData->getEntry($entries, $environment);
            $this->contentfulData->deleteEntry($entry);
        }
    }

    /**
     * @param $entry
     * @param $object
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function checkSizeChartStoreView($entry, $object)
    {
        $data = $object->getData();
        if ($object->getStoreViewId()[0] == '0') {
            $stores = $this->contentfulData->getAllStores();
            foreach ($stores as $store) {
                $locale = $this->contentfulData->getStoreLangByCode($store->getCode());
                if (!$locale) continue;
                $this->setEntryFieldsSizeChart($entry, $locale, $data);
            }
        } else {
            $locale = $this->contentfulData->getStoreLangByCode(
                $this->storeManager->getStore($object->getStoreViewId()[0])->getCode()
            );
            if (!$locale) return;
            $this->setEntryFieldsSizeChart($entry, $locale, $data);
        }
    }

    /**
     * @param $entry
     * @param $locale
     * @param $data
     * @return void
     */
    protected function setEntryFieldsSizeChart($entry, $locale, $data)
    {
        $entityId = (int)$data['entity_id'];
        foreach ($this->contentfulData->getAllStores() as $store) {
            $loc = $this->contentfulData->getStoreLangByCode($store->getCode());
            if (!$loc) continue;

            if (!$this->contentfulData->validateLocalesWithContentfulLocales($loc)) continue;
            $entry->setField('entity_id', $loc, $entityId);
        }

        $name = $data['name'];
        $optionId = (int)$data['parameters']['conditions']['1--1']['value'];
        $entry->setField('name', $locale, $name);
        $entry->setField('option_id', $locale, $optionId);
        $entry->setField('jsonData', $locale, $data);
    }
}
