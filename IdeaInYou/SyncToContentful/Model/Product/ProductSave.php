<?php

namespace IdeaInYou\SyncToContentful\Model\Product;

use IdeaInYou\SyncToContentful\Model\Product\ProductsCollection;

class ProductSave
{

    /**
     * @var ProductsCollection
     */
    private ProductsCollection $productsCollection;

    /**
     * @param ProductsCollection $productsCollection
     */
    public function __construct(
        ProductsCollection $productsCollection
    ) {
        $this->productsCollection = $productsCollection;
    }

    /**
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function saveAllEntity($type, $store){
        $this->productsCollection->saveAllProductsToContentful($type, $store);
    }

}
