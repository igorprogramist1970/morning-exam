<?php

namespace IdeaInYou\SyncToContentful\Model\Product;

use IdeaInYou\SyncToContentful\Model\ConnectToContentful\ContentfulData;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product;
use Magento\Catalog\Model\ProductRepository;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\Search\FilterGroup;
use Magento\Framework\Api\FilterBuilder;
use Magento\Catalog\Model\Product\Attribute\Source\Status;
use Magento\Catalog\Model\Product\Visibility;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\DataProductPrepare;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class ProductsCollection
 * @package IdeaInYou\SyncToContentful\Model\Product
 */
class ProductsCollection
{
    public static int $count = 0;
    /**
     * @var ProductRepository
     */
    protected ProductRepository $productRepository;
    /**
     * @var SearchCriteriaInterface
     */
    protected SearchCriteriaInterface $searchCriteria;
    /**
     * @var FilterGroup
     */
    protected FilterGroup $filterGroup;
    /**
     * @var FilterBuilder
     */
    protected FilterBuilder $filterBuilder;
    /**
     * @var Status
     */
    protected Status $productStatus;
    /**
     * @var Visibility
     */
    protected Visibility $productVisibility;
    /**
     * @var DataProductPrepare
     */
    protected DataProductPrepare $helperProduct;

    /**
     * @param ProductRepository $productRepository
     * @param SearchCriteriaInterface $criteria
     * @param FilterGroup $filterGroup
     * @param FilterBuilder $filterBuilder
     * @param Status $productStatus
     * @param Visibility $productVisibility
     * @param DataProductPrepare $helperProduct
     */
    public function __construct(
        ProductRepository $productRepository,
        SearchCriteriaInterface $criteria,
        FilterGroup $filterGroup,
        FilterBuilder $filterBuilder,
        Status $productStatus,
        Visibility $productVisibility,
        DataProductPrepare $helperProduct
    ) {
        $this->productRepository = $productRepository;
        $this->searchCriteria = $criteria;
        $this->filterGroup = $filterGroup;
        $this->filterBuilder = $filterBuilder;
        $this->productStatus = $productStatus;
        $this->productVisibility = $productVisibility;
        $this->helperProduct = $helperProduct;
    }

    /**
     * @throws NoSuchEntityException
     */
    public function saveAllProductsToContentful($type, $store)
    {
        $products = $this->getAllProducts();

        /** @var $product Product */
        foreach ($products as $product) {
            $this->helperProduct->prepareProductContentfulData($product);
            self::$count++;
        }
    }

    /**
     * @return ProductInterface[]
     */
    protected function getAllProducts(): array
    {
        $this->filterGroup->setFilters([
            $this->filterBuilder
                ->setField('status')
                ->setConditionType('in')
                ->setValue($this->productStatus->getVisibleStatusIds())
                ->create(),
            $this->filterBuilder
                ->setField('visibility')
                ->setConditionType('in')
                ->setValue($this->productVisibility->getVisibleInSiteIds())
                ->create(),
        ]);

        $this->searchCriteria->setFilterGroups([$this->filterGroup]);
        $products = $this->productRepository->getList($this->searchCriteria);

        return $products->getItems();
    }

    /**
     * @return void
     */
    public function resaveAllProducts(){
        $products = $this->getAllProducts();
        foreach ($products as $product) {
            $product->save();
            self::$count++;
        }
    }
}
