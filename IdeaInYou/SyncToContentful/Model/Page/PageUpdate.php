<?php

namespace IdeaInYou\SyncToContentful\Model\Page;

use IdeaInYou\SyncToContentful\Model\ConnectToContentful\ContentfulData;
use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\UrlRewrites;

class PageUpdate
{
    const TYPE_PAGE = 'page';
    /**
     * @var ContentfulData
     */
    private ContentfulData $contentfulData;

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var UrlRewrites
     */
    private UrlRewrites $urlRewrites;

    /**
     * @param ContentfulData $contentfulData
     * @param ValidateContentfulInterface $validateContentful
     * @param UrlRewrites $urlRewrites
     */
    public function __construct(
        ContentfulData              $contentfulData,
        ValidateContentfulInterface $validateContentful,
        UrlRewrites                 $urlRewrites
    ) {
        $this->contentfulData = $contentfulData;
        $this->validateContentful = $validateContentful;
        $this->urlRewrites = $urlRewrites;
    }

    /**
     * @param $object
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updatePageEntryContentful($object)
    {
        $contentTypeId = ContentfulData::CONTENT_URLREWRITE_TYPE_ID;
        $entityId = $object->getData('identifier');
        $fieldsQuery = 'urlKey';
        $fieldsQueryType = 'type';
        $customEntry = $this->validateContentful->validateContentfulEntries(
            $contentTypeId,
            $fieldsQuery,
            $entityId,
            $fieldsQueryType,
            self::TYPE_PAGE);
        if (!$customEntry) return;
        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];

        $entry = $this->contentfulData->getEntry($entries, $environment);
        $locale = $this->contentfulData->getStoreLangByCode($this->contentfulData->getStoreCode($object));
        if (!$locale) return;
        if (!empty($entry->getField('entity_id', $locale))) {
            $locales = $this->contentfulData->getAllISOCode();
            $tmp = [];

            foreach ($locales as $item) {
                if (!empty($entry->getField('entity_id', $item))) {
                    $tmp[$item] = $entry->getField('entity_id', $item);
                }
            }
            try {
                //TODO when create all_store entry and then delete it check how it will be correct to do
                $arrayOfEntries = array_unique($tmp);
                if (count($arrayOfEntries) == 1) {
                    $this->urlRewrites->removeUrl($object->getIdentifier());
                } elseif ($object->getStoreId()[0] == 0) {
                    $this->urlRewrites->removeUrl($object->getIdentifier());
                } else {
                    $entry->setField('entity_id', $locale, null);
                    $entry->setField('urlKey', $locale, $object->getIdentifier());
                    $entry->setField('type', $locale, self::TYPE_PAGE);
                    $entry->setField('entity', $locale, null);
                    $entry->setField('jsonData', $locale, null);
                    $entry->update();
                    $entry->publish();
                }
            } catch (\Exception $e) {
            }

        }

    }

}
