<?php

namespace IdeaInYou\SyncToContentful\Model\Page;

use Magento\Cms\Api\PageRepositoryInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\UrlRewrites;

class PageCollection
{
    /**
     * @var PageRepositoryInterface
     */
    private PageRepositoryInterface $pageRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;
    /**
     * @var UrlRewrites
     */
    private UrlRewrites $urlRewrites;

    /**
     * @param PageRepositoryInterface $pageRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param UrlRewrites $urlRewrites
     */
    public function __construct(
        PageRepositoryInterface $pageRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        UrlRewrites $urlRewrites
    ) {
        $this->pageRepository = $pageRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->urlRewrites = $urlRewrites;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function saveAllPagesToContentful($type, $store){

        $pages = $this->getPagesPerStore($store);
        /** @var $page \Magento\Cms\Model\Page */
        foreach ($pages as $page) {
            $page->setStoreId($store->getId());
            $this->urlRewrites->addUrlToContentful($page, $type);
        }
    }

    /**
     * @param $store
     * @return \Magento\Cms\Api\Data\PageInterface[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getPagesPerStore($store){
        return $this->pageRepository->getList($this->getSearchCriteria())->getItems();
    }

    /**
     * @return \Magento\Framework\Api\SearchCriteria
     */
    protected function getSearchCriteria()
    {
        return $this->searchCriteriaBuilder
            ->addFilter('is_active', '1')
            ->create();
    }
}
