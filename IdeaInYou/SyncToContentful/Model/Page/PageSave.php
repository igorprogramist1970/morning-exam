<?php

namespace IdeaInYou\SyncToContentful\Model\Page;

use IdeaInYou\SyncToContentful\Model\Page\PageCollection;

class PageSave
{
    /**
     * @var PageCollection
     */
    private PageCollection $pageCollection;

    /**
     * @param PageCollection $pageCollection
     */
    public function __construct(
        PageCollection $pageCollection
    ) {
        $this->pageCollection = $pageCollection;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function saveAllEntity($type, $store){
        $this->pageCollection->saveAllPagesToContentful($type, $store);
    }

}
