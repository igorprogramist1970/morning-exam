<?php

namespace IdeaInYou\SyncToContentful\Model\Category;

use IdeaInYou\SyncToContentful\Model\Category\CategoriesCollection;

class CategorySave
{
    /**
     * @var CategoriesCollection
     */
    private CategoriesCollection $categoriesCollection;

    /**
     * @param CategoriesCollection $categoriesCollection
     */
    public function __construct(
        CategoriesCollection $categoriesCollection
    ) {
        $this->categoriesCollection = $categoriesCollection;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function saveAllEntity($type, $store){
        $this->categoriesCollection->saveAllCategoriesToContentful($type, $store);
    }

}
