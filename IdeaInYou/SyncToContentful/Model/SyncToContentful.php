<?php

namespace IdeaInYou\SyncToContentful\Model;

use Magento\Store\Model\StoreManagerInterface;
use IdeaInYou\SyncToContentful\Api\SyncToContentfulInterface;

class SyncToContentful implements SyncToContentfulInterface
{
    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var SyncToContentfulInterface[]
     */
    private array $arguments;

    /**
     * @param StoreManagerInterface $storeManager
     * @param SyncToContentfulInterface[] $arguments
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        array                 $arguments = []
    ) {
        $this->arguments = $arguments;
        $this->storeManager = $storeManager;
    }

    /**
     * @return void
     */
    public function syncAllEntity()
    {
        $stores = $this->storeManager->getStores();
        foreach ($stores as $store) {
            $this->syncAllEntityByStoreId($store);
        }
    }

    /**
     * @param $storeId
     * @return void
     */
    public function syncAllEntityByStoreId($storeId)
    {
        foreach ($this->arguments as $key => $argument) {
            $argument->saveAllEntity($key, $storeId);
        }
    }
}
