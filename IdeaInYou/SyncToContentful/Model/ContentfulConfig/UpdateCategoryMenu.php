<?php

namespace IdeaInYou\SyncToContentful\Model\ContentfulConfig;

use IdeaInYou\SyncToContentful\Model\ConnectToContentful\ContentfulData;
use IdeaInYou\SyncToContentful\Block\CustomCategoryCollection;
use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\ResourceModel\Category as ResourceCategory;
use IdeaInYou\SyncToContentful\Block\Adminhtml\Category\TreeCollection;
use Magento\Framework\Registry;

class UpdateCategoryMenu
{
    /**
     * @var CustomCategoryCollection
     */
    private CustomCategoryCollection $collection;

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var ResourceCategory
     */
    private ResourceCategory $resourceCategory;

    /**
     * @var ContentfulData
     */
    private ContentfulData $contentfulData;

    /**
     * @var TreeCollection
     */
    private TreeCollection $treeCollection;

    /**
     * @var Registry
     */
    private Registry $registry;

    /**
     * @param CustomCategoryCollection $collection
     * @param ValidateContentfulInterface $validateContentful
     * @param ResourceCategory $resourceCategory
     * @param ContentfulData $contentfulData
     * @param TreeCollection $treeCollection
     * @param Registry $registry
     */
    public function __construct(
        CustomCategoryCollection    $collection,
        ValidateContentfulInterface $validateContentful,
        ResourceCategory            $resourceCategory,
        ContentfulData              $contentfulData,
        TreeCollection              $treeCollection,
        Registry                    $registry
    ) {
        $this->collection = $collection;
        $this->validateContentful = $validateContentful;
        $this->resourceCategory = $resourceCategory;
        $this->contentfulData = $contentfulData;
        $this->treeCollection = $treeCollection;
        $this->registry = $registry;
    }


    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTree()
    {
        return $this->collection->getTree();
    }


    /**
     * @param $object
     * @return mixed|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updateCategoryTree($object)
    {
        if ($object->getStore()->getId() == 0) {
            $store = $this->contentfulData->getStoreByLanguageCode(
                $this->contentfulData->getDefaultStoreView()
            );
        } else {
            $store = $object->getStore();
        }

        $storeCode = $object->getStore()->getCode() == 'admin'
            ? $this->contentfulData->getDefaultStoreView() : $object->getStore()->getCode();
        $locale = $this->contentfulData->getStoreLangByCode($storeCode);
        if (!$locale) return;

        $contentTypeId = ContentfulData::CONTENT_MENU_TYPE_ID;
        $fieldsQuery = 'code';
        $value = 'top-menu';
        $customEntry = $this->validateContentful->validateContentfulEntries($contentTypeId, $fieldsQuery, $value);
        if ($customEntry) {
            $myTree = $this->treeCollection->getCategoryCollectionPerStore($store);
            $this->registry->register('storeCategoryCollection', $myTree);
            $tree = $this->getTree()[0]['children'];
            $entries = $customEntry['entries'];
            $environment = $customEntry['environment'];
            $entry = $this->contentfulData->getEntry($entries, $environment);
            $entry->setField('jsonData', $locale, $tree);
            $entry->update();
            $entry->publish();
        } else {
            $this->resourceCategory->rollBack();
        }
    }

}
