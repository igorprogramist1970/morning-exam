<?php

namespace IdeaInYou\SyncToContentful\Model\ContentfulConfig;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface as ValidateContentfulInterface;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\ContentfulData;
use Magento\Framework\App\Config\ScopeConfigInterface;
use  Magento\Config\Model\ResourceModel\Config as ResourceConfig;
use Magento\Customer\Model\Session as Session;
use Magento\Framework\App\Config as Config;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Exception\RuntimeException;
use Exception;

class ValidateContentful implements ValidateContentfulInterface
{
    /**
     * @var ContentfulData
     */
    private ContentfulData $contentfulData;

    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var ResourceConfig
     */
    private ResourceConfig $resourceConfig;

    /**
     * @var Session
     */
    private Session $session;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var ManagerInterface
     */
    private ManagerInterface $messageManager;

    /**
     *
     * @param ContentfulData $contentfulData
     * @param ScopeConfigInterface $scopeConfig
     * @param ResourceConfig $resourceConfig
     * @param Session $session
     * @param Config $config
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        ContentfulData       $contentfulData,
        ScopeConfigInterface $scopeConfig,
        ResourceConfig       $resourceConfig,
        Session              $session,
        Config               $config,
        ManagerInterface     $messageManager
    )
    {
        $this->contentfulData = $contentfulData;
        $this->scopeConfig = $scopeConfig;
        $this->resourceConfig = $resourceConfig;
        $this->session = $session;
        $this->config = $config;
        $this->messageManager = $messageManager;
    }

    /**
     * @param $contentTypeId
     * @param $fieldsQuery
     * @param $value
     * @param $fieldsQueryType
     * @param $type
     * @return array|false
     * @throws Exception
     */
    public function validateContentfulEntries(
        $contentTypeId,
        $fieldsQuery,
        $value = null,
        $fieldsQueryType = null,
        $type = null
    )
    {
        if (!$this->isConfigEnable()) {
            return false;
        }
        try {
            $environment = $this->contentfulData->getEnvironment();
            $query = $this->query($contentTypeId, $fieldsQuery, $value, $fieldsQueryType, $type);
            $entries = $environment->getEntries($query);
            return [
                'entries' => $entries,
                'environment' => $environment,
            ];
        } catch (Exception $e) {
            $this->messageManager->addErrorMessage(__("Credentials for Сontentful are not correct"));
            return false;
        }
    }

    /**
     * @param $contentTypeId
     * @param $fieldsQuery
     * @param $value
     * @param $fieldsQueryType
     * @param $type
     * @return \Contentful\Management\Query
     */
    public function query(
        $contentTypeId,
        $fieldsQuery,
        $value,
        $fieldsQueryType = null,
        $type = null
    ): \Contentful\Management\Query
    {
        if ($fieldsQueryType !== null && $type !== null) {
            $query = (new \Contentful\Management\Query())->setContentType($contentTypeId)
                ->where('fields.' . $fieldsQuery, $value)->where('fields.' . $fieldsQueryType, $type);
        } else {
            $query = (new \Contentful\Management\Query())->setContentType($contentTypeId)
                ->where('fields.' . $fieldsQuery, $value);
        }

        return $query;
    }

    /**
     * @return bool
     * @throws RuntimeException
     */
    public function isConfigEnable(): bool
    {
        $this->config->clean();
        if (!$this->isEnable()) return false;
        $newConfigData = $this->session->getValidateData();
        if (!empty($newConfigData)) {
            foreach ($newConfigData['groups'] as $key => $group) {
                $path = $newConfigData['section'] . '/' . $key . '/';
                foreach ($group as $items) {
                    foreach ($items as $index => $item) {
                        $configPath = '';
                        $value = '';
                        if (isset($item['value']) && $item['value'] != '') {
                            $value = $item['value'];
                        } elseif (isset($item['inherit'])) {
                            $value = $item['inherit'];
                        }

                        $configPath = $path . $index;
                        $this->resourceConfig->saveConfig(
                            $configPath,
                            $value,
                            \Magento\Framework\App\ScopeInterface::SCOPE_DEFAULT,
                            0
                        );
                    }
                }
            }
            $this->config->clean();
            $this->session->unsValidateData();
        } elseif ($this->contentfulData->getContentManagementApiKey() == '' ||
            $this->contentfulData->getSpaceId() == '' ||
            $this->contentfulData->getEnvironmentId() == '' ||
            $this->contentfulData->getEnableId() == '' ||
            $this->contentfulData->getStoreViewLocales() == ''
        ) {
            $message = 'Fields cannot be empty in ContentfulSync';
            $this->messageManager->addErrorMessage($message);
            throw new \RuntimeException($message);
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isEnable(): bool
    {
        if ($this->scopeConfig->getValue($this->contentfulData::XML_CONTENTFUL_ENABLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE)) {
            return true;
        }
        return false;
    }

    /**
     * @param $type
     * @return void
     * @throws Exception
     */
    public function validator($type)
    {
        if (!$this->isEnable()) return;
        $this->contentfulData->checkIfLocaleContentfulIsEmpty();

        if ($type == 'category') {
            $fieldsQuery = 'categories';
        } else {
            $fieldsQuery = 'entity_id';
        }

        $valid = $this->validateContentfulEntries($type, $fieldsQuery, 0);
        if (!$valid) {
            $message = __('Credentials for Сontentful are not correct');
            $this->messageManager->addErrorMessage($message);
            throw new \RuntimeException($message);
        }
    }


}
