<?php

namespace IdeaInYou\SyncToContentful\Model\ConnectToContentful\Addition;

use IdeaInYou\ExtensionContentful\Model\Banner;
use IdeaInYou\ExtensionContentful\Model\BannerRepository;
use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\Category;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\ContentfulData;
use IdeaInYou\SyncToContentful\Api\SyncCustomAdditionToContentfulInterface;

class SyncCustomAdditionToContentful implements SyncCustomAdditionToContentfulInterface
{
    public static string $typeOfEntry = '';
    /**
     * @var ValidateContentfulInterface
     */
    protected ValidateContentfulInterface $validateContentful;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var ContentfulData
     */
    private ContentfulData $contentfulData;

    /**
     * @var BannerRepository
     */
    private BannerRepository $bannerRepository;

    /**
     * @var Banner
     */
    protected Banner $banner;

    /**
     * @var Category
     */
    private Category $category;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param StoreManagerInterface $storeManager
     * @param ContentfulData $contentfulData
     * @param BannerRepository $bannerRepository
     * @param Banner $banner
     * @param Category $category
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        StoreManagerInterface       $storeManager,
        ContentfulData              $contentfulData,
        BannerRepository            $bannerRepository,
        Banner                      $banner,
        Category                    $category
    ) {
        $this->validateContentful = $validateContentful;
        $this->storeManager = $storeManager;
        $this->contentfulData = $contentfulData;
        $this->bannerRepository = $bannerRepository;
        $this->banner = $banner;
        $this->category = $category;
    }

    /**
     * @param $type
     * @param $object
     * @return void
     * @throws NoSuchEntityException
     */
    public function prepareSyncContentfulData($type, $object)
    {
        $fieldsQuery = 'entity_id';
        $entityId = $object->getId();
        $customEntry = $this->validateContentful->validateContentfulEntries($type, $fieldsQuery, $entityId);
        if (!$customEntry) return;
        self::$typeOfEntry = $type;
        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        if ($entries->getTotal() > 0) {
            $entry = $this->contentfulData->getEntry($entries, $environment);
            $this->checkStoreView($entry, $object);
            $entry->update();
        } else {
            if (!$this->contentfulData->isCreateEntryEnable($object, $type)) return;
            $entry = new \Contentful\Management\Resource\Entry($type);
            try {
                $this->checkStoreView($entry, $object);
                $environment->create($entry);
            } catch (\Exception $e) {
            }
        }
        $entry->publish();
    }


    /**
     * @param $entry
     * @param $object
     * @return void
     * @throws NoSuchEntityException
     */
    protected function checkStoreView($entry, $object)
    {
        $data = $object->getData();
        if ($object->getStoreId() == '0') {
            $stores = $this->contentfulData->getAllStores();
            foreach ($stores as $store) {
                $locale = $this->contentfulData->getStoreLangByCode($store->getCode());
                if (!$locale) continue;
                $this->setEntryFields($entry, $locale, $data);
            }
        } else {
            $locale = $this->contentfulData->getStoreLangByCode(
                $this->storeManager->getStore($object->getStoreId())->getCode()
            );
            if (!$locale) return;
            $this->setEntryFields($entry, $locale, $data);
        }
    }

    /**
     * @param $data
     * @return array[]
     * @throws NoSuchEntityException
     */
    public function dataBanner($data): array
    {
        $tmp = $this->bannerRepository->getById((int)$data['entity_id'])->getData();
        $c['code'] = $data['code'];
        $j["jsonData"] = [
            "url" => $data['url'],
            "code" => $data['code'],
            "image" => $data['image'],
            "label" => $data['label'],
            "store_id" => [
                $data['store_id']
            ],
            "subtitle" => $data['subtitle'],
            "entity_id" => $data['entity_id'],
            "is_enable" => $data['is_enable'],
            "text_color" => $data['text_color'],
            "description" => $data['description'],
            "update_time" => $tmp['update_time'],
            "title_banner" => $data['title_banner'],
            "creation_time" => $tmp['creation_time'],
            "background_color" => $data['background_color']
        ];
        return [$c, $j];
    }

    /**
     * @param $data
     * @return array
     * @throws NoSuchEntityException
     */
    public function dataForSlider($data): array
    {
        $jsonBody = [];

        /* this variable must be same as max number ("banner_id_6") in table "ideainyou_slider" */
        $i = 1;
        while ($i <= 6) {
            $currentBanner = 'banner_id_' . $i;
            $currentPosition = 'position_' . $i++;
            if (isset($data[$currentBanner]) && $data[$currentBanner] != 0) {
                $pos = [];
                $banner = $this->bannerRepository->getById($data[$currentBanner])->getData();
                $res = $this->dataBanner($banner);
                $pos[$currentPosition] = $data[$currentPosition];
                $tmpResult = array_merge($res[1]['jsonData'], $pos);
                $jsonBody[] = ['code' => $res[0]['code'],
                    'jsonData' => $tmpResult];
            }
        }
        return $jsonBody;
    }

    /**
     * @param $data
     * @return array
     * @throws NoSuchEntityException
     */
    protected function dataForBanner($data): array
    {
        $arr = $this->dataBanner($data);

        return $arr[1]['jsonData'];
    }

    /**
     * @param $data
     * @return array
     */
    protected function dataForCategoryGroup($data): array
    {
        $jsonBody = [];

        /* this variable must be same as max number ("category_id_6") in table "ideainyou_categorygroup" */
        $i = 1;
        while ($i <= 6) {
            $currentCategory = 'category_id_' . $i;
            $currentPosition = 'position_' . $i++;
            if (isset($data[$currentCategory]) && $data[$currentCategory] != 0) {
                $pos = [];
                $category = $this->category->load((int)$data[$currentCategory])->getData();
                $res = $this->dataCategory($category);
                $pos[$currentPosition] = $data[$currentPosition];
                $tmpResult = array_merge($res[1]['jsonData'], $pos);
                $jsonBody[] = ['id' => $res[0]['id'],
                    'jsonData' => $tmpResult];
            }
        }
        return $jsonBody;
    }

    /**
     * @param $category
     * @return array[]
     */
    protected function dataCategory($category): array
    {
        $imageMenu = $category['image_menu'] ?? null;
        $id['id'] = $category['entity_id'];
        $j["jsonData"] = [
            "url" => '/' . $category['category_url'],
            "image" => $imageMenu,
            "title" => $category['name']
        ];
        return [$id, $j];
    }

    /**
     * @param $entry
     * @param $locale
     * @param $data
     * @return void
     */
    protected function setEntryFields($entry, $locale, $data)
    {
        $entityId = (int)$data['entity_id'];
        $isEnable = (bool)$data['is_enable'];
        foreach ($this->contentfulData->getAllStores() as $store) {
            $loc = $this->contentfulData->getStoreLangByCode($store->getCode());
            if (!$loc) continue;

            if (!$this->contentfulData->validateLocalesWithContentfulLocales($loc)) continue;
            $entry->setField('entity_id', $loc, $entityId);
        }
        $entry->setField('code', $locale, $data['code']);
        $entry->setField('visible', $locale, $isEnable);
        $jsonData = [];
        try {
            if (self::$typeOfEntry == 'banner') {
                $jsonData = $this->dataForBanner($data);
            }
            if (self::$typeOfEntry == 'slider') {
                $jsonData = ['items' => $this->dataForSlider($data)];
            }
            if (self::$typeOfEntry == 'categoryGroup') {
                $jsonData = ['items' => $this->dataForCategoryGroup($data)];
            }
        } catch (\Exception $e) {
        }

        $entry->setField('jsonData', $locale, $jsonData);
    }

    /**
     * @param $type
     * @param $object
     * @return void
     */
    public function prepareToDeleteContentfulData($type, $object)
    {
        $fieldsQuery = 'entity_id';
        $entityId = $object->getId();
        $customEntry = $this->validateContentful->validateContentfulEntries($type, $fieldsQuery, $entityId);
        if (!$customEntry) return;

        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        if ($entries->getTotal() == 0) {
            return;
        } else {
            $entry = $this->contentfulData->getEntry($entries, $environment);
            $this->contentfulData->deleteEntry($entry);
        }
    }
}
