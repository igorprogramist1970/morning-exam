<?php

namespace IdeaInYou\SyncToContentful\Model\ConnectToContentful;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Locale\Resolver;
use Magento\Framework\Registry;
use Magento\Store\Model\ScopeInterface;
use Magento\Directory\Api\CountryInformationAcquirerInterface;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Directory\Api\Data\CountryInformationInterface;
use Magento\Store\Model\StoreManagerInterface;
use IdeaInYou\SyncToContentful\Model\GetDataLocal;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class ConfigurationData
 * @package IdeaInYou\SyncToContentful\Model\ConnectToContentful
 */
class ConfigurationData extends ContentfulData
{
    /**
     * @var ScopeConfigInterface
     */
    protected ScopeConfigInterface $scopeConfig;

    /**
     * @var CountryInformationAcquirerInterface
     */
    protected CountryInformationAcquirerInterface $countryInformationAcquirer;

    /**
     * @var DataObjectProcessor
     */
    protected DataObjectProcessor $dataProcessor;

    /**
     *
     */
    const ALLOWED_SECTIONS = [
        'contentfulconfig'
    ];

    /**
     *
     */
    const REWRITE_DATA_BY_PATH = [
        'general/country/default/allow' => 'getCountries'
    ];

    /**
     *
     */
    const XML_PATH_ADDITIONAL_PATCHES = 'contentfulconfigsync/additional_config/config_patches';

    /**
     * @var ValidateContentfulInterface
     */
    protected ValidateContentfulInterface $validateContentful;
    /**
     * @var Resolver
     */
    private Resolver $resolver;
    /**
     * @var Session
     */
    private Session $customerSession;

    /**
     * ConfigurationData constructor.
     * @param Resolver $resolver
     * @param Session $customerSession
     * @param ScopeConfigInterface $scopeConfig
     * @param CountryInformationAcquirerInterface $countryInformationAcquirer
     * @param DataObjectProcessor $dataProcessor
     * @param StoreManagerInterface $storeManager
     * @param ValidateContentfulInterface $validateContentful
     * @param GetDataLocal $getDataLocal
     * @param Registry $registry
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        Resolver                            $resolver,
        Session                             $customerSession,
        ScopeConfigInterface                $scopeConfig,
        CountryInformationAcquirerInterface $countryInformationAcquirer,
        DataObjectProcessor                 $dataProcessor,
        StoreManagerInterface               $storeManager,
        ValidateContentfulInterface         $validateContentful,
        GetDataLocal                        $getDataLocal,
        Registry                            $registry,
        ManagerInterface                    $messageManager
    ) {
        parent::__construct(
            $scopeConfig,
            $storeManager,
            $getDataLocal,
            $registry,
            $messageManager
        );
        $this->scopeConfig = $scopeConfig;
        $this->countryInformationAcquirer = $countryInformationAcquirer;
        $this->dataProcessor = $dataProcessor;
        $this->validateContentful = $validateContentful;
        $this->resolver = $resolver;
        $this->customerSession = $customerSession;
    }


    /**
     * @return string[]
     */
    public function getAllowedSections()
    {
        return self::ALLOWED_SECTIONS;
    }

    /**
     * @return false|string[]
     */
    public function getAdditionalPatches()
    {
        return explode(PHP_EOL, $this->configurationGetValue(self::XML_PATH_ADDITIONAL_PATCHES));
    }

    /**
     * @param $path
     * @return mixed
     */
    public function configurationGetValue($path)
    {
        return $this->scopeConfig->getValue($path, ScopeInterface::SCOPE_STORE);
    }

    /**
     * @param $data
     * @return void
     */
    public function syncSectionFields($data)
    {
        $this->customerSession->setValidateData($data);
        $path = $data['section'];

        foreach ($data['groups'] as $groupName => $groupFields) {
            $path .= '/' . $groupName;
            if ((in_array($data['section'], $this->getAllowedSections()) || in_array($path, $this->getAdditionalPatches()))) {
                $this->addConfigurationToContentful($path, $groupFields);
            } elseif (isset($groupFields['fields'])) {
                foreach ($groupFields['fields'] as $fieldName => $fieldValue) {
                    $path .= '/' . $fieldName;
                    if (
                        (isset($fieldValue['value']) && $fieldValue['value'])
                        &&
                        (in_array($data['section'], $this->getAllowedSections()) || in_array($path, $this->getAdditionalPatches()))
                    ) {
                        $this->addConfigurationToContentful($path, $fieldValue);
                    } elseif (!isset($fieldValue['value']) || !$fieldValue['value']) {
                        $this->deleteThisPathIfExist($path);
                    }
                }
            }
        }
    }

    /**
     * @param $path
     * @return void
     */
    public function deleteThisPathIfExist($path)
    {
        $contentTypeId = self::CONTENT_CONFIGURATION_TYPE_ID;
        $fieldsQuery = 'path';
        $customEntry = $this->validateContentful->validateContentfulEntries($contentTypeId, $fieldsQuery, $path);
        if (!$customEntry) return;

        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        if ($entries->getTotal()) {
            $entry = $this->getEntry($entries, $environment);
            $this->deleteEntry($entry);
        }
    }

    /**
     * @param $path
     * @param $data
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function addConfigurationToContentful($path, $data)
    {
        $contentTypeId = self::CONTENT_CONFIGURATION_TYPE_ID;
        $fieldsQuery = 'path';
        $customEntry = $this->validateContentful->validateContentfulEntries($contentTypeId, $fieldsQuery, $path);
        if (!$customEntry) return;

        $data = $this->removeEmptyFields($data);
        if (isset(self::REWRITE_DATA_BY_PATH[$path])) {
            $data = $this->{self::REWRITE_DATA_BY_PATH[$path]}();
        }
        if (!$data) {
            $this->deleteThisPathIfExist($path);
            return;
        }

        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        if ($entries->getTotal() > 0) {
            $entry = $this->getEntry($entries, $environment);
            $this->setConfigurationEntryField($entry, $data, $path);
            $entry->update();
        } else {
            try {
                if (!$this->isCreateEntryEnable($data)) return;
                $entry = new \Contentful\Management\Resource\Entry($contentTypeId);
                $this->setConfigurationEntryField($entry, $data, $path);
                $environment->create($entry);
            } catch (\Exception $e) {
            }

        }
        $entry->publish();
    }

    /**
     * @param $entry
     * @param $data
     * @param $path
     * @return void
     */
    public function setConfigurationEntryField($entry, $data, $path)
    {
        $currentLocaleCode = $this->resolver->getLocale();
        $locale = $this->getStoreLangByCode($currentLocaleCode);

        if (!$locale) return;

        $entry->setField('path', $locale, $path);
        $entry->setField('jsonData', $locale, $data);
    }

    /**
     * @param $data
     * @return false
     */
    public function removeEmptyFields($data)
    {
        if (!isset($data['fields'])) return $data;

        foreach ($data['fields'] as $fieldName => $fieldValue) {
            if (!$fieldValue['value']) {
                unset($data['fields'][$fieldName]);
            }
        }

        if (empty($data['fields'])) return false;

        return $data;
    }

    /**
     * @return array
     */
    public function getCountries()
    {
        $countries = $this->countryInformationAcquirer->getCountriesInfo();
        $output = [];

        foreach ($countries as $country) {
            $output[] = $this->dataProcessor->buildOutputDataArray($country,
                CountryInformationInterface::class);
        }

        return $output;
    }
}
