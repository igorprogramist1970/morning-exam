<?php

namespace IdeaInYou\SyncToContentful\Model\ConnectToContentful;

use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\SyncToContentful\Model\GetDataLocal;
use Magento\Catalog\Model\ResourceModel\Product as ResourceProduct;


/**
 * Class DataProductPrepare
 * @package IdeaInYou\SyncToContentful\Model\ConnectToContentful
 */
class DataProductPrepare extends ContentfulData
{
    /**
     * @var ProductRepositoryInterface
     */
    protected ProductRepositoryInterface $productRepository;

    /**
     * @var UrlRewrites
     */
    protected UrlRewrites $urlRewrites;

    /**
     * @var CategoryRepositoryInterface
     */
    protected CategoryRepositoryInterface $categoryRepository;

    /**
     * @var ValidateContentfulInterface
     */
    protected ValidateContentfulInterface $validateContentful;

    /**
     * @var ResourceProduct
     */
    private ResourceProduct $resourceProduct;
    /**
     * @var Configurable
     */
    private Configurable $configurable;

    /**
     * @param ProductRepositoryInterface $productRepository
     * @param UrlRewrites $urlRewrites
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param CategoryRepositoryInterface $categoryRepository
     * @param ValidateContentfulInterface $validateContentful
     * @param GetDataLocal $getDataLocal
     * @param ResourceProduct $resourceProduct
     * @param Registry $registry
     * @param ManagerInterface $messageManager
     * @param Configurable $configurable
     */
    public function __construct(
        ProductRepositoryInterface  $productRepository,
        UrlRewrites                 $urlRewrites,
        ScopeConfigInterface        $scopeConfig,
        StoreManagerInterface       $storeManager,
        CategoryRepositoryInterface $categoryRepository,
        ValidateContentfulInterface $validateContentful,
        GetDataLocal                $getDataLocal,
        ResourceProduct             $resourceProduct,
        Registry                    $registry,
        ManagerInterface            $messageManager,
        Configurable                $configurable
    ) {
        parent::__construct(
            $scopeConfig,
            $storeManager,
            $getDataLocal,
            $registry,
            $messageManager
        );
        $this->productRepository = $productRepository;
        $this->urlRewrites = $urlRewrites;
        $this->categoryRepository = $categoryRepository;
        $this->validateContentful = $validateContentful;
        $this->resourceProduct = $resourceProduct;
        $this->configurable = $configurable;
    }

    /**
     * @param $object
     * @return void
     * @throws NoSuchEntityException
     */
    public function prepareProductContentfulData($object)
    {
        $websites = $object->getWebsiteIds();
        foreach ($websites as $websiteId) {
            $objectLang = $object->getCollection()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('entity_id', $object->getId())
                ->addStoreFilter($websiteId)
                ->getFirstItem();
            if (!$this->isCreateEntryEnable($objectLang)) continue;
            $this->syncProduct($objectLang);
        }
    }

    /**
     * @param $object
     * @return void
     * @throws NoSuchEntityException
     */
    protected function syncProduct($object)
    {
        $contentTypeId = self::CONTENT_PRODUCT_TYPE_ID;
        $parentId = $this->getParentId((int)$object->getEntityId());
        $fieldsQuery = 'entity_id';
        $value = $parentId ?: (int)$object->getData()['entity_id'];
        $customEntry = $this->validateContentful->validateContentfulEntries($contentTypeId, $fieldsQuery, $value);
        if (!$customEntry) {
            $this->resourceProduct->rollBack();
            return;
        }
        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];

        if ($parentId && $entries->getTotal() > 0) {
            $object = $this->productRepository->getById($parentId);
        }
        if ($entries->getTotal() > 0) {
            $entry = $this->getEntry($entries, $environment);
            $flagForSimple = $object->getFlagForSimpleProduct() ?? null;

            if ($flagForSimple == 1 && $object->getTypeId() == 'simple') {
                $this->setProductEntryField($entry, $object);
                $entry->update();
                $object->getStatus() == 1 ? $entry->publish() : $entry->unpublish();
            } elseif ($object->getTypeId() == "configurable" || $object->getTypeId() == "bundle") {
                $this->setProductEntryField($entry, $object);
                $entry->update();
                $object->getStatus() == 1 ? $entry->publish() : $entry->unpublish();
            } else {
                $this->deleteProduct($object);
            }
        } else {
            if (!$this->isCreateEntryEnable($object)) return;
            if ($object->getFlagForSimpleProduct() == 1 && $object->getTypeId() == 'simple') {
                $entry = new \Contentful\Management\Resource\Entry($contentTypeId);
                $this->setProductEntryField($entry, $object);
                $environment->create($entry);
                if ($object->getStatus() == 1) {
                    $entry->publish();
                }
            } elseif ($object->getTypeId() == "configurable" || $object->getTypeId() == "bundle") {
                $entry = new \Contentful\Management\Resource\Entry($contentTypeId);
                $this->setProductEntryField($entry, $object);
                $environment->create($entry);
                if ($object->getStatus() == 1) {
                    $entry->publish();
                }
            }
        }
        $this->urlRewrites->addUrlToContentful($object, 'product');
    }

    /**
     * @param $entry
     * @param $object
     * @return void
     * @throws NoSuchEntityException
     */
    protected function setProductEntryField($entry, $object)
    {
        $locale = $this->getStoreLangByCode($object->getStore()->getCode());

        if (!$locale) return;

        $entityId = (int)$object->getEntityId();
        $isEnable = (bool)$object->getStatus();
        $title = $object->getName();
        $sku = $object->getSku();
        $categories = $object->getCategoryIds() ?? ["Don't consist categories"];
        $price = (float)$object->getPrice();

        if ($object->getCategoryIds()) {
            $lastCategory = $object->getCategoryIds()[count($object->getCategoryIds()) - 1];
            $object['category'] = $this->getCategoryById($lastCategory, $object->getStore()->getId());
        }

        if ($object->getTypeId() == "configurable") {
            $object['configurable_attributes_data'] = $object->getTypeInstance()
                ->getConfigurableAttributesAsArray($object);

            $tags = $object->getTypeInstance(true)->getConfigurableAttributes($object)->getItems();
            $tagCode = [];

            if ($tags) {
                foreach ($tags as $tag) {
                    $tagName = $tag->getProductAttribute()->getAttributeCode();

                    $values = $tag->getData('options');
                    foreach ($values as $value) {
                        $defaultStoreId = $this->getStoreByLanguageCode(
                            $this->getDefaultStoreView()
                        )->getId();
                        $valueTag = $tag->getProductAttribute()->setStoreId($defaultStoreId)->getSource()
                            ->getOptionText($value['value_index']);
                        $tagCode[] = strtolower($tagName) . '-' . str_replace(' ', '_', strtolower($valueTag));
                    }
                }
            }
        } elseif ($object->getTypeId() == "simple" || $object->getTypeId() == "bundle") {
            $tagCode = [];
        }

        $created = date("c", strtotime($object->getData('created_at')));
        $data = $this->reformatData($object);
        $this->productRepository->cleanCache();
        $configurableProduct = $this->productRepository->getById($entityId);
        if ($object->getTypeId() == "configurable") {
            $childIds = $configurableProduct->getExtensionAttributes()->getConfigurableProductLinks() ?? [];
            $childIds = array_values($childIds);
        } elseif ($object->getTypeId() == "bundle") {
            $childOptions = $configurableProduct->getTypeInstance()->getChildrenIds($object->getId(), false) ?? [];
            $childIds = [];
            foreach ($childOptions as $childOption) {
                foreach ($childOption as $child) {
                    $childIds[] = $child;
                }
            }
        } elseif ($object->getTypeId() == "simple") {
            $childIds = [];
        }

        if (isset($object->getData()['related_products'])) {
            $relate = $object->getData()['related_products'];

            $tmp = [];
            foreach ($relate as $item) {
                $product = $this->productRepository->getById($item->getId());

                if ($product->getTypeId() == 'configurable') {
                    $product['configurable_attributes_data'] = $product->getTypeInstance()
                        ->getConfigurableAttributesAsArray($product);
                    $relatedChildrenProducts = $product->getTypeInstance()->getUsedProducts($product);
                    $relatedChildData = [];
                    foreach ($relatedChildrenProducts as $childrenProduct) {
                        $relatedChildData[] = $childrenProduct->getData();
                    }
                    $product['children_products'] = $relatedChildData;
                }
                $tmp[] = [
                    'jsonData' => $this->reformatData($product),
                ];
            }
            $data['related_products'] = $tmp;
        }

        if (isset($object->getData()['cross_sell_products'])) {
            $cross = $object->getData()['cross_sell_products'];
            $tmp = [];
            foreach ($cross as $item) {
                $product = $this->productRepository->getById($item->getId());

                if ($product->getTypeId() == 'configurable') {
                    $product['configurable_attributes_data'] = $product->getTypeInstance()
                        ->getConfigurableAttributesAsArray($product);
                    $crossChildrenProducts = $product->getTypeInstance()->getUsedProducts($product);
                    $crossChildData = [];
                    foreach ($crossChildrenProducts as $childrenProduct) {
                        $crossChildData[] = $childrenProduct->getData();
                    }
                    $product['children_products'] = $crossChildData;
                }
                $tmp[] = [
                    'jsonData' => $this->reformatData($product),
                ];
            }
            $data['cross_sell_products'] = $tmp;
        }

        if ($object->getTypeId() == "configurable" || $object->getTypeId() == "configurable") {
            $images = [];
            $childrenProducts = $object->getTypeInstance()->getUsedProducts($object);

            foreach ($childrenProducts as $childrenProduct) {
                if (isset($childrenProduct->getData()['media_gallery'])) {
                    $images[] = $childrenProduct->getData()['media_gallery']['images'];
                }
                $data['children_products'][] = $childrenProduct->getData();
            }

            if (!isset($data['media_gallery'])) {
                $data['media_gallery']['images'] = $images;
            }
        }

        if (isset($data['description'])) {
            $data['description'] = html_entity_decode($data['description']);
        }

        foreach ($this->getAllStores() as $store) {
            $loc = $this->getStoreLangByCode($store->getCode());
            if (!$loc) continue;
            $entry->setField('entity_id', $loc, $entityId);
        }
        $entry->setField('title', $locale, $title);
        $entry->setField('visible', $locale, $isEnable);
        $entry->setField('jsonData', $locale, $data);
        $entry->setField('sku', $locale, $sku);
        $entry->setField('categories', $locale, $categories);
        $entry->setField('tags', $locale, $tagCode);
        $entry->setField('created', $locale, $created);
        $entry->setField('childIds', $locale, $childIds);
        if ($object->getTypeId() == "configurable") {
            $entry->setField('price', $locale, (float)$object->getPriceInfo()->getPrice('regular_price')
                ->getMaxRegularAmount()->getValue());
            $entry->setField('min_price', $locale, (float)$object->getPriceInfo()->getPrice('regular_price')
                ->getMinRegularAmount()->getValue());
            $entry->setField('max_price', $locale, (float)$object->getPriceInfo()->getPrice('regular_price')
                ->getMaxRegularAmount()->getValue());
        } elseif ($object->getTypeId() == "bundle") {
            $entry->setField('price', $locale, (float)$object->getPriceInfo()->getPrice('regular_price')
                ->getValue());
        } else {
            $entry->setField('min_price', $locale, $price);
            $entry->setField('max_price', $locale, $price);
            $entry->setField('price', $locale, $price);
        }
    }

    /**
     * @param $object
     * @return array
     */
    protected function reformatData($object)
    {
        $data = $object->getData();
        $data = $this->unsetNotNecessaryData($data);
        if (isset($data['url_key'])) {
            $data['url_key'] = '/' . $data['url_key'];
        }

        if (isset($data['configurable_attributes_data'])) {
            foreach ($data['configurable_attributes_data'] as $key => $attribute) {
                if (!isset($attribute['code']) && isset($attribute['attribute_code'])) {
                    $data['configurable_attributes_data'][$key]['code'] = $attribute['attribute_code'];
                }
                foreach ($attribute['values'] as $attributeKey => $value) {
                    $data['configurable_attributes_data'][$key]['values'][$attributeKey]['value'] = $object->getResource()
                        ->getAttribute($data['configurable_attributes_data'][$key]['code'])
                        ->getSource()
                        ->getOptionText($value['value_index']);
                }
            }
        }

        return $data;
    }

    /**
     * @param $data
     * @return mixed
     */
    protected function unsetNotNecessaryData($data)
    {
        if (isset($data['_cache_instance_product_set_attributes'])) {
            unset($data['_cache_instance_product_set_attributes']);
        }

        if (isset($data['_cache_instance_configurable_attributes'])) {
            unset($data['_cache_instance_configurable_attributes']);
        }

        if (isset($data['_cache_instance_used_product_attributes'])) {
            unset($data['_cache_instance_used_product_attributes']);
        }

        if (isset($data['_cache_instance_used_product_attribute_ids'])) {
            unset($data['_cache_instance_used_product_attribute_ids']);
        }

        if (isset($data['page_layout'])) {
            unset($data['page_layout']);
        }

        if (isset($data['climate'])) {
            unset($data['climate']);
        }

        if (isset($data['status'])) {
            unset($data['status']);
        }

        if (isset($data['new'])) {
            unset($data['new']);
        }

        if (isset($data['pattern'])) {
            unset($data['pattern']);
        }

        if (isset($data['_edit_mode'])) {
            unset($data['_edit_mode']);
        }

        if (isset($data['visibility'])) {
            unset($data['visibility']);
        }

        if (isset($data['small_image'])) {
            unset($data['small_image']);
        }

        if (isset($data['thumbnail'])) {
            unset($data['thumbnail']);
        }

        if (isset($data['material'])) {
            unset($data['material']);
        }

        return $data;
    }

    /**
     * for simple product of configurable product
     * @param $childId
     * @return false|mixed
     */
    public function getParentId($childId)
    {
        $product = $this->configurable->getParentIdsByChild($childId);
        if (isset($product[0])) {
            return $product[0];
        }
        return false;
    }

    /**
     * @param $object
     * @return void
     * @throws NoSuchEntityException
     */
    public function deleteProductFromContentful($object)
    {
        $contentTypeId = self::CONTENT_PRODUCT_TYPE_ID;
        $fieldsQuery = 'entity_id';
        $entityId = (int)$object->getData()['entity_id'];
        $customEntry = $this->validateContentful->validateContentfulEntries($contentTypeId, $fieldsQuery, $entityId);
        if (!$customEntry) {
            $this->resourceProduct->rollBack();
            return;
        }

        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        $defaultIsoCode = $this->getDefaultISOCode();
        $url = '';
        if ($entries->getTotal()) {
            $entry = $this->getEntry($entries, $environment);
            if (!empty($entry->getField('jsonData', $defaultIsoCode))) {
                $url = ltrim($entry->getField('jsonData', $defaultIsoCode)['url_key'], '/');
            } else {
                $tmp = $this->getAllISOCode();
                foreach ($tmp as $item) {
                    if ($item == $defaultIsoCode) continue;
                    if (!empty($item)) {
                        $url = ltrim($entry->getField('jsonData', $item)['url_key'], '/');
                    }
                }
            }

            $this->deleteEntry($entry);
        }
        if (empty($url)) return;
        $this->urlRewrites->removeUrl($url);
    }

    /**
     * @param int $id
     * @param null $storeId
     * @return array
     * @throws NoSuchEntityException
     */
    protected function getCategoryById($id, $storeId = null)
    {
        $categoryInstance = $this->categoryRepository->get($id, $storeId);
        return [
            'name' => $categoryInstance->getName(),
            'link' => $categoryInstance->getUrlKey()
        ];
    }
}
