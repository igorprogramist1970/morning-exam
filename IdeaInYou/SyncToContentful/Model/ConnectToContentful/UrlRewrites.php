<?php

namespace IdeaInYou\SyncToContentful\Model\ConnectToContentful;


use Magento\Catalog\Model\ResourceModel\Product\Collection;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory;
use Magento\CatalogInventory\Helper\Stock;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Registry;
use Magento\Framework\Webapi\Rest\Request;
use Magento\GraphQl\Model\Query\ContextInterface;
use Magento\CatalogGraphQl\Model\Resolver\Products\Query\ProductQueryInterface;
use IdeaInYou\SyncToContentful\Model\GetFiltersLayerNavigation;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\ProductFactory;
use IdeaInYou\SyncToContentful\Model\GetDataLocal;
use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\Product;

/**
 * Class UrlRewrites
 * @package IdeaInYou\SyncToContentful\Model\ConnectToContentful
 */
class UrlRewrites extends ContentfulData
{
    /**
     * @var \Magento\Framework\Webapi\Rest\Request
     */
    private Request $request;
    /**
     * @var ResolveInfo
     */
    private ResolveInfo $info;
    /**
     * @var ContextInterface
     */
    private ContextInterface $context;
    /**
     * @var ProductQueryInterface
     */
    private ProductQueryInterface $query;
    /**
     * @var ProductQueryInterface
     */
    private ProductQueryInterface $searchQuery;
    /**
     * @var GetFiltersLayerNavigation
     */
    private GetFiltersLayerNavigation $filtersLayerNavigation;
    /**
     * @var Collection
     */
    private Collection $collection;
    /**
     * @var Stock
     */
    private Stock $stockFilter;
    /**
     * @var ProductFactory
     */
    private ProductFactory $factory;
    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;
    /**
     * @var ValidateContentfulInterface
     */
    protected ValidateContentfulInterface $validateContentful;
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $productFactory;

    /**
     * @var Product
     */
    private Product $modelProduct;


    /**
     * @param Request $request
     * @param ProductQueryInterface $searchQuery
     * @param GetFiltersLayerNavigation $filtersLayerNavigation
     * @param CollectionFactory $productFactory
     * @param Stock $stockFilter
     * @param ScopeConfigInterface $scopeConfig
     * @param StoreManagerInterface $storeManager
     * @param ProductFactory $factory
     * @param GetDataLocal $getDataLocal
     * @param ValidateContentfulInterface $validateContentful
     * @param Registry $registry
     * @param ManagerInterface $messageManager
     * @param Product $modelProduct
     */
    public function __construct(
        Request                     $request,
        ProductQueryInterface       $searchQuery,
        GetFiltersLayerNavigation   $filtersLayerNavigation,
        CollectionFactory           $productFactory,
        Stock                       $stockFilter,
        ScopeConfigInterface        $scopeConfig,
        StoreManagerInterface       $storeManager,
        ProductFactory              $factory,
        GetDataLocal                $getDataLocal,
        ValidateContentfulInterface $validateContentful,
        Registry                    $registry,
        ManagerInterface            $messageManager,
        Product                     $modelProduct
    ) {
        parent::__construct(
            $scopeConfig,
            $storeManager,
            $getDataLocal,
            $registry,
            $messageManager
        );
        $this->request = $request;
        $this->searchQuery = $searchQuery;
        $this->filtersLayerNavigation = $filtersLayerNavigation;
        $this->productFactory = $productFactory;
        $this->stockFilter = $stockFilter;
        $this->factory = $factory;
        $this->storeManager = $storeManager;
        $this->validateContentful = $validateContentful;
        $this->modelProduct = $modelProduct;
    }

    /**
     * @param $object
     * @param $type
     * @throws NoSuchEntityException
     */
    public function addUrlToContentful($object, $type)
    {
        if ($type != "page" && $object->getStore()->getId() == 0) return;

        if ($type == "page") {
            $entityId = $object->getData('identifier');
            $fieldsQuery = 'urlKey';
        } else {
            $entityId = (int)$object->getData()['entity_id'];
            $fieldsQuery = 'entity_id';
        }
        $contentTypeId = self::CONTENT_URLREWRITE_TYPE_ID;

        $fieldsQueryType = 'type';
        $customEntry = $this->validateContentful->validateContentfulEntries(
            $contentTypeId,
            $fieldsQuery,
            $entityId,
            $fieldsQueryType,
            $type);

        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        if ($entries->getTotal() > 0) {
            $entry = $this->getEntry($entries, $environment);
            $this->setUrlEntryField($entry, $object, $type, $entries->getTotal());
            $entry->update();
        } else {
            //todo логика для управления созданием сущности если сторвью отсутствует в $storeLocales
            if (!$this->isCreateEntryEnable($object, $type)) return;
            $entry = new \Contentful\Management\Resource\Entry($contentTypeId);
            $this->setUrlEntryField($entry, $object, $type, $entries->getTotal());
            $environment->create($entry);
        }
        $entry->publish();

        if ($object->getTypeId() == "configurable" && $type == "product") {
            $childProducts = $object->getTypeInstance()->getUsedProducts($object);
            foreach ($childProducts as $childProduct) {
                $entityId = $childProduct->getData("entity_id");
                $query = $this->validateContentful->query(
                    $contentTypeId,
                    $fieldsQuery,
                    $entityId,
                    $fieldsQueryType,
                    $type);
                $entries = $environment->getEntries($query);

                if ($entries->getTotal() > 0) {
                    $entry = $this->getEntry($entries, $environment);
                    $this->setUrlEntryField($entry, $childProduct, $type, $entries->getTotal());
                    $entry->update();
                } else {
                    if (!$this->isCreateEntryEnable($object, $type)) return;
                    $entry = new \Contentful\Management\Resource\Entry($contentTypeId);
                    $this->setUrlEntryField($entry, $childProduct, $type, $entries->getTotal());
                    $environment->create($entry);
                }
                $entry->publish();
            }
        }
    }


    /**
     * @param $object
     */
    public function addUrlToContentfulCategories($object)
    {
        $categoryId = $object['entity_id'];
        $contentTypeId = self::CONTENT_CATEGORY_TYPE_ID;
        $fieldsQuery = 'categories';
        $customEntry = $this->validateContentful->validateContentfulEntries($contentTypeId, $fieldsQuery, $categoryId);

        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        if ($entries->getTotal() > 0) {
            $entry = $this->getEntry($entries, $environment);
            $this->setCategoryEntryField($entry, $object);
            $entry->update();
        } else {
            $entry = new \Contentful\Management\Resource\Entry($contentTypeId);
            $this->setCategoryEntryField($entry, $object);
            $environment->create($entry);
        }
        $entry->publish();
    }

    /**
     * @param $entry
     * @param $object
     */
    public function setCategoryEntryField($entry, $object)
    {
        if ($object->getStore()->getCode() == 'admin') {
            $defaultLocale = $this->getDefaultISOCode();
            $this->setCategoryContentfulFields($entry, $defaultLocale, $object);
        } else {
            $locale = $this->getStoreLangByCode($object->getStore()->getCode());
            if (!$locale) return;
            $this->setCategoryContentfulFields($entry, $locale, $object);
        }
    }

    /**
     * @param $entry
     * @param $locale
     * @param $object
     * @return void
     */
    protected function setCategoryContentfulFields($entry, $locale, $object)
    {
        $entry->setField('title', $locale, $object->getName());
        $entry->setField('categories', $locale, [$object->getEntityId()]);
        try {
            $entry->setField('jsonData', $locale, $this->jsonDataCategory($object));
        } catch (\Exception $e) {
        }
    }

    /**
     * @param $category
     * @return array
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function jsonDataCategory($category): array
    {
        $items = $this->filtersLayerNavigation->getLayerNavigation($category);
        $array = $this->getDataJson($items, $category);
        $description = str_replace(
            "&nbsp;",
            "",
            html_entity_decode(html_entity_decode($category->getDescription()))
        );

        return [
            "url" => $category->getCategoryUrl(),
            "categories" => $category->getEntityId(),
            "content" => [
                "title" => $category->getName(),
                "description" => $description,
            ],
            "filters" => [
                "items" => $array
            ],
            "sort" => [
                "items" => [
                    [
                        "label" => $this->translate("From cheap to expensive", $category->getStore()->getId()),
                        "order" => "price_ASC"
                    ],
                    [
                        "label" => $this->translate("From expensive to cheap", $category->getStore()->getId()),
                        "order" => "price_DESC"
                    ],
                    [
                        "label" => $this->translate("The old ones first", $category->getStore()->getId()),
                        "order" => "created_ASC"
                    ],
                    [
                        "label" => $this->translate("New ones first", $category->getStore()->getId()),
                        "order" => "created_DESC",
                        "selected" => true
                    ]
                ]
            ]
        ];
    }

    /**
     * @param $entry
     * @param $object
     * @param $type
     * @param $total
     * @throws NoSuchEntityException
     */
    protected function setUrlEntryField($entry, $object, $type, $total)
    {
        if ($object->getData('store_id') == 0) return;
        $currentLocaleCodes = (array)$object->getData('store_id');

        foreach ($currentLocaleCodes as $currentLocaleCode) {
            $storeCode = $this->storeManager->getStore($currentLocaleCode)->getCode();
            $locale = $this->getStoreLangByCode($storeCode);
            $old = false;

            if (!$locale) return;

            if ($total > 0 &&
                isset($entry->getFields()['entity']) &&
                isset($entry->getFields()['entity'][$locale]) &&
                $type == "product") {
                $old = $entry->getFields()['entity'][$locale];
            }
            $stores = $this->storeManager->getStores();
            if ($type == "page") {
                if ($storeCode == 'admin') {
                    foreach ($stores as $store) {
                        $loc = $this->getStoreLangByCode($store->getCode());
                        if (!$loc) continue;
                        $entry->setField('entity_id', $loc, (int)$object->getPageId());
                        $entry->setField('urlKey', $loc, $object->getIdentifier());
                        $entry->setField('type', $loc, $type);
                        $entry->setField('entity', $loc, []);
                        $entry->setField('jsonData', $loc, $this->{'getJsonData' . strtoupper($type)}($object));
                    }
                    return;
                } else {
                    foreach ($stores as $store) {
                        $loc = $this->getStoreLangByCode($store->getCode());
                        if (!$loc) continue;
                        $entry->setField('entity_id', $loc, (int)$object->getPageId());
                        $entry->setField('urlKey', $loc, $object->getIdentifier());
                        $entry->setField('type', $loc, $type);
                    }
                }
            } else {
                foreach ($stores as $store) {
                    $loc = $this->getStoreLangByCode($store->getCode());
                    if (!$loc) continue;
                    $entry->setField('entity_id', $loc, (int)$object->getEntityId());
                    $entry->setField('urlKey', $loc, $object->getUrlKey());
                    $entry->setField('type', $loc, $type);
                }
            }
            if ($object->getTypeId() == "configurable" && $type == "product") {
                $entry->setField('entity', $locale, $this->getEntityData($object));

                if ($total > 0 &&
                    isset($entry->getFields()['entity']) &&
                    $old &&
                    isset($entry->getFields()['entity'][$locale])
                ) {
                    $new = $this->getEntityData($object);
                    $this->findDifferent($new, $old);
                }
            } elseif ($object->getTypeId() == "bundle" && $type == "product") {
                $entry->setField('entity', $locale, $this->getEntityBundleData($object));
            } else {
                $entry->setField('entity', $locale, []);
            }
            $entry->setField('jsonData', $locale, $this->{'getJsonData' . strtoupper($type)}($object));
        }
    }

    /**
     * @param $url
     * @return void
     * @throws NoSuchEntityException
     */
    public function removeUrl($url)
    {
        if (empty($url)) return;
        $contentTypeId = self::CONTENT_URLREWRITE_TYPE_ID;
        $fieldsQuery = 'urlKey';
        $customEntry = $this->validateContentful->validateContentfulEntries($contentTypeId, $fieldsQuery, $url);
        if (!$customEntry) return;

        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        $defaultIsoCode = $this->getDefaultISOCode();
        $arr = [];
        if ($entries->getTotal()) {
            $entry = $this->getEntry($entries, $environment);
            if (!empty($entry->getField('entity', $defaultIsoCode))) {
                $arr = $entry->getField('entity', $defaultIsoCode);
            } else {
                $tmp = $this->getAllISOCode();
                foreach ($tmp as $item) {
                    if ($item == $defaultIsoCode) continue;
                    if (!empty($item)) {
                        $arr = $entry->getField('entity', $item);
                    }
                }
            }

            $this->deleteEntry($entry);
        }
        if (!empty($arr)) {
            foreach ($arr as $item) {
                $q1 = $this->validateContentful->query($contentTypeId, 'entity_id', (int)$item);
                $entries = $environment->getEntries($q1);
                $entry = $this->getEntry($entries, $environment);
                $this->deleteEntry($entry);
            }
        }
    }

    /**
     * @param $categoryId
     * @return void
     */
    public function deleteCategoryFromContentful($categoryId)
    {
        $contentTypeId = self::CONTENT_CATEGORY_TYPE_ID;
        $fieldsQuery = 'categories';
        $customEntry = $this->validateContentful->validateContentfulEntries($contentTypeId, $fieldsQuery, $categoryId);
        if (!$customEntry) return;

        $entries = $customEntry['entries'];
        $environment = $customEntry['environment'];
        if ($entries->getTotal()) {
            $entry = $this->getEntry($entries, $environment);
            $this->deleteEntry($entry);
        }
    }

    /**
     * @param $new
     * @param $old
     */
    public function findDifferent($new, $old)
    {
        $different = array_diff($old, $new);
        if (!empty($different)) {
            foreach ($different as $diff) {
                $this->removeUrl($diff);
            }
        }
    }

    /**
     * @param $product
     * @return array
     */
    protected function getJsonDataProduct($product)
    {
        if ($product->getParentId()) {
            $parent = $this->factory->create()->load($product->getParentId());
            $data = ["type" => 'product',
                "sku" => $parent->getSku(),
                "childSku" => $product->getSku()];
        } else {
            $data = ["type" => 'product',
                "sku" => $product->getSku()];
        }

        $data["name"] = "product_" . $product->getId();

        try {
            $title = $this->getProductMetaTitle($product);
        } catch (\Exception $e) {
            $title = $product->getMetaTitle();
        }

        try {
            $description = $this->getProductMetaDescription($product);
        } catch (\Exception $e) {
            $description = $product->getMetaDescription();
        }

        $data['meta'] = [
            "title" => $title,
            "description" => $description,
        ];

        return $data;
    }

    /**
     * @param $product
     * @return string
     */
    protected function getProductMetaTitle($product)
    {
        if ($product->getTypeId() == 'configurable') {
            $title = $product->getMetaTitle();
        } else {
            $productId = $product->getEntityId();
            $storeId = $product->getStoreId();
            $productLang = $this->modelProduct->load($productId)->setStoreId($storeId);
            $title = $productLang->getMetaTitle() ?? '';
        }

        return $title;
    }

    /**
     * @param $product
     * @return string
     */
    protected function getProductMetaDescription($product)
    {
        if ($product->getTypeId() == 'configurable') {
            $description = $product->getMetaDescription();
        } else {
            $productId = $product->getEntityId();
            $storeId = $product->getStoreId();
            $productLang = $this->modelProduct->load($productId)->setStoreId($storeId);
            $description = $productLang->getMetaDescription() ?? '';
        }

        return $description;
    }

    /**
     * @param $product
     * @return array
     */
    protected function getEntityData($object)
    {
        $childIds = [];
        $childProducts = $object->getTypeInstance()->getUsedProducts($object);

        foreach ($childProducts as $child) {
            $childIds[] = $child->getData("entity_id");
        }
        return $childIds;
    }

    /**
     * @param $page
     * @return string[]
     */
    protected function getJsonDataPage($page)
    {
        $jsonBodyContent = str_replace("\r\n", "", $page->getData('json_body_content'));
//        $jsonBody = json_decode(stripslashes($jsonBodyContent));
//        $jsonBody = json_decode($jsonBodyContent);
        $jsonBody = json_decode($page->getData('json_body_content'));
        return [
            "type" => 'page',
            "name" => "page_" . $page->getId(),
            "meta" => [
                "title" => $page->getMetaTitle(),
                "description" => $page->getMetaDescription(),
            ],
            "items" => $jsonBody

        ];
    }

    /**
     * @param $category
     * @return string[]
     */
    protected function getJsonDataCategory($category)
    {
        return [
            "type" => 'category',
            "name" => "category_" . $category->getId(),
            "categories" => $category->getEntityId(),
            "meta" => [
                "title" => $category->getMetaTitle(),
                "description" => $category->getMetaDescription(),
            ]
        ];
    }

    /**
     * @param $items
     * @param $category
     * @return array
     */
    public function getDataJson($items, $category)
    {
        if (isset($items)) {
            $array = [];
            foreach ($items as $name => $item) {
                if ($data = $this->getdataItems($item, $name, $category)) {
                    $array[] = [
                        "code" => strtolower($name),
                        "label" => $item[0]->getFilter()
                            ->getAttributeModel()
                            ->setStoreId($category->getStore()->getId())
                            ->getData('store_label'),
                        "type" => $name == "price" ? "range" : "checkbox",
                        ($name == "price" ? 'range' : 'items') => $data
                    ];
                }
            }

            return $array;
        }
    }

    /**
     * @param $item
     * @param $name
     * @param $category
     * @return array|false
     */
    public function getDataItems($item, $name, $category)
    {
        $array = [];

        $collection = $this->productFactory->create();
        $categoryCollectionProductsIds = $category->getProductCollection()->getAllIds();

        $categoryCollectionPrices = $collection
            ->addAttributeToSelect(['price', 'entity_id'])
            ->addStoreFilter($category->getStore()->getId())
            ->addFieldToFilter('entity_id', ['in' => $categoryCollectionProductsIds]);
        $minPrice = $categoryCollectionPrices->setOrder('price', 'DESC')->getMinPrice();
        $maxPrice = $categoryCollectionPrices->setOrder('price', 'DESC')->getMaxPrice();

        if ($name == "price") {
            $array['from'] = $minPrice;
            $array['to'] = $maxPrice;
        } else {
            if (count($item) <= 1) {
                return false;
            }

            foreach ($item as $value) {
                if (!$value->getFilter()->hasAttributeModel()) continue;
                $array[] = [
                    "label" => $value->getFilter()
                        ->getAttributeModel()
                        ->setStoreId($category->getStore()->getId())
                        ->getSource()
                        ->getOptionText($value->getValue()),
                    "tag" => $value->getFilter()
                            ->getAttributeModel()
                            ->setStoreId($category->getStore()->getId())
                            ->getAttributeCode() . "-" . (
                        strtolower(
                            str_replace(
                                ' ',
                                '_',
                                $value->getFilter()
                                    ->getAttributeModel()
                                    ->setStoreId($this->getStoreByLanguageCode('en')->getId())
                                    ->getSource()->getOptionText($value->getValue())
                            )
                        )
                        )
                ];
            }

            return $array;
        }

        return $array;
    }

    /**
     * @param $object
     * @return array
     */
    public function getEntityBundleData($object)
    {
        $bundleOptions = $object->getTypeInstance()->getChildrenIds($object->getId(), false);
        $childUrlKey = [];
        foreach ($bundleOptions as $childrenValue) {
            foreach ($childrenValue as $value) {
                $child = $this->factory->create()->load($value);
                $childUrlKey[] = $child->getSku();
            }
        }
        return $childUrlKey;
    }

    /**
     * @param $name
     * @param $storeId
     * @return string
     */
    public function translate($name, $storeId)
    {
        $storeId = (string)$storeId;
        $translate = [
            '0' => [
                'From cheap to expensive' => 'Від дешевого до дорогого',
                'From expensive to cheap' => 'Від дорогого до дешевого',
                'The old ones first' => 'Спершу старі',
                'New ones first' => 'Спершу нові'
            ],
            '1' => [
                'From cheap to expensive' => 'Від дешевого до дорогого',
                'From expensive to cheap' => 'Від дорогого до дешевого',
                'The old ones first' => 'Спершу старі',
                'New ones first' => 'Спершу нові'
            ],
            '2' => [
                'From cheap to expensive' => 'От дешевого к дорогому',
                'From expensive to cheap' => 'От дорогого к дешевому',
                'The old ones first' => 'Сначала старые',
                'New ones first' => 'Сначала новые'
            ],
            '3' => [
                'From cheap to expensive' => 'From cheap to expensive',
                'From expensive to cheap' => 'From expensive to cheap',
                'The old ones first' => 'The old ones first',
                'New ones first' => 'New ones first'
            ]
        ];

        return $translate[$storeId][$name];
    }
}
