<?php

namespace IdeaInYou\SyncToContentful\Model\System\Config\Source\Locale;

use Magento\Framework\Data\OptionSourceInterface;
use Magento\Store\Model\StoreManagerInterface;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\ContentfulData;

class DefaultStoreView implements OptionSourceInterface
{
    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var ContentfulData
     */
    private ContentfulData $contentfulData;

    /**
     * @param StoreManagerInterface $storeManager
     * @param ContentfulData $contentfulData
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        ContentfulData        $contentfulData
    ) {
        $this->storeManager = $storeManager;
        $this->contentfulData = $contentfulData;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function toOptionArray()
    {
        $arr[] = [
            'value' => '',
            'label' => __('-- Select an Store View --')
        ];
        $stores = $this->storeManager->getStores();
        foreach ($stores as $store) {
            $label = $this->contentfulData->getStoreLangByCode($store->getCode());
            if (!$label) continue;
            $arr[] = [
                'value' => $store->getCode(),
                'label' => __($label)
            ];
        }
        return $arr;
    }

}
