<?php

namespace IdeaInYou\SyncToContentful\Model;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Serialize\Serializer\Json;

class GetDataLocal extends AbstractHelper
{

    const LOCALE = 'contentfulconfigsync/locale/row';
    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $_storeManager;
    /**
     * @var Json
     */
    protected Json $serialize;

    /**
     * @param Context $context
     * @param StoreManagerInterface $storeManager
     * @param Json $serialize
     */
    public function __construct(
        Context               $context,
        StoreManagerInterface $storeManager,
        Json                  $serialize
    ) {
        $this->_storeManager = $storeManager;
        $this->serialize = $serialize;
        parent::__construct($context);
    }

    /**
     * @return int
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreid()
    {
        return $this->_storeManager->getStore()->getId();
    }

    /**
     * @return array|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getLocal()
    {
        $localConfig = $this->scopeConfig->getValue(
            self::LOCALE, ScopeInterface::SCOPE_STORE, $this->getStoreid()
        );
        if ($localConfig == '' || $localConfig == null) return;

        $unSerializeData = $this->serialize->unserialize($localConfig);
        $localConfigArray = [];
        foreach ($unSerializeData as $row) {
            $localConfigArray[$row['store_view']] = $row['locale_contentful'];
        }

        return $localConfigArray;
    }
}
