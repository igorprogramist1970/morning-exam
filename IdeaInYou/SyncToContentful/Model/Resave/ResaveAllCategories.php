<?php

namespace IdeaInYou\SyncToContentful\Model\Resave;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Api\CategoryRepositoryInterface;

class ResaveAllCategories
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var CategoryRepositoryInterface
     */
    private CategoryRepositoryInterface $categoryRepository;

    /**
     * @var Registry
     */
    private \Magento\Framework\Registry $registry;

    /**
     * @param CollectionFactory $collectionFactory
     * @param StoreManagerInterface $storeManager
     * @param CategoryRepositoryInterface $categoryRepository
     * @param Registry $registry
     */
    public function __construct(
        CollectionFactory           $collectionFactory,
        StoreManagerInterface       $storeManager,
        CategoryRepositoryInterface $categoryRepository,
        \Magento\Framework\Registry $registry
    ) {
        $this->collectionFactory = $collectionFactory;
        $this->storeManager = $storeManager;
        $this->categoryRepository = $categoryRepository;
        $this->registry = $registry;
    }


    /**
     * @return void
     */
    public function resaveAllCategories()
    {
        $stores = $this->storeManager->getStores();
        foreach ($stores as $store) {
            try {
                $categories = $this->getCategoriesForStore($store)->getItems();
                $this->resaveCategory($categories);
            } catch (\Exception $e) {

            }
        }
    }

    /**
     * @param $store
     * @return \Magento\Catalog\Model\ResourceModel\Category\Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function getCategoriesForStore($store)
    {
        return $this->collectionFactory->create()
            ->addAttributeToSelect('*')
            ->setStore($store)
            ->addAttributeToFilter('is_active', '1');
    }

    /**
     * @param $categories
     * @return void
     * @throws CouldNotSaveException
     * @throws NoSuchEntityException
     */
    protected function resaveCategory($categories)
    {
        foreach ($categories as $category) {
            try {
                $catId = $category->getEntityId();
                /**
                 *  if query via graphql will fall, check $this->categoryRepository->getChildren(),
                 *  you must apply  explode() function for that field
                 */
                $tmpCategory = $this->categoryRepository->get($catId);
                $this->categoryRepository->save($tmpCategory);
            } catch (\Exception $e) {

            }
            $this->registry->__destruct();
        }
    }
}
