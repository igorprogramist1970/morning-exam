<?php

namespace IdeaInYou\SyncToContentful\Model;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Catalog\Api\CategoryRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\ResourceConnection;

class GetFiltersLayerNavigation extends \Magento\Framework\Model\AbstractModel
{

    /**
     * @var \Magento\Framework\Model\Context
     */
    private \Magento\Framework\Model\Context $context;
    /**
     * @var \Magento\Framework\Registry
     */
    private \Magento\Framework\Registry $registry;
    /**
     * @var StoreManagerInterface
     */
    private \Magento\Store\Model\StoreManagerInterface $storeManager;
    /**
     * @var \Magento\Store\Model\App\Emulation
     */
    private \Magento\Store\Model\App\Emulation $appEmulation;

    /**
     * @var string[]
     */
    protected $_ignoreFilter = ['cat', 'price'];
    /**
     * @var \Magento\Framework\App\State
     */
    private \Magento\Framework\App\State $state;

    /**
     * @var RequestInterface
     */
    private RequestInterface $request;

    /**
     * @var CategoryRepositoryInterface
     */
    private CategoryRepositoryInterface $categoryRepository;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $categoryCollectionFactory;

    /**
     * @var ResourceConnection
     */
    private ResourceConnection $resourceConnection;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Store\Model\App\Emulation $appEmulation
     * @param CollectionFactory $categoryCollectionFactory
     * @param CategoryRepositoryInterface $categoryRepository
     * @param RequestInterface $request
     * @param ResourceConnection $resourceConnection
     * @param \Magento\Framework\App\State $state
     */
    public function __construct(
        \Magento\Framework\Model\Context           $context,
        \Magento\Framework\Registry                $registry,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Store\Model\App\Emulation         $appEmulation,
        CollectionFactory                          $categoryCollectionFactory,
        CategoryRepositoryInterface                $categoryRepository,
        RequestInterface                           $request,
        ResourceConnection                         $resourceConnection,
        \Magento\Framework\App\State               $state
    ) {
        parent::__construct($context, $registry);
        $this->storeManager = $storeManager;
        $this->appEmulation = $appEmulation;
        $this->request = $request;
        $this->categoryRepository = $categoryRepository;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
        $this->resourceConnection = $resourceConnection;
        $this->connection = $resourceConnection->getConnection();
        $this->state = $state;
    }

    /**
     * @param $category
     * @return mixed
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getLayerNavigation($category)
    {
        $storeId = $this->storeManager->getStore()->getId();
        $id = $this->storeManager->getDefaultStoreView()->getId();
        $this->storeManager->setCurrentStore($id);
        try {
            $this->state->getAreaCode();
        } catch (\Exception $exception) {
            $this->state->setAreaCode(\Magento\Framework\App\Area::AREA_FRONTEND);
        }
        $this->appEmulation->startEnvironmentEmulation($storeId, \Magento\Framework\App\Area::AREA_FRONTEND, true);
        $this->state->getAreaCode();
        $items = $this->state->emulateAreaCode(
            \Magento\Framework\App\Area::AREA_FRONTEND,
            [$this, 'areaEmulateCode'],
            [$category]
        );
        $this->appEmulation->stopEnvironmentEmulation();
        return $items;
    }

    /**
     * @param $category
     * @return array|void
     * @throws NoSuchEntityException
     */
    public function areaEmulateCode($category)
    {
        $storeId = $this->storeManager->getStore()->getId();

        if ($storeId == 0) return;

        $id = $this->storeManager->getDefaultStoreView()->getId();
        $this->storeManager->setCurrentStore($storeId);
        $this->storeManager->getDefaultStoreView();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();

        $layer = $objectManager
            ->create(
                \Magento\Catalog\Model\Layer\Category::class,
                [
                    'data' => ['current_category' => $category]
                ]
            );

        $layer->getProductCollection()->getFacetedData('category');

        $fill = $objectManager->create('Magento\Catalog\Model\Layer\Category\FilterableAttributeList');
        $layerCategoryConfig = $objectManager->create('Magento\Catalog\Model\Config\LayerCategoryConfig');

        $filterList = new \Magento\Catalog\Model\Layer\FilterList($objectManager, $fill, $layerCategoryConfig);
        $filterAttributes = $filterList->getFilters($layer);

        $value = [];

        foreach ($filterAttributes as $filter) {

            /**
             * Gives all available filter options in that particular filter
             */
            $items = $filter->getItems();

            foreach ($items as $item) {
                $value[$filter->getRequestVar()][] = $item;
            }
        }
        return $value;
    }

}
