<?php

namespace IdeaInYou\SyncToContentful\Console\Command;

use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Magento\Store\Model\StoreManagerInterface;
use IdeaInYou\SyncToContentful\Model\Product\ProductsCollection;


class ResaveProduct extends Command
{
    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var ProductsCollection
     */
    private ProductsCollection $productsCollection;

    /**
     * @var State
     */
    private \Magento\Framework\App\State $state;

    /**
     * @param StoreManagerInterface $storeManager
     * @param ProductsCollection $productsCollection
     * @param State $state
     */
    public function __construct(
        StoreManagerInterface        $storeManager,
        ProductsCollection           $productsCollection,
        \Magento\Framework\App\State $state
    ) {
        parent::__construct();
        $this->storeManager = $storeManager;
        $this->productsCollection = $productsCollection;
        $this->state = $state;
    }

    /**
     * Initialization of the command.
     */
    protected function configure()
    {
        $this->setName('product:resave');
        $this->setDescription('Resave all product');
        parent::configure();
    }

    /**
     * CLI command description.
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return void
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        try {
            $this->state->setAreaCode('adminhtml');
            $this->productsCollection->resaveAllProducts();
        } catch (\Exception $e) {
            $output->writeln("Entity save error", $e);
        }
        $output->writeln("Products were resave successfully. You updated " . ProductsCollection::$count . " products.");
    }
}
