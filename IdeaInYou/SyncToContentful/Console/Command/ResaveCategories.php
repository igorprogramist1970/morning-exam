<?php

namespace IdeaInYou\SyncToContentful\Console\Command;

use Magento\Framework\App\State;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use IdeaInYou\SyncToContentful\Model\Resave\ResaveAllCategories;

class ResaveCategories extends Command
{
    /**
     * @var State
     */
    private State $state;

    /**
     * @var ResaveAllCategories
     */
    private ResaveAllCategories $resaveAllCategories;

    /**
     * @param ResaveAllCategories $resaveAllCategories
     * @param State $state
     */
    public function __construct(
        ResaveAllCategories $resaveAllCategories,
        State               $state
    ) {
        parent::__construct();
        $this->state = $state;
        $this->resaveAllCategories = $resaveAllCategories;
    }

    /**
     * Initialization of the command.
     */
    protected function configure()
    {
        $this->setName('categories:resave');
        $this->setDescription('Resave all categories');
        parent::configure();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return void
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $this->state->setAreaCode('adminhtml');
        try {
            $this->resaveAllCategories->resaveAllCategories();
        } catch (\Exception $e) {
            $output->writeln("Entity save error", $e);
        }
        $output->writeln("Categories were resave successfully.");
    }
}
