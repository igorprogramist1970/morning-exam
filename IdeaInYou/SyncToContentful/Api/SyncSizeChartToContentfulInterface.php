<?php

namespace IdeaInYou\SyncToContentful\Api;

interface SyncSizeChartToContentfulInterface
{
    /**
     * @param $type
     * @param $object
     * @return mixed
     */
    public function addSizeChartToContentful($type,$object);

    /**
     * @param $type
     * @param $object
     * @return mixed
     */
    public function deleteSizeChartFromContentful($type,$object);

}
