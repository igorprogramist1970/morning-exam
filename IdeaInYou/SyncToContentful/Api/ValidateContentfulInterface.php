<?php

namespace IdeaInYou\SyncToContentful\Api;

interface ValidateContentfulInterface
{
    /**
     * @param $contentTypeId
     * @param $fieldsQuery
     * @param $value
     * @param $fieldsQueryType
     * @param $type
     * @return mixed
     */
    public function validateContentfulEntries($contentTypeId, $fieldsQuery, $value = null, $fieldsQueryType = null, $type = null);

    /**
     * @param $contentTypeId
     * @param $fieldsQuery
     * @param $value
     * @param $fieldsQueryType
     * @param $type
     * @return mixed
     */
    public function query($contentTypeId, $fieldsQuery, $value, $fieldsQueryType = null, $type = null);

    /**
     * @return mixed
     */
    public function isConfigEnable();

    /**
     * @param $type
     * @return mixed
     */
    public function validator($type);

    /**
     * @return mixed
     */
    public function isEnable();

}
