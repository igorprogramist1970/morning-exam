<?php

namespace IdeaInYou\SyncToContentful\Api;

use IdeaInYou\SyncToContentful\Model\SyncToContentful;

interface SyncToContentfulInterface
{

    /**
     * @api
     * @param
     * @return SyncToContentful;
     */
    public function syncAllEntity();

    /**
     * @api
     * @param
     * $entity
     * $store_id
     * @return SyncToContentful;
     */
    public function syncAllEntityByStoreId($storeId);
}
