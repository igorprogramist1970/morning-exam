<?php

namespace IdeaInYou\SyncToContentful\Setup\Patch\Data;

use Magento\Catalog\Model\Category;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Setup\Patch\DataPatchInterface;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use \Magento\Framework\App\State;

/**
 * Class UpdateUrlKeyHerProducts
 * @package IdeaInYou\SyncToContentful\Setup\Patch\Data
 */
class UpdateUrlKeyHerProducts implements DataPatchInterface
{

    /**
     * @var CategoryFactory
     */
    protected CategoryFactory $categoryFactory;

    /**
     * @var ModuleDataSetupInterface
     */
    private $moduleDataSetup;

    /**
     *
     */
    const CATEGORY_HER_ID = 4;

    /**
     *
     */
    const HER_URL_KEY_SLUG = '-she';
    /**
     * @var State
     */
    private State $state;

    /**
     * UpdateUrlKeyHerProducts constructor.
     * @param CategoryFactory $categoryFactory
     * @param ModuleDataSetupInterface $moduleDataSetup
     * @param State $state
     */
    public function __construct(
        CategoryFactory $categoryFactory,
        ModuleDataSetupInterface $moduleDataSetup,
        State $state
    )
    {
        $this->categoryFactory = $categoryFactory;
        $this->moduleDataSetup = $moduleDataSetup;
        $this->state = $state;
    }


    /**
     * @return void
     * @throws LocalizedException
     */
    public function apply()
    {
        $this->moduleDataSetup->getConnection()->startSetup();

        $this->state->setAreaCode('adminhtml');

        $products = $this->getProductCollection()->getItems();

        foreach ($products as $product) {
            $productWebsite = $product->getWebsiteIds();
            foreach ($productWebsite as $websiteId) {
                $productLang = $product->getCollection()
                    ->addAttributeToSelect('*')
                    ->addFieldToFilter('entity_id', $product->getId())
                    ->addStoreFilter($websiteId)
                    ->getFirstItem();

                if($productLang->getUrlKey()) {
                    $productLang->setUrlKey($productLang->getUrlKey() . self::HER_URL_KEY_SLUG);
                    $productLang->save();
                }
            }
        }

        $this->moduleDataSetup->getConnection()->endSetup();
    }

    /**
     * @return Category
     */
    public function getCategory(): Category
    {
        return $this->categoryFactory->create()->load(self::CATEGORY_HER_ID);
    }

    /**
     * @return mixed
     */
    public function getProductCollection()
    {
        return $this->getCategory()->getProductCollection()->addAttributeToSelect('*');
    }

    /**
     * @return array|string[]
     */
    public static function getDependencies(): array
    {
        return [];
    }

    /**
     * @return array|string[]
     */
    public function getAliases(): array
    {
        return [];
    }
}
