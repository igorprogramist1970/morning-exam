<?php

namespace IdeaInYou\SyncToContentful\Plugin\Model\ResourceModel;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\ResourceModel\Category;
use Magento\Framework\Model\AbstractModel;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\UrlRewrites;
use Magento\Framework\Registry;

class CategoryDelete
{
    /**
     * @var UrlRewrites
     */
    private UrlRewrites $urlRewrites;

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var Registry
     */
    private Registry $registry;

    /**
     * CategorySave constructor.
     * @param UrlRewrites $urlRewrites
     * @param ValidateContentfulInterface $validateContentful
     * @param Registry $registry
     */
    public function __construct(
        UrlRewrites                 $urlRewrites,
        ValidateContentfulInterface $validateContentful,
        Registry                    $registry
    ) {
        $this->urlRewrites = $urlRewrites;
        $this->validateContentful = $validateContentful;
        $this->registry = $registry;
    }


    /**
     * @param Category $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterDelete(Category $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $url = $this->registry->registry((string)$object->getEntityId());
        $this->urlRewrites->removeUrl($url);
        $this->urlRewrites->deleteCategoryFromContentful($object->getEntityId());

        return $result;
    }
}
