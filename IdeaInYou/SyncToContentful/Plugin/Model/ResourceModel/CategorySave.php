<?php

namespace IdeaInYou\SyncToContentful\Plugin\Model\ResourceModel;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\ResourceModel\Category;
use Magento\Framework\Model\AbstractModel;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\UrlRewrites;
use IdeaInYou\SyncToContentful\Model\ContentfulConfig\UpdateCategoryMenu;

/**
 * Class CategorySave
 * @package IdeaInYou\SyncToContentful\Plugin\Model\ResourceModel
 */
class CategorySave
{
    const TYPE_CATEGORY = 'category';
    /**
     * @var UrlRewrites
     */
    private UrlRewrites $urlRewrites;

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var UpdateCategoryMenu
     */
    private UpdateCategoryMenu $updateCategoryMenu;

    /**
     * CategorySave constructor.
     * @param UrlRewrites $urlRewrites
     * @param ValidateContentfulInterface $validateContentful
     * @param UpdateCategoryMenu $updateCategoryMenu
     */
    public function __construct(
        UrlRewrites                 $urlRewrites,
        ValidateContentfulInterface $validateContentful,
        UpdateCategoryMenu          $updateCategoryMenu
    ) {
        $this->urlRewrites = $urlRewrites;
        $this->validateContentful = $validateContentful;
        $this->updateCategoryMenu = $updateCategoryMenu;
    }


    /**
     * @param Category $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed|void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterSave(Category $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        if (!($object->getEntityId() == 2)) {
            $this->urlRewrites->addUrlToContentful($object, self::TYPE_CATEGORY);
            $this->urlRewrites->addUrlToContentfulCategories($object);
        }
        $this->updateCategoryMenu->updateCategoryTree($object);
        return $result;
    }
}
