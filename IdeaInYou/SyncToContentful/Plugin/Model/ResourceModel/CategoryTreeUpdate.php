<?php

namespace IdeaInYou\SyncToContentful\Plugin\Model\ResourceModel;

use Magento\Catalog\Model\CategoryRepository;
use IdeaInYou\SyncToContentful\Model\ContentfulConfig\UpdateCategoryMenu;
use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\Category;

/**
 * Class CategoryDelete
 * @package IdeaInYou\SyncToContentful\Plugin\Model\ResourceModel
 */
class CategoryTreeUpdate
{
    /**
     * @var UpdateCategoryMenu
     */
    private UpdateCategoryMenu $updateCategoryMenu;

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;


    /**
     * @param UpdateCategoryMenu $updateCategoryMenu
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        UpdateCategoryMenu          $updateCategoryMenu,
        ValidateContentfulInterface $validateContentful
    ) {
        $this->updateCategoryMenu = $updateCategoryMenu;
        $this->validateContentful = $validateContentful;
    }


    /**
     * @param CategoryRepository $subject
     * @param $result
     * @param Category $object
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterDelete(CategoryRepository $subject, $result, Category $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->updateCategoryMenu->updateCategoryTree($object);

        return $result;
    }
}
