<?php

namespace IdeaInYou\SyncToContentful\Plugin\Model\ResourceModel;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\Category;

class BeforeCategoryMove
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Category $subject
     * @return void
     */
    public function beforeMove(Category $subject)
    {
        $this->validateContentful->validator(CategorySave::TYPE_CATEGORY);
    }
}
