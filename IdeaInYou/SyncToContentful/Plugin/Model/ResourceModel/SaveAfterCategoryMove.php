<?php

namespace IdeaInYou\SyncToContentful\Plugin\Model\ResourceModel;

use Magento\Catalog\Model\Category;
use IdeaInYou\SyncToContentful\Model\ContentfulConfig\UpdateCategoryMenu;

class SaveAfterCategoryMove
{
    /**
     * @var UpdateCategoryMenu
     */
    private UpdateCategoryMenu $updateCategoryMenu;

    /**
     * @param UpdateCategoryMenu $updateCategoryMenu
     */
    public function __construct(
        UpdateCategoryMenu $updateCategoryMenu
    ) {
        $this->updateCategoryMenu = $updateCategoryMenu;
    }

    /**
     * @param Category $subject
     * @param $result
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterMove(Category $subject, $result)
    {
        $this->updateCategoryMenu->updateCategoryTree($subject);
        return $result;
    }

}
