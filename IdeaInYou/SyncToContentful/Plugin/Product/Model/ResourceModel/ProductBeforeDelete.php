<?php

namespace IdeaInYou\SyncToContentful\Plugin\Product\Model\ResourceModel;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Catalog\Model\ResourceModel\Product;


class ProductBeforeDelete
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Product $subject
     * @return void
     */
    public function beforeDelete(Product $subject)
    {
        $this->validateContentful->validator(ProductBeforeSave::TYPE_PRODUCT);
    }
}
