<?php

namespace IdeaInYou\SyncToContentful\Plugin\Product\Model\ResourceModel;

use Magento\Catalog\Model\ResourceModel\Product;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Model\CategoryRepository;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Model\AbstractModel;

class ProductSaveMetaData
{
    const META_DATA_PRODUCT_TITLE = "meta_data_product/meta_data/meta_title";

    const META_DATA_PRODUCT_DESCRIPTION = "meta_data_product/meta_data/meta_description";

    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var CategoryRepository
     */
    private CategoryRepository $categoryRepository;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @param ScopeConfigInterface $scopeConfig
     * @param CategoryRepository $categoryRepository
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ScopeConfigInterface  $scopeConfig,
        CategoryRepository    $categoryRepository,
        StoreManagerInterface $storeManager
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->categoryRepository = $categoryRepository;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Product $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterSave(Product $subject, $result, AbstractModel $object)
    {
        $websites = $object->getWebsiteIds();
        foreach ($websites as $websiteId) {
            $objectLang = $object->getCollection()
                ->addAttributeToSelect('*')
                ->addFieldToFilter('entity_id', $object->getId())
                ->addStoreFilter($websiteId)
                ->getFirstItem();
            if ($this->checkMetaField($objectLang)) {
                continue;
            }
            $this->setMetaDataToProduct($objectLang);
            $objectLang->save();
        }

        return $result;
    }

    /**
     * @param $product
     * @return void
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function setMetaDataToProduct($product)
    {
        $storeId = $product->getStoreId();
        $category = $this->getCurrentCategory($product);
        $product->setStoreId($storeId);
        $metaTitle = $this->getMetaDataTitle($storeId);
        $metaDescription = $this->getMetaDataDescription($storeId);
        $changedTitle = str_replace("{name}", $product->getName(), $metaTitle);
        $changedDescription = str_replace(["{category}", "{name}"],
            [$category->getName(), $product->getName()], $metaDescription);
        $product->setMetaTitle($changedTitle);
        $product->setMetaDescription($changedDescription);
    }

    /**
     * @return mixed
     */
    public function getMetaDataTitle($storeId)
    {
        return $this->scopeConfig->getValue(self::META_DATA_PRODUCT_TITLE,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * @return mixed
     */
    public function getMetaDataDescription($storeId)
    {
        return $this->scopeConfig->getValue(self::META_DATA_PRODUCT_DESCRIPTION,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE, $storeId);
    }

    /**
     * @param $objectLang
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function checkMetaField($objectLang): bool
    {
        $storeId = $objectLang->getStoreId();
        $category = $this->getCurrentCategory($objectLang);
        $metaDescription = $this->getMetaDataDescription($storeId);
        $changeMetaDescription = str_replace(["{category}", "{name}"],
            [$category->getName(), $objectLang->getName()], $metaDescription);

        $compareDescription = $objectLang->getMetaDescription() ?? "";

        if ($changeMetaDescription == $compareDescription) {
            return true;
        }
        return false;
    }

    /**
     * @param $product
     * @return \Magento\Catalog\Api\Data\CategoryInterface|mixed|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function getCurrentCategory($product)
    {
        $storeId = $product->getStoreId();
        $array = $product->getCategoryIds();
        $currentId = end($array) ? end($array) : 2;
        return $this->categoryRepository->get($currentId, $storeId);
    }

}
