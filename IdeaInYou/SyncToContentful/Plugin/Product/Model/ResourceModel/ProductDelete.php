<?php

namespace IdeaInYou\SyncToContentful\Plugin\Product\Model\ResourceModel;

use Magento\Catalog\Model\ResourceModel\Product;
use Magento\Framework\Model\AbstractModel;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\DataProductPrepare;
use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;

class ProductDelete
{
    /**
     * @var DataProductPrepare
     */
    private DataProductPrepare $dataProductPrepare;

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param DataProductPrepare $dataProductPrepare
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        DataProductPrepare          $dataProductPrepare,
        ValidateContentfulInterface $validateContentful
    ) {
        $this->dataProductPrepare = $dataProductPrepare;
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Product $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterDelete(Product $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->dataProductPrepare->deleteProductFromContentful($object);
        return $result;
    }
}
