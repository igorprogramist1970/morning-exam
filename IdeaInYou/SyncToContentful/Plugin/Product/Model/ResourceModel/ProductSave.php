<?php

namespace IdeaInYou\SyncToContentful\Plugin\Product\Model\ResourceModel;

use Magento\Catalog\Model\ResourceModel\Product;
use Magento\Framework\Model\AbstractModel;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\DataProductPrepare;
use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;

class ProductSave
{

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var DataProductPrepare
     */
    private DataProductPrepare $dataProductPrepare;


    /**
     * @param DataProductPrepare $dataProductPrepare
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        DataProductPrepare          $dataProductPrepare,
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
        $this->dataProductPrepare = $dataProductPrepare;
    }

    /**
     * @param Product $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterSave(Product $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->dataProductPrepare->prepareProductContentfulData($object);
        return $result;
    }
}
