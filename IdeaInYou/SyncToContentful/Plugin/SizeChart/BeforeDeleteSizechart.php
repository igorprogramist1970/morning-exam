<?php

namespace IdeaInYou\SyncToContentful\Plugin\SizeChart;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Framework\Controller\ResultFactory;
use Magepow\Sizechart\Controller\Adminhtml\Sizechart\MassDelete;
use Magento\Backend\App\Action\Context;
use Magepow\Sizechart\Model\ResourceModel\Sizechart\CollectionFactory;
use Magento\Ui\Component\MassAction\Filter;

class BeforeDeleteSizechart extends MassDelete
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        Context                     $context,
        Filter                      $filter,
        CollectionFactory           $collectionFactory
    ) {
        parent::__construct($context, $filter, $collectionFactory);
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param MassDelete $subject
     * @param \Closure $proceed
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\Result\Redirect&\Magento\Framework\Controller\ResultInterface|mixed
     */
    public function aroundExecute(MassDelete $subject, \Closure $proceed)
    {
        try {
            $this->validateContentful->validator(SaveAfterSizechart::TYPE_SIZECHART);
            return $proceed();
        } catch (\Exception $e) {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('*/*/');
        }
    }
}
