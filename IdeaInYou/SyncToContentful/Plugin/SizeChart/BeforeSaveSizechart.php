<?php

namespace IdeaInYou\SyncToContentful\Plugin\SizeChart;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magepow\Sizechart\Model\ResourceModel\Sizechart;

class BeforeSaveSizechart
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Sizechart $subject
     * @return void
     */
    public function beforeSave(Sizechart $subject){
        $this->validateContentful->validator(SaveAfterSizechart::TYPE_SIZECHART);
    }
}
