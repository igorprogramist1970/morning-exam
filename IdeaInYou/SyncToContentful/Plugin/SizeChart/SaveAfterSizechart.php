<?php

namespace IdeaInYou\SyncToContentful\Plugin\SizeChart;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magepow\Sizechart\Model\ResourceModel\Sizechart;
use Magento\Framework\Model\AbstractModel;
use IdeaInYou\SyncToContentful\Model\SizeChart\SizeChartToContentful;

class SaveAfterSizechart
{
    const TYPE_SIZECHART = 'sizeChart';
    /**
     * @var SizeChartToContentful
     */
    private SizeChartToContentful $sizeChart;
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;


    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param SizeChartToContentful $sizeChart
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        SizeChartToContentful       $sizeChart
    ) {
        $this->sizeChart = $sizeChart;
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Sizechart $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterSave(Sizechart $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        try {
            $this->sizeChart->addSizeChartToContentful(self::TYPE_SIZECHART, $object);
        } catch (\Exception $e){
        }

        return $result;
    }
}
