<?php

namespace IdeaInYou\SyncToContentful\Plugin\SizeChart;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\SyncToContentful\Model\SizeChart\SizeChartToContentful;
use Magento\Framework\Model\AbstractModel;
use Magepow\Sizechart\Model\ResourceModel\Sizechart;

class DeleteAfterSizechart
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;
    /**
     * @var SizeChartToContentful
     */
    private SizeChartToContentful $sizeChart;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param SizeChartToContentful $sizeChart
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        SizeChartToContentful       $sizeChart
    ) {
        $this->validateContentful = $validateContentful;
        $this->sizeChart = $sizeChart;
    }

    /**
     * @param Sizechart $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterDelete(Sizechart $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->sizeChart->deleteSizeChartFromContentful(SaveAfterSizechart::TYPE_SIZECHART, $object);
        return $result;
    }
}
