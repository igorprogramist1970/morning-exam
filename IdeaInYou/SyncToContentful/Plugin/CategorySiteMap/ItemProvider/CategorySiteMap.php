<?php

namespace IdeaInYou\SyncToContentful\Plugin\CategorySiteMap\ItemProvider;

use Magento\Sitemap\Model\ItemProvider\ConfigReaderInterface;
use Magento\Sitemap\Model\ResourceModel\Catalog\CategoryFactory;
use Magento\Catalog\Model\CategoryFactory as ModelCategory;
use Magento\Sitemap\Model\SitemapItemInterfaceFactory;
use \Magento\Store\Model\StoreManagerInterface;
use Magento\Eav\Model\Config as EavConfig;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\ContentfulData;

class CategorySiteMap
{
    /**
     * @var CategoryFactory
     */
    private CategoryFactory $categoryFactory;

    /**
     * @var ModelCategory
     */
    private ModelCategory $modelCategory;

    /**
     * @var SitemapItemInterfaceFactory
     */
    private SitemapItemInterfaceFactory $itemInterfaceFactory;

    /**
     * @var ConfigReaderInterface
     */
    private ConfigReaderInterface $configReader;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var EavConfig
     */
    private EavConfig $eavConfig;

    /**
     * @var ContentfulData
     */
    private ContentfulData $contentfulData;

    /**
     * @param CategoryFactory $categoryFactory
     * @param ModelCategory $modelCategory
     * @param SitemapItemInterfaceFactory $itemInterfaceFactory
     * @param ConfigReaderInterface $configReader
     * @param StoreManagerInterface $storeManager
     * @param EavConfig $eavConfig
     * @param ContentfulData $contentfulData
     */
    public function __construct(
        CategoryFactory             $categoryFactory,
        ModelCategory               $modelCategory,
        SitemapItemInterfaceFactory $itemInterfaceFactory,
        ConfigReaderInterface       $configReader,
        StoreManagerInterface       $storeManager,
        EavConfig                   $eavConfig,
        ContentfulData              $contentfulData
    ) {
        $this->categoryFactory = $categoryFactory;
        $this->modelCategory = $modelCategory;
        $this->itemInterfaceFactory = $itemInterfaceFactory;
        $this->configReader = $configReader;
        $this->storeManager = $storeManager;
        $this->eavConfig = $eavConfig;
        $this->contentfulData = $contentfulData;
    }

    /**
     * @param \Magento\Sitemap\Model\ItemProvider\Category $subject
     * @param callable $proceed
     * @param $storeId
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundGetItems(\Magento\Sitemap\Model\ItemProvider\Category $subject, callable $proceed, $storeId): array
    {
        $enStore = $this->contentfulData->getEnStore();
        $collection = $this->categoryFactory->create()->getCollection($storeId);
        $attrColor = $this->eavConfig->getAttribute("catalog_product", "color");
        $attrSize = $this->eavConfig->getAttribute("catalog_product", "size");
        $colorOptions = $attrColor->setStoreId($enStore->getStoreId())->getSource()->getAllOptions();
        $sizeOptions = $attrSize->setStoreId($enStore->getStoreId())->getSource()->getAllOptions();
        $storeCode = $this->storeManager->getStore($storeId)->getCode();
        if (str_contains($storeCode, "ru")) {
            $ru = "ru/";
        } else {
            $ru = '';
        }

        $items = [];
        foreach ($collection as $item) {
            $categoryUrl = $this->modelCategory->create()->load($item->getId())->getCategoryUrl();

            if (isset($ru)) {
                $categoryUrl = $ru . $categoryUrl;
            }
            foreach ($sizeOptions as $sizeOption) {

                if ($sizeOption['label'] !== " ") {
                    $filter = "/filter-size-";
                    $label = mb_strtolower($sizeOption['label']);
                } else {
                    $filter = '';
                    $label = '';
                }
                $categoryPath = $categoryUrl . $filter . $label . "/";

                $items[] = $this->itemInterfaceFactory->create([
                    'url' => $categoryPath,
                    'updatedAt' => $item->getUpdatedAt(),
                    'images' => $item->getImages(),
                    'priority' => $this->configReader->getPriority($storeId),
                    'changeFrequency' => $this->configReader->getChangeFrequency($storeId)
                ]);
                $filter = '';
                $label = '';
            }

            foreach ($colorOptions as $colorOption) {

                if ($colorOption['label'] !== " ") {
                    $filterColor = "/filter-color-";
                    $labelColor = mb_strtolower($colorOption['label']);
                } else {
                    $filterColor = '';
                    $labelColor = '';
                }

                if ($colorOption['label'] !== " ") {
                    foreach ($sizeOptions as $sizeOption) {

                        if ($sizeOption['label'] !== " ") {
                            $size = "size";
                            $labelSize = mb_strtolower($sizeOption['label']);
                            $dash = "-";
                        } else {
                            $filter = '';
                            $labelSize = '';
                            $dash = '';
                            $size = '';
                        }

                        $categoryPath = $categoryUrl . $filterColor . $labelColor . $dash . $size . $dash . $labelSize . "/";

                        $items[] = $this->itemInterfaceFactory->create([
                            'url' => $categoryPath,
                            'updatedAt' => $item->getUpdatedAt(),
                            'images' => $item->getImages(),
                            'priority' => $this->configReader->getPriority($storeId),
                            'changeFrequency' => $this->configReader->getChangeFrequency($storeId)
                        ]);

                    }
                    $dash = '';
                    $size = '';
                    $filterColor = '';
                    $labelColor = '';
                }
            }
        }
        return $items;
    }
}
