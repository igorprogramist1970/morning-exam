<?php

namespace IdeaInYou\SyncToContentful\Plugin\CategorySiteMap\ItemProvider;

use Magento\Sitemap\Model\ItemProvider\CmsPage;
use Magento\Sitemap\Model\ItemProvider\ConfigReaderInterface;
use Magento\Sitemap\Model\ResourceModel\Cms\PageFactory;
use Magento\Sitemap\Model\SitemapItemInterfaceFactory;
use Magento\Cms\Model\PageFactory as ModelPage;

class CmsPageSiteMap
{
    /**
     * @var ModelPage
     */
    private ModelPage $pageFactory;

    /**
     * @var ConfigReaderInterface
     */
    private ConfigReaderInterface $configReader;

    /**
     * @var PageFactory
     */
    private PageFactory $cmsPageFactory;

    /**
     * @var SitemapItemInterfaceFactory
     */
    private SitemapItemInterfaceFactory $itemFactory;

    /**
     * @param ConfigReaderInterface $configReader
     * @param PageFactory $cmsPageFactory
     * @param SitemapItemInterfaceFactory $itemFactory
     * @param ModelPage $pageFactory
     */
    public function __construct(
        ConfigReaderInterface       $configReader,
        PageFactory                 $cmsPageFactory,
        SitemapItemInterfaceFactory $itemFactory,
        ModelPage                   $pageFactory
    ) {
        $this->pageFactory = $pageFactory;
        $this->configReader = $configReader;
        $this->cmsPageFactory = $cmsPageFactory;
        $this->itemFactory = $itemFactory;
    }

    /**
     * @param CmsPage $subject
     * @param callable $proceed
     * @param $storeId
     * @return array|\Magento\Sitemap\Model\SitemapItemInterface[]
     */
    public function aroundGetItems(CmsPage $subject, callable $proceed, $storeId): array
    {
        $collection = $this->cmsPageFactory->create()->getCollection($storeId);
        return array_map(function ($item) use ($storeId) {
            $pageUrl = $this->pageFactory->create()->load($item->getId())->getIdentifier();
            return $this->itemFactory->create([
                'url' => $pageUrl . "/",
                'updatedAt' => $item->getUpdatedAt(),
                'images' => $item->getImages(),
                'priority' => $this->configReader->getPriority($storeId),
                'changeFrequency' => $this->configReader->getChangeFrequency($storeId),
            ]);
        }, $collection);
    }
}
