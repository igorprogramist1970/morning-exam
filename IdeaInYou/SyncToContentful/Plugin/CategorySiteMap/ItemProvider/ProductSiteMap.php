<?php

namespace IdeaInYou\SyncToContentful\Plugin\CategorySiteMap\ItemProvider;

use Magento\Sitemap\Model\ItemProvider\ConfigReaderInterface;
use Magento\Sitemap\Model\ItemProvider\Product;
use Magento\Sitemap\Model\ResourceModel\Catalog\ProductFactory;
use Magento\Sitemap\Model\SitemapItemInterfaceFactory;
use Magento\Catalog\Model\ProductFactory as ModelProduct;

class ProductSiteMap
{
    /**
     * @var ProductFactory
     */
    private ProductFactory $productFactory;

    /**
     * @var ConfigReaderInterface
     */
    private ConfigReaderInterface $configReader;

    /**
     * @var SitemapItemInterfaceFactory
     */
    private SitemapItemInterfaceFactory $itemFactory;

    /**
     * @var ModelProduct
     */
    private ModelProduct $modelProduct;

    /**
     * @param ConfigReaderInterface $configReader
     * @param ProductFactory $productFactory
     * @param SitemapItemInterfaceFactory $itemFactory
     * @param ModelProduct $modelProduct
     */
    public function __construct(
        ConfigReaderInterface       $configReader,
        ProductFactory              $productFactory,
        SitemapItemInterfaceFactory $itemFactory,
        ModelProduct                $modelProduct
    ) {
        $this->productFactory = $productFactory;
        $this->configReader = $configReader;
        $this->itemFactory = $itemFactory;
        $this->modelProduct = $modelProduct;
    }

    /**
     * @param Product $subject
     * @param callable $proceed
     * @param $storeId
     * @return array|\Magento\Sitemap\Model\SitemapItemInterface[]
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     * @throws \Zend_Db_Statement_Exception
     */
    public function aroundGetItems(Product $subject, callable $proceed, $storeId): array
    {
        $collection = $this->productFactory->create()
            ->getCollection($storeId);

        return array_map(function ($item) use ($storeId) {
            $productUrl = $this->modelProduct->create()->load($item->getId())->getUrlKey();
            return $this->itemFactory->create([
                'url' => $productUrl . "/",
                'updatedAt' => $item->getUpdatedAt(),
                'images' => $item->getImages(),
                'priority' => $this->configReader->getPriority($storeId),
                'changeFrequency' => $this->configReader->getChangeFrequency($storeId),
            ]);
        }, $collection);
    }
}
