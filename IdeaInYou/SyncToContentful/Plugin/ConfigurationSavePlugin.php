<?php

namespace IdeaInYou\SyncToContentful\Plugin;

use Magento\Config\Model\Config as MagentoConfig;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\ConfigurationData;
use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\ContentfulData;

class ConfigurationSavePlugin
{

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var ConfigurationData
     */
    private ConfigurationData $configurationData;

    /**
     * @var ContentfulData
     */
    private ContentfulData $contentfulData;


    /**
     * @param ConfigurationData $configurationData
     * @param ValidateContentfulInterface $validateContentful
     * @param ContentfulData $contentfulData
     */
    public function __construct(
        ConfigurationData           $configurationData,
        ValidateContentfulInterface $validateContentful,
        ContentfulData $contentfulData
    ) {
        $this->validateContentful = $validateContentful;
        $this->configurationData = $configurationData;
        $this->contentfulData = $contentfulData;
    }

    /**
     * @param MagentoConfig $subject
     * @param \Closure $proceed
     * @return mixed
     */
    public function aroundSave(MagentoConfig $subject, \Closure $proceed)
    {
        if ($this->validateContentful->isEnable()) {
            try {
                $this->contentfulData->checkIfLocaleContentfulIsEmpty();
            } catch (\Exception $e){
            }

            $this->configurationData->syncSectionFields($subject->getData());
        }
        return $proceed();
    }
}
