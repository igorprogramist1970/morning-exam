<?php

namespace IdeaInYou\SyncToContentful\Plugin\Page\Model\ResourceModel;

use Magento\Cms\Model\ResourceModel\Page;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\AbstractModel;
use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\SyncToContentful\Model\Page\PageUpdate;

class PageDelete
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var PageUpdate
     */
    private PageUpdate $pageUpdate;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param PageUpdate $pageUpdate
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        PageUpdate $pageUpdate
    ) {
        $this->validateContentful = $validateContentful;
        $this->pageUpdate = $pageUpdate;
    }

    /**
     * @param Page $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function afterDelete(Page $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->pageUpdate->updatePageEntryContentful($object);
        return $result;
    }
}
