<?php

namespace IdeaInYou\SyncToContentful\Plugin\Page\Model\ResourceModel;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Cms\Model\ResourceModel\Page;

class PageBeforeSave
{
    const TYPE_URL_REWRITE = 'urlRewrite';

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Page $subject
     * @return void
     */
    public function beforeSave(Page $subject)
    {
        $this->validateContentful->validator(self::TYPE_URL_REWRITE);
    }
}
