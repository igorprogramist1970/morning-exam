<?php

namespace IdeaInYou\SyncToContentful\Plugin\Page\Model\ResourceModel;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Backend\Model\View\Result\Redirect;
use Magento\Cms\Controller\Adminhtml\Page\MassDelete;
use Magento\Framework\Controller\ResultFactory;
use Magento\Backend\App\Action\Context;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Cms\Model\ResourceModel\Page\CollectionFactory;

class PageBeforeMassDelete extends MassDelete
{
    /**
     * @var ResultFactory
     */
    protected $resultFactory;

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        Context                     $context,
        Filter                      $filter,
        CollectionFactory           $collectionFactory,
        ValidateContentfulInterface $validateContentful
    ) {
        parent::__construct($context, $filter, $collectionFactory);
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param MassDelete $subject
     * @param \Closure $proceed
     * @return \Magento\Framework\Controller\Result\Redirect|\Magento\Framework\Controller\Result\Redirect&\Magento\Framework\Controller\ResultInterface|mixed
     */
    public function aroundExecute(MassDelete $subject, \Closure $proceed)
    {
        try {
            $this->validateContentful->validator(PageBeforeSave::TYPE_URL_REWRITE);
            return $proceed();
        } catch (\Exception $e) {
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            return $resultRedirect->setPath('*/*/');
        }
    }
}
