<?php

namespace IdeaInYou\SyncToContentful\Plugin\Page\Model\ResourceModel;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use Magento\Cms\Model\ResourceModel\Page;

class PageBeforeDelete
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param Page $subject
     * @return void
     */
    public function beforeDelete(Page $subject)
    {
        $this->validateContentful->validator(PageBeforeSave::TYPE_URL_REWRITE);
    }
}
