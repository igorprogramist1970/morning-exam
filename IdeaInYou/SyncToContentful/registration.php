<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    'IdeaInYou_SyncToContentful',
    __DIR__
);
