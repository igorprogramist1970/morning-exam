<?php

namespace IdeaInYou\SyncToContentful\Cron;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Process\PhpExecutableFinderFactory;
use Magento\Framework\Shell;
use Magento\Framework\Shell\CommandRenderer;
use Psr\Log\LoggerInterface;
use Symfony\Component\Process\PhpExecutableFinder;

class ApplyChanges
{
    /**
     * @var LoggerInterface
     */
    protected LoggerInterface $logger;
    /**
     * @var Shell
     */
    protected Shell $shell;

    /**
     * @var PhpExecutableFinder
     */
    private PhpExecutableFinder $phpExecutableFinder;

    /**
     * @param LoggerInterface $logger
     * @param PhpExecutableFinderFactory $phpExecutableFinderFactory
     */
    public function __construct(
        LoggerInterface            $logger,
        PhpExecutableFinderFactory $phpExecutableFinderFactory
    ) {
        $this->logger = $logger;
        $this->shell = new Shell(new CommandRenderer());
        $this->phpExecutableFinder = $phpExecutableFinderFactory->create();
    }

    /**
     * @param $commands
     * @return void
     * @throws LocalizedException
     */
    public function execute($commands = ['save:allentry'])
    {
        $phpPath = $this->phpExecutableFinder->find() ?: 'php';
        foreach ($commands as $command) {
            $this->shell->execute('%s %s %s', [$phpPath, BP . '/bin/magento', $command]);
        }
        $this->logger->info('Cron Works');
    }
}
