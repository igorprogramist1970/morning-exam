<?php

namespace IdeaInYou\SyncToContentful\Block;

use Magento\Backend\Block\Template\Context;
use Magento\Catalog\Block\Adminhtml\Category\AbstractCategory;
use Magento\Catalog\Model\CategoryFactory;
use Magento\Catalog\Model\ResourceModel\Category\Tree;
use Magento\Framework\Data\Tree\Node;
use Magento\Framework\Registry;
use Magento\Store\Model\StoreManagerInterface;

class CustomCategoryCollection extends AbstractCategory
{
    /**
     * @var \Magento\Catalog\Block\Adminhtml\Category\Tree|Adminhtml\Category\Tree
     */
    private \Magento\Catalog\Block\Adminhtml\Category\Tree $tree;
    /**
     * @var StoreManagerInterface
     */
    private \Magento\Store\Model\StoreManagerInterface $storeManager;

    /**
     * @param Context $context
     * @param Tree $categoryTree
     * @param Registry $registry
     * @param CategoryFactory $categoryFactory
     * @param Adminhtml\Category\Tree $tree
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param array $data
     */
    public function __construct(
        Context                 $context,
        Tree                    $categoryTree,
        Registry                $registry,
        CategoryFactory         $categoryFactory,
        Adminhtml\Category\Tree $tree,
        StoreManagerInterface   $storeManager,
        array                   $data = []
    ) {
        parent::__construct($context, $categoryTree, $registry, $categoryFactory, $data);
        $this->tree = $tree;
        $this->storeManager = $storeManager;
    }

    /**
     * Get tree
     *
     * @param mixed|null $parenNodeCategory
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getTree($parenNodeCategory = null)
    {
        $rootArray = $this->tree->_getNodeJson($this->getRoot($parenNodeCategory));
        return $rootArray['children'] ?? [];
    }

    /**
     * Get root category for tree
     *
     * @param mixed|null $parentNodeCategory
     * @param int $recursionLevel
     * @return Node|array|null
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getRoot($parentNodeCategory = null, $recursionLevel = 3)
    {
        if ($parentNodeCategory !== null && $parentNodeCategory->getId()) {
            return $this->getNode($parentNodeCategory, $recursionLevel);
        }
        $root = $this->_coreRegistry->registry('root');
        if ($root === null) {
            $storeId = (int)$this->getRequest()->getParam('store');

            if ($storeId) {
                $store = $this->_storeManager->getStore($storeId);
                $rootId = $store->getRootCategoryId();
            } else {
                $rootId = \Magento\Catalog\Model\Category::TREE_ROOT_ID;
            }

            $tree = $this->_categoryTree->load(null, $recursionLevel);

            if ($this->getCategory()) {
                $tree->loadEnsuredNodes((object)$this->getCategory(), $tree->getNodeById($rootId));
            }

            $customCategoryCollection = $this->_coreRegistry->registry('storeCategoryCollection');

            $tree->addCollectionData($customCategoryCollection);

            $root = $tree->getNodeById($rootId);

            if ($root) {
                $root->setIsVisible(true);
                if ($root->getId() == \Magento\Catalog\Model\Category::TREE_ROOT_ID) {
                    $root->setName(__('Root'));
                }
            }

            $this->_coreRegistry->register('root', $root);
        }

        return $root;
    }

    /**
     * Get category collection
     *
     * @return \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getCategoryCollection()
    {
        $storeId = $this->storeManager->getStore()->getId();
        //$storeId = $this->getRequest()->getParam('store', $this->_getDefaultStoreId());
        $collection = $this->getData('category_collection');
        if ($collection === null) {
            $collection = $this->_categoryFactory->create()->getCollection();

            $collection->addAttributeToSelect(
                'name'
            )->addAttributeToSelect(
                'is_active'
            )->addAttributeToSelect(
                'category_url'
            )->setProductStoreId(
                $storeId
            )->setLoadProductCount(
                $this->_withProductCount
            )->setStoreId(
                $storeId
            );

            $this->setData('category_collection', $collection);
        }
        return $collection;
    }
}
