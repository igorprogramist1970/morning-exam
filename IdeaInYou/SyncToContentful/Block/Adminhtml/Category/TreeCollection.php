<?php

namespace IdeaInYou\SyncToContentful\Block\Adminhtml\Category;

use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory;
use Magento\Framework\Registry;

class TreeCollection
{
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $categoryCollection;

    /**
     * @param CollectionFactory $categoryCollection
     */
    public function __construct(
        CollectionFactory $categoryCollection
    ) {
        $this->categoryCollection = $categoryCollection;
    }

    /**
     * @param $store
     * @return \Magento\Catalog\Model\ResourceModel\Category\Collection
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getCategoryCollectionPerStore($store)
    {
        return $this->categoryCollection->create()
            ->addAttributeToSelect('*')
            ->setStore($store)
            ->addAttributeToFilter('is_active', '1');
    }

}
