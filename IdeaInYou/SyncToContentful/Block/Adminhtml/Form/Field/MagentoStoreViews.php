<?php

namespace IdeaInYou\SyncToContentful\Block\Adminhtml\Form\Field;

use Magento\Framework\View\Element\Html\Select;
use IdeaInYou\SyncToContentful\Model\ConnectToContentful\ContentfulData;
use Magento\Framework\View\Element\Context;

class MagentoStoreViews extends Select
{
    /**
     * @var ContentfulData
     */
    private ContentfulData $contentfulData;

    public function __construct(
        Context        $context,
        ContentfulData $contentfulData,
        array          $data = []
    ) {
        parent::__construct($context, $data);
        $this->contentfulData = $contentfulData;
    }

    /**
     * Set "name" for <select> element
     *
     * @param string $value
     * @return $this
     */
    public function setInputName($value)
    {
        return $this->setName($value);
    }

    /**
     * Set "id" for <select> element
     *
     * @param $value
     * @return $this
     */
    public function setInputId($value)
    {
        return $this->setId($value);
    }

    /**
     * Render block HTML
     *
     * @return string
     */
    public function _toHtml()
    {
        if (!$this->getOptions()) {
            $this->setOptions($this->getSourceOptions());
        }
        return parent::_toHtml();
    }

    /**
     * @return array
     */
    private function getSourceOptions(): array
    {
        $arr[] = [
            'value' => '',
            'label' => __('-- Select an Store View --')
        ];
        try {
            $stores = $this->contentfulData->getAllStores();
            foreach ($stores as $store) {
//                $label = $this->contentfulData->getStoreLangByCode($store->getCode());
//                if (!$label) continue;
                $arr[] = [
                    'value' => $store->getCode(),
                    'label' => __($store->getCode())
                ];
            }
        } catch (\Exception $e) {

        }

        return $arr;
    }
}
