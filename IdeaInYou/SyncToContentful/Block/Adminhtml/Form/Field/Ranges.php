<?php

namespace IdeaInYou\SyncToContentful\Block\Adminhtml\Form\Field;

use Magento\Config\Block\System\Config\Form\Field\FieldArray\AbstractFieldArray;
use Magento\Framework\DataObject;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\View\Element\BlockInterface;

class Ranges extends AbstractFieldArray
{
    /**
     * @return void
     * @throws LocalizedException
     */
    protected function _prepareToRender()
    {
        $this->addColumn('store_view', [
            'label' => __('Store View'),
            'renderer' => $this->getStoreViewRenderer()
        ]);
        $this->addColumn('locale_contentful', [
            'label' => __('Locales'),
            'renderer' => $this->getLocalesRenderer()
        ]);
        $this->_addAfter = false;
        $this->_addButtonLabel = __('Add Row');
    }

    /**
     * Prepare existing row data object
     *
     * @param DataObject $row
     * @throws LocalizedException
     */
    protected function _prepareArrayRow(DataObject $row)
    {
        $options = [];
        $locales = $row->getLocales();
        if ($locales !== null) {
            $options['option_' . $this->getLocalesRenderer()->calcOptionHash($locales)] = 'selected="selected"';
        }

        $storeView = $row->getStoreView();
        if ($locales !== null) {
            $options['option_' . $this->getStoreViewRenderer()->calcOptionHash($storeView)] = 'selected="selected"';
        }

        $row->setData('option_extra_attrs', $options);
    }

    /**
     * @return ContentfulLocales|ContentfulLocales&BlockInterface|BlockInterface
     * @throws LocalizedException
     */
    private function getLocalesRenderer()
    {
        return $this->getLayout()->createBlock(
            ContentfulLocales::class,
            '',
            ['data' => ['is_render_to_js_template' => true]]
        );
    }


    /**
     * @return MagentoStoreViews|MagentoStoreViews&BlockInterface|BlockInterface
     * @throws LocalizedException
     */
    private function getStoreViewRenderer()
    {
        return $this->getLayout()->createBlock(
            MagentoStoreViews::class,
            '',
            ['data' => ['is_render_to_js_template' => true]]
        );
    }
}
