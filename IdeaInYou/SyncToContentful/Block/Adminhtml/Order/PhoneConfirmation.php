<?php

namespace IdeaInYou\SyncToContentful\Block\Adminhtml\Order;

use Magento\Backend\Block\Template;
use \Magento\Backend\Block\Template\Context;
use \Magento\Sales\Model\Order;

class PhoneConfirmation extends Template
{
    private Order $order;

    /**
     * @param Context $context
     * @param Order $order
     * @param array $data
     */
    public function __construct(
        Context $context,
        Order $order,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->order = $order;
    }

    public function getOrderId()
    {
        $orderId = $this->getRequest()->getParam('order_id');
        return $orderId;
    }

    public function getPhoneConfirmation()
    {
        $orderId = $this->getOrderId();
        $customfieldvalue = $this->order->load($orderId)->getData('phoneConfirmation');
        return $customfieldvalue;
    }

    public function myFunction()
    {
        //your code
        return "Customers' Instruction";
    }

}
