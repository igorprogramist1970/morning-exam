<?php

use Magento\Framework\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'IdeaInYou_CheckoutGraphQl', __DIR__);
