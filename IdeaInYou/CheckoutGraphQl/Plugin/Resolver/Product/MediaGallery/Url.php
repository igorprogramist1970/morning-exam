<?php

namespace IdeaInYou\CheckoutGraphQl\Plugin\Resolver\Product\MediaGallery;

use Magento\Catalog\Model\Product;
use Magento\CatalogGraphQl\Model\Resolver\Product\MediaGallery\Url as MagentoGalleryUrl;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;

/**
 * Class Url
 * @package IdeaInYou\SyncToContentful\Plugin\Resolver\Product\MediaGallery
 */
class Url
{
    /**
     * @param MagentoGalleryUrl $subject
     * @param callable $proceed
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return mixed|null
     */
    public function aroundResolve(
        MagentoGalleryUrl $subject,
        callable $proceed,
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    ) {
        $result = $proceed($field, $context, $info, $value, $args);

        /** @var Product $product */
        $product = $value['model'];
        if (isset($value['image_type'])) {
            return $product->getData($value['image_type']);
        }

        return $result;
    }
}
