<?php

namespace IdeaInYou\CheckoutGraphQl\Api;

use IdeaInYou\CheckoutGraphQl\Model\NovaPoshta\NovaPoshtaData;

interface NovaPoshtaDataInterface
{
    /**
     * @return mixed
     */
    public function getApiKey();

    /**
     * @api
     * @param string $findByString
     * @return \IdeaInYou\CheckoutGraphQl\Model\NovaPoshta\NovaPoshtaData
     *
     */
    public function getCities($findByString);

    /**
     * @param $cityId
     * @return void
     */
    public function getWarehouses($cityId);

    /**
     * @api
     * @param string $searchStringArray
     * @return \IdeaInYou\CheckoutGraphQl\Model\NovaPoshta\NovaPoshtaData
     *
     */
    public function findNearestWarehouse($searchStringArray);
}
