<?php

namespace IdeaInYou\CheckoutGraphQl\Api;

use IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\UkrPoshtaData;

interface UkrPoshtaDataInterface
{
    /**
     * @api
     * @return \IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\UkrPoshtaData
     *
     */
    public function getRegions();

    /**
     * @api
     * @param string $regionId
     * @return \IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\UkrPoshtaData
     *
     */
    public function getDistrict($regionId);

    /**
     * @api
     * @param string $city
     * @return \IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\UkrPoshtaData
     *
     */
    public function getCity($city);

    /**
     * @api
     * @param string $cityId
     * @return \IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\UkrPoshtaData
     *
     */
    public function getPostofficeByCity($cityId);

    /**
     * @api
     * @param string $postIndex
     * @return \IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\UkrPoshtaData
     *
     */
    public function getPostofficeByIndex($postIndex);
}
