<?php

namespace IdeaInYou\CheckoutGraphQl\Model\NovaPoshta;

use IdeaInYou\CheckoutGraphQl\Api\NovaPoshtaDataInterface;
use LisDev\Delivery\NovaPoshtaApi2Factory;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\View\Element\Template;

class NovaPoshtaData extends Template implements NovaPoshtaDataInterface
{
    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    const NOVA_POSHTA_API_KEY = 'novaposhtaapi/api/api_key';
    /**
     * @var NovaPoshtaApi2Factory
     */
    private NovaPoshtaApi2Factory $novaPoshtaApi2;

    /**
     * @param Template\Context $context
     * @param ScopeConfigInterface $scopeConfig
     * @param NovaPoshtaApi2Factory $novaPoshtaApi2
     * @param array $data
     */
    public function __construct(
        Template\Context      $context,
        ScopeConfigInterface  $scopeConfig,
        NovaPoshtaApi2Factory $novaPoshtaApi2,
        array                 $data = []
    ) {
        parent::__construct($context, $data);
        $this->scopeConfig = $scopeConfig;
        $this->novaPoshtaApi2 = $novaPoshtaApi2;
    }

    /**
     * @return \LisDev\Delivery\NovaPoshtaApi2
     */
    public function getApiKey()
    {
        $apiKey = $this->scopeConfig->getValue(self::NOVA_POSHTA_API_KEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        return $this->novaPoshtaApi2->create(["key" => $apiKey]);
    }

    /**
     * @param $findByString
     * @return array|mixed|string
     */
    public function getCities($findByString)
    {
        $np = $this->getApiKey();
        $items = $np->getCities($page = 0, $findByString);
        $data = [];
        foreach ($items['data'] as $item) {
            $data[] = ['value' => $item["SettlementTypeDescription"] . " " . $item["Description"] . ", " . $item['AreaDescription'] . ' ' . "область"];
        }
        return $data;
    }

    /**
     * @param $searchStringArray
     * @return array|NovaPoshtaData|mixed|string
     */
    public function findNearestWarehouse($searchStringArray)
    {
        $np = $this->getApiKey();
        $cityRefs = $np->findNearestWarehouse($searchStringArray);
        $cityId = $cityRefs['data'][0][0][0]['CityRef'];
        $warehouses = $np->getWarehouses($cityId)['data'];
        $data = [];
        foreach ($warehouses as $warehouse) {
            $data[] = ['value' => $warehouse['Description']];
        }
        return $data;
    }

    /**
     * @param $cityId
     * @return array|mixed|string
     */
    public function getWarehouses($cityId)
    {
        $np = $this->getApiKey();
        return $np->getWarehouses($cityId);
    }

}
