<?php

namespace IdeaInYou\CheckoutGraphQl\Model\UkrPoshta;

use IdeaInYou\CheckoutGraphQl\Api\UkrPoshtaDataInterface;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\DataObjectFactory;
use Magento\Framework\Serialize\Serializer\Json;
use Ukrposhta\Data\ConfigurationFactory;
use Ukrposhta\Data\StorageFactory;

class UkrPoshtaData implements UkrPoshtaDataInterface
{
    /**
     *
     */
    const UKR_POSHTA_CACHE_LIST = 'cacheList_for_UKR_POSTA';
    /**
     *
     */
    const UKR_POSHTA_BEARER_KEY = 'ukrposhtaapi/api/bearer_key';
    /**
     *
     */
    const UKR_POSHTA_TOKEN_KEY = 'ukrposhtaapi/api/token_key';

    /**
     * @var ConfigurationFactory
     */
    private ConfigurationFactory $ukrposhta;

    /**
     * @var StorageFactory
     */
    private StorageFactory $storageFactory;

    /**
     * @var ScopeConfigInterface
     */
    private ScopeConfigInterface $scopeConfig;

    /**
     * @var DataObjectFactory
     */
    private DataObjectFactory $dataObjectFactory;

    /**
     * @var CacheInterface
     */
    private CacheInterface $cache;

    /**
     * @var Json
     */
    private Json $serializer;

    /**
     * @param ConfigurationFactory $ukrposhta
     * @param StorageFactory $storageFactory
     * @param ScopeConfigInterface $scopeConfig
     * @param DataObjectFactory $dataObjectFactory
     * @param CacheInterface $cache
     * @param Json $serializer
     */
    public function __construct(
        ConfigurationFactory    $ukrposhta,
        StorageFactory          $storageFactory,
        ScopeConfigInterface    $scopeConfig,
        DataObjectFactory       $dataObjectFactory,
        CacheInterface          $cache,
        Json                    $serializer
    ) {
        $this->ukrposhta = $ukrposhta;
        $this->storageFactory = $storageFactory;
        $this->scopeConfig = $scopeConfig;
        $this->dataObjectFactory = $dataObjectFactory;
        $this->cache = $cache;
        $this->serializer = $serializer;
    }

    /**
     * @return \Ukrposhta\Data\Configuration
     */
    public function conectApi()
    {
        $config = $this->ukrposhta->create();
        $bearerKey = $this->scopeConfig->getValue(self::UKR_POSHTA_BEARER_KEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $tokenKey = $this->scopeConfig->getValue(self::UKR_POSHTA_TOKEN_KEY,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
        $config->setBearer($bearerKey);
        $config->setToken($tokenKey);

        return $config;
    }

    /**
     * @param $district
     * @return mixed
     */
    public function getDistrictId($district)
    {
        $config = $this->conectApi();
        $params = new \Ukrposhta\Data\Storage();
        $params->district_ua = $district;
        $district = new \Ukrposhta\Directory\District($config);
        return $district->getList($params)['Entry']['DISTRICT_ID'];
    }

    /**
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getCity($districtName)
    {
        $config = $this->conectApi();
        $district = str_replace(" район", "", $districtName);
        $districtId = $this->getDistrictId($district);
        $params = new \Ukrposhta\Data\Storage();
        $params->district_id = $districtId;
        $cities = new \Ukrposhta\Directory\City($config);
        $listCities = $cities->getList($params);


        $data = [];
        $cityList = [];
        if (array_key_exists("CITY_UA", $listCities['Entry'])) {
            return $listCities['Entry']["CITYTYPE_UA"] . " " . $listCities['Entry']["CITY_UA"];
        } else {
            foreach ($listCities['Entry'] as $listCity) {
                $data[] = ['value' => $listCity["CITYTYPE_UA"] . " " . $listCity["CITY_UA"]
                    . ", " . $listCity['DISTRICT_UA'] . " район"];
                $cityList[$listCity['CITY_ID']] = $listCity["CITYTYPE_UA"] . " " . $listCity["CITY_UA"]
                    . ", " . $listCity['DISTRICT_UA'] . " район";
            }
            $cacheList = $this->serializer->serialize($cityList);
            $this->cache->save($cacheList, self::UKR_POSHTA_CACHE_LIST, [], 7200);

            return $data;
        }
    }

    /**
     * @param $regionName
     * @return mixed
     */
    public function getRegionId($regionName)
    {
        $config = $this->conectApi();
        $region = str_replace(" область", "", $regionName);
        $params = new \Ukrposhta\Data\Storage();
        $params->region_name = $region;
        $regions = new \Ukrposhta\Directory\Region($config);
        return $regions->getList($params)['Entry']['REGION_ID'];
    }

    /**
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getDistrict($regionName)
    {
        $config = $this->conectApi();
        $regionId = $this->getRegionId($regionName);
        $params = new \Ukrposhta\Data\Storage();
        $params->region_id = $regionId;
        $district = new \Ukrposhta\Directory\District($config);
        $listDistricts = $district->getList($params);
        $data = [];
        if (array_key_exists("DISTRICT_UA", $listDistricts['Entry'])) {
            return ["value" => $listDistricts['Entry']["DISTRICT_UA"] . ' район'];
        } else {
            foreach ($listDistricts['Entry'] as $listDistrict) {
                $data[] = ["value" => $listDistrict['DISTRICT_UA'] . ' район'];
            }
            return $data;
        }
    }



    /**
     * @param $cityId
     * @return false|int|string
     */
    public function getCityId($cityId)
    {
        $cityList = $this->serializer->unserialize(
            $this->cache->load(self::UKR_POSHTA_CACHE_LIST)
        );
        return array_search($cityId, $cityList);
    }



    /**
     * @param $cityId
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getPostofficeByCity($cityId)
    {
        $config = $this->conectApi();
        $cityIdEntity = $this->getCityId($cityId);
        $postoffice = new \Ukrposhta\Directory\Postoffice($config);
        $postoffices = $postoffice->getByCityId($cityIdEntity);
        $data = [];
        if (empty($postoffices)) {
            return [];
        } else {
            if (array_key_exists("DISTRICT_UA", $postoffices['Entry'])) {
                return $postoffices['Entry']["PO_LONG"];
            } else {
                foreach ($postoffices['Entry'] as $postoff) {
                    $data[] = ['value' => $postoff['PO_LONG']];
                }
                return $data;
            }
        }
    }

    /**
     * @param $postIndex
     * @return array
     */
    public function getPostofficeByIndex($postIndex)
    {
        $config = $this->conectApi();
        $postoffice = new \IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\PostofficeExtension($config);
        $postoffices = $postoffice->getByPostIndexExtension((string)$postIndex);
        $data = [];
        if (empty($postoffices)) {
            return [];
        } else {
            try {
                $data[] = ['value' => $postoffices['Entry']['PO_LONG']];
            } catch (\Exception $e) {
                foreach ($postoffices['Entry'] as $postoffice) {
                    $tmp['value'][] = $postoffice['PO_LONG'];
                }
                $tmp = array_unique($tmp['value']);
                foreach ($tmp as $item) {
                    $data[] = ['value' => $item];
                }
            }
        }
        return $data;
    }

    /**
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getRegions()
    {
        $config = $this->conectApi();
        $regions = new \Ukrposhta\Directory\Region($config);
        $getAllRegions = $regions->getList()['Entry'];
        $data = [];
        foreach ($getAllRegions as $getRegion) {
            $data[] = ["value" => $getRegion['REGION_UA'] . ' ' . 'область'];
        }
        return $data;
    }

}
