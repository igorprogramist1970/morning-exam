<?php

namespace IdeaInYou\CheckoutGraphQl\Model\UkrPoshta;

use Ukrposhta\Data\Storage;

class PostofficeExtension extends \Ukrposhta\Directory\Postoffice
{
    /**
     * @param string $postIndex
     * @param Storage|null $params
     * @return mixed|\Psr\Http\Message\ResponseInterface
     */
    public function getByPostIndexExtension(string $postIndex, Storage $params = null)
    {
        if ($params === null) {
            $params = new Storage();
        }

        $url = $this->getUrl(function (string $url) {
            return $url . '/' . self::REQUEST_URL_GET_POSTOFFICE;
        });
        $params->pi = $postIndex;

        return $this->send($url, $params);
    }
}
