<?php

namespace IdeaInYou\CheckoutGraphQl\Model\Resolver;

use Fondy\Fondy\Block\Widget\Redirect as Fondy;
use IdeaInYou\CheckoutGraphQl\Model\NovaPoshta\NovaPoshtaData;
use IdeaInYou\CheckoutGraphQl\Model\UkrPoshta\UkrPoshtaData;
use Magento\Checkout\Model\Session;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\PaymentMethodManagementInterface;
use Magento\Quote\Model\Quote;
use Magento\QuoteGraphQl\Model\Cart\CheckCartCheckoutAllowance;
use Magento\QuoteGraphQl\Model\Cart\GetCartForUser;
use Magento\QuoteGraphQl\Model\Cart\SetBillingAddressOnCart as SetBillingAddressOnCartModel;
use Magento\QuoteGraphQl\Model\Cart\SetPaymentMethodOnCart as SetPaymentMethodOnCartModel;
use Magento\QuoteGraphQl\Model\Cart\SetShippingAddressesOnCartInterface;
use Magento\QuoteGraphQl\Model\Cart\SetShippingMethodsOnCartInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use function IdeaInYou\CheckoutGraphQl\Model\Resolver\__;

/**
 * Class CheckoutPayment
 * @package IdeaInYou\CheckoutGraphQl\Model\Resolver
 */
class PoshtaData implements ResolverInterface
{
    /**
     * @var Quote
     */
    private Quote $quote;
    /**
     * @var Fondy
     */
    private Fondy $fondy;
    /**
     * @var SetBillingAddressOnCartModel
     */
    private SetBillingAddressOnCartModel $setBillingAddressOnCart;

    /**
     * @var SetPaymentMethodOnCartModel
     */
    private SetPaymentMethodOnCartModel $setPaymentMethodOnCart;
    /**
     * @var SetShippingMethodsOnCartInterface
     */
    private SetShippingMethodsOnCartInterface $setShippingMethodsOnCart;
    /**
     * @var SetShippingAddressesOnCartInterface
     */
    private SetShippingAddressesOnCartInterface $setShippingAddressesOnCart;
    /**
     * @var CartManagementInterface
     */
    private CartManagementInterface $cartManagement;

    /**
     * @var GetCartForUser
     */
    private GetCartForUser $getCartForUser;

    /**
     * @var OrderRepositoryInterface
     */
    private OrderRepositoryInterface $orderRepository;

    /**
     * @var CheckCartCheckoutAllowance
     */
    private CheckCartCheckoutAllowance $checkCartCheckoutAllowance;

    /**
     * @var PaymentMethodManagementInterface
     */
    private PaymentMethodManagementInterface $paymentMethodManagement;
    /**
     * @var Session
     */
    protected Session $_checkoutSession;
    /**
     * @var NovaPoshtaData
     */
    private NovaPoshtaData $novaPoshtaData;
    /**
     * @var UkrPoshtaData
     */
    private UkrPoshtaData $ukrPoshtaData;


    /**
     * @param GetCartForUser $getCartForUser
     * @param CartManagementInterface $cartManagement
     * @param OrderRepositoryInterface $orderRepository
     * @param CheckCartCheckoutAllowance $checkCartCheckoutAllowance
     * @param PaymentMethodManagementInterface $paymentMethodManagement
     * @param SetShippingAddressesOnCartInterface $setShippingAddressesOnCart
     * @param SetShippingMethodsOnCartInterface $setShippingMethodsOnCart
     * @param SetPaymentMethodOnCartModel $setPaymentMethodOnCart
     * @param SetBillingAddressOnCartModel $setBillingAddressOnCart
     * @param Fondy $fondy
     * @param Session $checkoutSession
     * @param Quote $quote
     * @param NovaPoshtaData $novaPoshtaData
     * @param UkrPoshtaData $ukrPoshtaData
     */
    public function __construct(
        GetCartForUser                      $getCartForUser,
        CartManagementInterface             $cartManagement,
        OrderRepositoryInterface            $orderRepository,
        CheckCartCheckoutAllowance          $checkCartCheckoutAllowance,
        PaymentMethodManagementInterface    $paymentMethodManagement,
        SetShippingAddressesOnCartInterface $setShippingAddressesOnCart,
        SetShippingMethodsOnCartInterface   $setShippingMethodsOnCart,
        SetPaymentMethodOnCartModel         $setPaymentMethodOnCart,
        SetBillingAddressOnCartModel        $setBillingAddressOnCart,
        Fondy                               $fondy,
        Session                             $checkoutSession,
        Quote                               $quote,
        NovaPoshtaData                      $novaPoshtaData,
        UkrPoshtaData                       $ukrPoshtaData
    ) {
        $this->getCartForUser = $getCartForUser;
        $this->cartManagement = $cartManagement;
        $this->orderRepository = $orderRepository;
        $this->checkCartCheckoutAllowance = $checkCartCheckoutAllowance;
        $this->paymentMethodManagement = $paymentMethodManagement;
        $this->setShippingAddressesOnCart = $setShippingAddressesOnCart;
        $this->setShippingMethodsOnCart = $setShippingMethodsOnCart;
        $this->setPaymentMethodOnCart = $setPaymentMethodOnCart;
        $this->setBillingAddressOnCart = $setBillingAddressOnCart;
        $this->fondy = $fondy;
        $this->_checkoutSession = $checkoutSession;
        $this->quote = $quote;
        $this->novaPoshtaData = $novaPoshtaData;
        $this->ukrPoshtaData = $ukrPoshtaData;
    }

    /**
     * @param Field $field
     * @param $context
     * @param ResolveInfo $info
     * @param array|null $value
     * @param array|null $args
     * @return array[]|void
     * @throws GraphQlInputException
     */
    public function resolve(
        Field       $field,
                    $context,
        ResolveInfo $info,
        array       $value = null,
        array       $args = null
    )
    {
        $input = $args['input'];

        if ($input['type'] == 'novaposhta') {
            return $this->novaPoshta($input);
        } elseif ($input['type'] == 'ukrposhta') {
            return $this->ukrPoshta($input);
        }

    }

    public function novaPoshta($input)
    {
        if (empty($input['fieldType'])) {
            throw new GraphQlInputException(__('Required parameter "fieldType" is missing'));
        }
        $maskedCartId = $input['fieldType'];
        $data = [];
        if ($maskedCartId == "city") {
            $items = $this->novaPoshtaData->getCities($input['slug'])["data"];
            foreach ($items as $item) {
                $data[] = [
                    'label' => $item["SettlementTypeDescription"] . " " . $item["Description"] . ", " . $item['AreaDescription'] . ' ' . "область",
                    'value' => $item['Ref']
                ];
            }

            return ["list" => $data];
        } elseif ($maskedCartId == "warehouses") {
            $items = $this->novaPoshtaData->getWarehouses($input['slug'])["data"];

            foreach ($items as $item) {
                $data[] = [
                    'label' => $item['Description'],
                    'value' => $item['Ref']
                ];
            }

            return ["list" => $data];
        }
    }

    public function ukrPoshta($input)
    {
        if (empty($input['fieldType'])) {
            throw new GraphQlInputException(__('Required parameter "fieldType" is missing'));
        }

        $maskedCartId = $input['fieldType'];
        $data = [];
        if ($maskedCartId == "region") {
            $regions = $this->ukrPoshtaData->getRegions();
            foreach ($regions['Entry'] as $region) {
                $data[] = [
                    'label' => $region['REGION_UA'] . ' ' . 'область',
                    'value' => $region['REGION_ID']
                ];
            }

            return ["list" => $data];
        }

        if ($maskedCartId == "district") {
            $districts = $this->ukrPoshtaData->getDistrict($input['slug']);
            foreach ($districts['Entry'] as $district) {
                $data[] = [
                    'label' => $district['DISTRICT_UA'] . ' ' . 'район',
                    'value' => $district['DISTRICT_ID']
                ];
            }

            return ["list" => $data];
        }

        if ($maskedCartId == "city") {

            $cities = $this->ukrPoshtaData->getCity($input['slug']);
            foreach ($cities['Entry'] as $city) {
                $data[] = [
                    'label' => $city['CITYTYPE_UA'] . ' ' . $city['CITY_UA'],
                    'value' => $city['CITY_ID']
                ];
            }

            return ["list" => $data];
        }

        if ($maskedCartId == "postoffice") {
            $postoffice = $this->ukrPoshtaData->getPostofficeByCity($input['slug']);
            foreach ($postoffice['Entry'] as $item) {
                $data[] = [
                    'label' => $item['POSTINDEX'] . ', ' . $item['PO_LONG'],
                    'value' => $item['ID']
                ];
            }

            return ["list" => $data];
        }
    }
}
