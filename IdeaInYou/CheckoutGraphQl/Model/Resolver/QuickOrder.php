<?php

namespace IdeaInYou\CheckoutGraphQl\Model\Resolver;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\CustomerFactory;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Api\Data\ShippingMethodInterface;
use Magento\Quote\Model\Quote\Address\Rate;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteManagement;
use Magento\Sales\Model\Order\Email\Sender\OrderSender;
use Magento\Store\Model\StoreManagerInterface;


/**
 * Class QuickOrder
 * @package IdeaInYou\QuickOrderGraphQl\Model\Resolver
 */
class QuickOrder implements ResolverInterface
{
    /**
     *
     */
    const QUICK_ORDER_EMAIL = 'quickorder@morningstart.com';

    /**
     * @var StoreManagerInterface
     */
    protected $storeManager;
    /**
     * @var ProductRepositoryInterface
     */
    protected $productRepository;
    /**
     * @var CustomerRepositoryInterface
     */
    protected $customerRepository;
    /**
     * @var QuoteFactory
     */
    protected $quote;
    /**
     * @var QuoteManagement
     */
    protected $quoteManagement;
    /**
     * @var OrderSender
     */
    protected $orderSender;
    /**
     * @var CustomerFactory
     */
    protected $customerFactory;
    /**
     * @var Rate
     */
    private Rate $shippingRate;
    /**
     * @var ShippingMethodInterface
     */
    private ShippingMethodInterface $shippingMethod;


    /**
     * @param StoreManagerInterface $storeManager
     * @param ProductRepositoryInterface $productRepository
     * @param CustomerRepositoryInterface $customerRepository
     * @param QuoteFactory $quote
     * @param CustomerFactory $customerFactory
     * @param QuoteManagement $quoteManagement
     * @param OrderSender $orderSender
     * @param Rate $shippingRate
     * @param ShippingMethodInterface $shippingMethod
     */
    public function __construct(
        StoreManagerInterface       $storeManager,
        ProductRepositoryInterface  $productRepository,
        CustomerRepositoryInterface $customerRepository,
        QuoteFactory                $quote,
        CustomerFactory             $customerFactory,
        QuoteManagement             $quoteManagement,
        OrderSender                 $orderSender,
        Rate                        $shippingRate,
        ShippingMethodInterface     $shippingMethod
    ) {
        $this->storeManager = $storeManager;
        $this->productRepository = $productRepository;
        $this->customerRepository = $customerRepository;
        $this->quote = $quote;
        $this->quoteManagement = $quoteManagement;
        $this->orderSender = $orderSender;
        $this->customerFactory = $customerFactory;
        $this->shippingRate = $shippingRate;
        $this->shippingMethod = $shippingMethod;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field       $field,
                    $context,
        ResolveInfo $info,
        array       $value = null,
        array       $args = null
    )
    {
        return $this->createOrder($args);
    }

    /*
   * create order programmatically
   */
    /**
     * @param $orderInfo
     * @return array
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function createOrder($orderInfo)
    {
        $result = [];
        $inputData = $orderInfo['input'];
        $store = $this->getStoreByCode($inputData['store_code']);
        $websiteId = $store->getWebsiteId();
        $quote = $this->quote->create();
        $quote->setStore($store);
        $customer = $this->customerFactory->create();
        $customer->setWebsiteId($websiteId);
        $customer->loadByEmail(self::QUICK_ORDER_EMAIL);
        $nameArr = explode(' ', $inputData['full_name']);
        $name = $nameArr[0];
        $lastName = ltrim(str_replace($nameArr[0], '', $inputData['full_name']), ' ') ?? $nameArr[0];

        if (!$customer->getEntityId()) {
            $customer->setWebsiteId($websiteId)
                ->setStore($store)
                ->setFirstname($name)
                ->setLastname($lastName)
                ->setEmail(self::QUICK_ORDER_EMAIL)
                ->setPassword(self::QUICK_ORDER_EMAIL);
            $customer->save();
        }

        $product = $this->productRepository->getById($inputData['product_id']);

        if ($product->getTypeId() == 'configurable') {
            $childrenProduct = $this->productRepository->getById($inputData['simple_id']);
            $superAttribute = $this->getSuperAttributeData($childrenProduct, $product);
            $buyRequest = new \Magento\Framework\DataObject(['qty' => $inputData['qty'], 'super_attribute' => $superAttribute]);
            $quote->addProduct($childrenProduct, $buyRequest);
        } else {
            $quote->addProduct($product, intval($inputData['qty']));
        }

        $address = [
            'firstname' => $name,
            'lastname' => $lastName,
            'street' => 'street',
            'city' => 'Chernivtsi',
            'country_id' => 'UA',
            'regionId' => 'Chernivetska oblast',
            'region' => 'Chernivtsi',
            'postcode' => '58000',
            'telephone' => $inputData['telephone'],
            'fax' => 'fax'
        ];

        $customer = $this->customerRepository->getById($customer->getEntityId());
        $quote->assignCustomer($customer);
        $quote->getBillingAddress()->addData($address);
        $quote->getShippingAddress()->addData($address);

        // Collect Rates and Set Shipping & Payment Method
        $this->shippingRate
            ->setCode('novaposhta')
            ->getPrice(1);
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress->setCollectShippingRates(true)
            ->collectShippingRates()
            ->setShippingMethod('novaposhta');

        $quote->getShippingAddress()->addShippingRate($this->shippingRate);
        $quote->setPaymentMethod('checkmo');
        $quote->setInventoryProcessed(false);
        $quote->save();

        $quote->getPayment()->importData(['method' => 'checkmo']);
        $quote->collectTotals()->save();
        $order = $this->quoteManagement->submit($quote);
        $this->orderSender->send($order);
        $orderId = $order->getIncrementId();

        if ($orderId) {
            $result['status'] = true;
        } else {
            $result['status'] = false;
        }
        return $result;
    }

    /**
     * @param $code
     * @return \Magento\Store\Api\Data\StoreInterface
     */
    function getStoreByCode($code)
    {
        return $this->storeManager->getStores(true, $code)[$code];
    }

    /**
     * @param $childrenProduct
     * @param $parentProduct
     * @return array
     */
    public function getSuperAttributeData($childrenProduct, $parentProduct)
    {
        $superAttributes = [];
        $parentProductAttributes = $parentProduct->getTypeInstance()->getConfigurableAttributes($parentProduct);
        foreach ($parentProductAttributes->getItems() as $attribute) {
            $attributeData = $attribute->getData();
            $superAttributes[] = ['id' => $attributeData['attribute_id'], 'name' => strtolower($attributeData['label'])];
        }

        $index = 0;
        foreach ($superAttributes as $attribute) {
            $superAttributes[intval($attribute['id'])] = intval($childrenProduct->getData($attribute['name']));
            unset($superAttributes[$index]);
            $index++;
        }

        return $superAttributes;
    }
}
