<?php

namespace IdeaInYou\CheckoutGraphQl\Model\Resolver;

use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Exception\GraphQlInputException;
use Magento\Framework\GraphQl\Exception\GraphQlNoSuchEntityException;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Quote\Api\PaymentMethodManagementInterface;
use Magento\QuoteGraphQl\Model\Cart\CheckCartCheckoutAllowance;
use Magento\QuoteGraphQl\Model\Cart\GetCartForUser;
use Magento\Sales\Api\OrderRepositoryInterface;
use Magento\QuoteGraphQl\Model\Cart\SetShippingAddressesOnCartInterface;
use Magento\QuoteGraphQl\Model\Cart\SetShippingMethodsOnCartInterface;
use Magento\QuoteGraphQl\Model\Cart\SetPaymentMethodOnCart as SetPaymentMethodOnCartModel;
use Magento\QuoteGraphQl\Model\Cart\SetBillingAddressOnCart as SetBillingAddressOnCartModel;
use Fondy\Fondy\Block\Widget\Redirect as Fondy;
use Magento\Checkout\Model\Session;
use Magento\Quote\Model\Quote;

/**
 * Class CheckoutPayment
 * @package IdeaInYou\CheckoutGraphQl\Model\Resolver
 */
class CheckoutPayment implements ResolverInterface
{
    /**
     * @var Quote
     */
    private $quote;
    /**
     * @var Fondy
     */
    private $fondy;
    /**
     * @var SetBillingAddressOnCartModel
     */
    private $setBillingAddressOnCart;

    /**
     * @var SetPaymentMethodOnCartModel
     */
    private $setPaymentMethodOnCart;
    /**
     * @var SetShippingMethodsOnCartInterface
     */
    private $setShippingMethodsOnCart;
    /**
     * @var SetShippingAddressesOnCartInterface
     */
    private $setShippingAddressesOnCart;
    /**
     * @var CartManagementInterface
     */
    private $cartManagement;

    /**
     * @var GetCartForUser
     */
    private $getCartForUser;

    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;

    /**
     * @var CheckCartCheckoutAllowance
     */
    private $checkCartCheckoutAllowance;

    /**
     * @var PaymentMethodManagementInterface
     */
    private $paymentMethodManagement;
    /**
     * @var Session
     */
    protected $_checkoutSession;

    /**
     * @param GetCartForUser $getCartForUser
     * @param CartManagementInterface $cartManagement
     * @param OrderRepositoryInterface $orderRepository
     * @param CheckCartCheckoutAllowance $checkCartCheckoutAllowance
     * @param PaymentMethodManagementInterface $paymentMethodManagement
     * @param SetShippingAddressesOnCartInterface $setShippingAddressesOnCart
     * @param SetShippingMethodsOnCartInterface $setShippingMethodsOnCart
     * @param SetPaymentMethodOnCartModel $setPaymentMethodOnCart
     * @param SetBillingAddressOnCartModel $setBillingAddressOnCart
     * @param Fondy $fondy
     * @param Session $checkoutSession
     * @param Quote $quote
     */
    public function __construct(
        GetCartForUser $getCartForUser,
        CartManagementInterface $cartManagement,
        OrderRepositoryInterface $orderRepository,
        CheckCartCheckoutAllowance $checkCartCheckoutAllowance,
        PaymentMethodManagementInterface $paymentMethodManagement,
        SetShippingAddressesOnCartInterface $setShippingAddressesOnCart,
        SetShippingMethodsOnCartInterface $setShippingMethodsOnCart,
        SetPaymentMethodOnCartModel $setPaymentMethodOnCart,
        SetBillingAddressOnCartModel $setBillingAddressOnCart,
        Fondy $fondy,
        Session $checkoutSession,
        Quote $quote
    )
    {
        $this->getCartForUser = $getCartForUser;
        $this->cartManagement = $cartManagement;
        $this->orderRepository = $orderRepository;
        $this->checkCartCheckoutAllowance = $checkCartCheckoutAllowance;
        $this->paymentMethodManagement = $paymentMethodManagement;
        $this->setShippingAddressesOnCart = $setShippingAddressesOnCart;
        $this->setShippingMethodsOnCart = $setShippingMethodsOnCart;
        $this->setPaymentMethodOnCart = $setPaymentMethodOnCart;
        $this->setBillingAddressOnCart = $setBillingAddressOnCart;
        $this->fondy = $fondy;
        $this->_checkoutSession = $checkoutSession;
        $this->quote = $quote;
    }

    /**
     * @inheritdoc
     */
    public function resolve(
        Field $field,
        $context,
        ResolveInfo $info,
        array $value = null,
        array $args = null
    )
    {
        $orderData = $this->createCartOrder($args['input'], $context);
        return $this->placeOrder($orderData, $args['input']);
    }


    /**
     * @throws \Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException
     * @throws NoSuchEntityException
     * @throws GraphQlNoSuchEntityException
     * @throws GraphQlInputException
     */
    public function createCartOrder($input, $context)
    {
        if (empty($input['cart_id'])) {
            throw new GraphQlInputException(__('Required parameter "cart_id" is missing'));
        }
        $maskedCartId = $input['cart_id'];

        $storeId = (int)$context->getExtensionAttributes()->getStore()->getId();

        $cart = $this->getCartForUser->execute($maskedCartId, $context->getUserId(), $storeId);

        $this->checkCartCheckoutAllowance->execute($cart);

        $cart = $this->setDataToCart($input, $cart, $context);


        if ((int)$context->getUserId() === 0) {
            if (!$cart->getCustomerEmail()) {
                throw new GraphQlInputException(__("Guest email for cart is missing."));
            }
            $cart->setCheckoutMethod(CartManagementInterface::METHOD_GUEST);
        }

        try {
            $cartId = $cart->getId();
            $orderId = $this->cartManagement->placeOrder($cartId, $this->paymentMethodManagement->get($cartId));
            $order = $this->orderRepository->get($orderId);

            return [
                'order' => [
                    'order_number' => $order->getIncrementId(),
                    // @deprecated The order_id field is deprecated, use order_number instead
                    'order_id' => $order->getIncrementId(),
                ],
            ];
        } catch (NoSuchEntityException $e) {
            throw new GraphQlNoSuchEntityException(__($e->getMessage()), $e);
        } catch (LocalizedException $e) {
            throw new GraphQlInputException(__('Unable to place order: %message', ['message' => $e->getMessage()]), $e);
        }
    }

    /**
     * @param $input
     * @param $cart
     * @param $context
     * @return mixed
     * @throws GraphQlInputException
     * @throws GraphQlNoSuchEntityException
     * @throws \Magento\Framework\GraphQl\Exception\GraphQlAuthenticationException
     * @throws \Magento\Framework\GraphQl\Exception\GraphQlAuthorizationException
     */
    public function setDataToCart($input, $cart, $context)
    {
        $cart->setCustomerEmail($input['email']);

        $cart->setPhoneConfirmation($input['phoneConfirmation']);

        $postcode = 'not_exist';


        if($input['deliveryData']['deliveryType'] === 'courier') {
            $regionId = $input['deliveryData']['street'];
            $region = $input['deliveryData']['region'];
        }else {
            $region = $input['deliveryData']['region'];
            $regionId = $input['deliveryData']['street'];
        }

        if($input['deliveryData']['warehouseNumber']) {
            $street = $input['deliveryData']['warehouseNumber'];
        }else {
            $street = $input['deliveryData']['apartmentNumber'] . ' - ' . $input['deliveryData']['floorNumber'] . ' - ' . $input['deliveryData']['residenceNumber']. ' - ' . $input['deliveryData']['entranceNumber'];
        }


        if($input['deliveryData']['postalCode']) {
            $postcode = $input['deliveryData']['postalCode'];
        }


        $shippingAddresses[0]['address'] = [
            'firstname' => $input['firstName'],
            'lastname' => $input['lastName'],
            'street' => $street,
            'city' => $input['deliveryData']['city'],
            'country_code' => $input['deliveryData']['country_code'],
            'regionId' => $regionId,
            'region' => $region,
            'postcode' => $postcode,
            'telephone' => $input['phone'],
        ];

        $shippingMethods[0] = [
            'carrier_code' => 'freeshipping',
            'method_code' => 'freeshipping'
        ];

        $paymentData['code'] = strtolower($input['paymentMethod']);

        $billingAddress = $shippingAddresses[0];

        $this->setShippingAddressesOnCart->execute($context, $cart, $shippingAddresses);
        $this->setBillingAddressOnCart->execute($context, $cart, $billingAddress);

        $this->setShippingMethodsOnCart->execute($context, $cart, $shippingMethods);
        $this->setPaymentMethodOnCart->execute($cart, $paymentData);

        return $cart;
    }

    /**
     * @param $orderData
     * @param $input
     * @return array|bool[]
     * @throws GraphQlInputException
     * @throws LocalizedException
     */
    public function placeOrder($orderData, $input)
    {
        switch (strtolower($input['paymentMethod'])) {
            case "fondy": {
                return $this->placeOrderFondy($orderData);
            }
            default: {
                return $this->placeOrderCheckMo();
            }
        }
    }

    /**
     * @return bool[]
     */
    public function placeOrderCheckMo() {
        return [
            'status' => true
        ];
    }

    /**
     * @param $orderData
     * @return array
     * @throws GraphQlInputException|LocalizedException
     * @throws \Exception
     */
    public function placeOrderFondy($orderData)
    {
        $data['order'] = $orderData['order']['order_number'];

        $post_data = $this->fondy->getPostData($data);

        if (isset($post_data['error'])) {
            throw new GraphQlInputException(__('Payment failed: %message', ['message' => __('No items in cart. Please try again.')]));
        }

        $request = $this->doRequest($post_data);

        $url = json_decode($request, true);

        if (isset($url['response']['checkout_url'])) {
            return [
                'status' => true,
                'redirectUrl' => $url['response']['checkout_url']
            ];
        } else {
            $this->restoreCart();
            throw new GraphQlInputException(__('Payment failed: %message', ['message' => __('Please try again.')]));
        }
    }

    /**
     * @param $data
     * @return mixed
     * @throws GraphQlInputException
     */
    private function doRequest($data)
    {
        try {
            $httpHeaders = new \Zend\Http\Headers();
            $httpHeaders->addHeaders([
                'User-Agent' => 'Magento 2 CMS',
                'Accept' => 'application/json',
                'Content-Type' => 'application/json'
            ]);
            $request = new \Zend\Http\Request();
            $request->setHeaders($httpHeaders);
            $request->setUri('https://api.fondy.eu/api/checkout/url/');
            $request->setMethod(\Zend\Http\Request::METHOD_POST);

            $params = json_encode(['request' => $data]);
            $request->setContent($params);

            $client = new \Zend\Http\Client();
            $options = [
                'adapter' => 'Zend\Http\Client\Adapter\Curl',
                'curloptions' => [CURLOPT_FOLLOWLOCATION => true],
                'maxredirects' => 1,
                'timeout' => 30
            ];
            $client->setOptions($options);

            $response = $client->send($request);

            return $response->getBody();

        } catch (LocalizedException $e) {
            throw new GraphQlInputException(__('Payment capturing error: %message', ['message' => $e->getMessage()]), $e);
        }
    }

    /**
     * @throws \Exception
     */
    public function restoreCart()
    {
        $lastQuoteId = $this->_checkoutSession->getLastQuoteId();
        if ($quote = $this->quote->loadByIdWithoutStore($lastQuoteId)) {
            $quote->setIsActive(true)
                ->setReservedOrderId(null)
                ->save();
            $this->_checkoutSession->setQuoteId($lastQuoteId);
        }
    }
}
