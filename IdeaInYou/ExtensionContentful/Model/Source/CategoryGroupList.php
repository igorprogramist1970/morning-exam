<?php

namespace IdeaInYou\ExtensionContentful\Model\Source;

use IdeaInYou\ExtensionContentful\Model\ResourceModel\CategoryGroup\Grid\CollectionFactory;
use Magento\Catalog\Model\ResourceModel\Category\CollectionFactory as CollectionFactoryCategory;
use Magento\Catalog\Model\ResourceModel\Product\CollectionFactory as CollectionFactoryProduct;
use Magento\Framework\Data\OptionSourceInterface;

class CategoryGroupList implements OptionSourceInterface
{
    /**
     * @var CollectionFactoryProduct
     */
    protected CollectionFactoryProduct $_productCollectionFactory;
    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var CollectionFactoryCategory
     */
    private CollectionFactoryCategory $categoryCollectionFactory;

    /**
     * @param CollectionFactoryCategory $categoryCollectionFactory
     * @param CollectionFactoryProduct $productCollectionFactory
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        CollectionFactoryCategory $categoryCollectionFactory,
        CollectionFactoryProduct  $productCollectionFactory,
        CollectionFactory         $collectionFactory
    ) {
        $this->_productCollectionFactory = $productCollectionFactory;
        $this->collectionFactory = $collectionFactory;
        $this->categoryCollectionFactory = $categoryCollectionFactory;
    }

    /**
     * @return array
     */
    public function toOptionArray()
    {
        $categories = $this->categoryCollectionFactory->create()
            ->addAttributeToSelect('*');
        $options2 = [];

        foreach ($categories as $category) {
            $options2[] = ['label' => $category->getName(), 'value' => $category->getId()];
        }
        return $options2;
    }

}
