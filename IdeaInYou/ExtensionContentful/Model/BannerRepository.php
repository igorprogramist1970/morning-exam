<?php

namespace IdeaInYou\ExtensionContentful\Model;

use IdeaInYou\ExtensionContentful\Model\BannerFactory;
use IdeaInYou\ExtensionContentful\Model\ResourceModel\Banner as ResourceBanner;
use IdeaInYou\ExtensionContentful\Model\ResourceModel\Banners\CollectionFactory;
use IdeaInYou\ExtensionContentful\Api\BannerRepositoryInterface;
use Magento\Cms\Api\Data\PageSearchResultsInterface;
use Magento\Cms\Api\Data\PageSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;

class BannerRepository implements BannerRepositoryInterface
{
    /**
     * @var BannerFactory
     */
    private BannerFactory $bannerFactory;

    /**
     * @var ResourceBanner
     */
    private ResourceBanner $resource;

    /**
     * @var PageSearchResultsInterfaceFactory
     */
    private PageSearchResultsInterfaceFactory $searchCriteria;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private CollectionProcessorInterface $collectionProcessorInterfaceFactory;

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var ManagerInterface
     */
    private ManagerInterface $messageManager;

    /**
     * @param BannerFactory $bannerFactory
     * @param ResourceBanner $resource
     * @param PageSearchResultsInterfaceFactory $searchCriteria
     * @param CollectionFactory $collectionFactory
     * @param CollectionProcessorInterface $collectionProcessorInterfaceFactory
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        BannerFactory                     $bannerFactory,
        ResourceBanner                    $resource,
        PageSearchResultsInterfaceFactory $searchCriteria,
        CollectionFactory                 $collectionFactory,
        CollectionProcessorInterface      $collectionProcessorInterfaceFactory,
        ManagerInterface                  $messageManager
    ) {
        $this->resource = $resource;
        $this->bannerFactory = $bannerFactory;
        $this->searchCriteria = $searchCriteria;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessorInterfaceFactory = $collectionProcessorInterfaceFactory;
        $this->messageManager = $messageManager;
    }

    public function getById($id)
    {
        $banner = $this->bannerFactory->create();
        $this->resource->load($banner, $id);
        if (!$banner->getId()) {
            throw new NoSuchEntityException(__('Banner with id "%1" does not exist.', $id));
        }
        return $banner;
    }

    /**
     * @param Banner $banner
     * @return Banner
     * @throws CouldNotSaveException
     */
    public function save(Banner $banner)
    {
        try {
            $this->resource->save($banner);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the banner: %1',
                $exception->getMessage()
            ));
        }
        return $banner;
    }

    /**
     * @param Banner $banner
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Banner $banner)
    {
        try {
            $this->resource->delete($banner);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Banner: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @param $id
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        $this->delete($this->getById($id));
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return PageSearchResultsInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchCriteria->create();
        $collection = $this->collectionFactory->create();
        $this->collectionProcessorInterfaceFactory->process($searchCriteria, $collection);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }
}
