<?php

namespace IdeaInYou\ExtensionContentful\Model;

use Magento\Framework\App\Helper\AbstractHelper;

class DataSlider extends AbstractHelper
{
    const ENABLE_CONFIG_PATH = 'slider/general/enable';
    const TITLE_CONFIG_PATH = 'slider/general/title';

    /**
     * @return bool
     */
    public function getEnabled(): bool
    {
        return (bool) $this->scopeConfig->getValue(self::ENABLE_CONFIG_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->scopeConfig->getValue(self::TITLE_CONFIG_PATH,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }
}
