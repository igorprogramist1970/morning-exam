<?php

namespace IdeaInYou\ExtensionContentful\Model\CategoryGroup;

class CategoryGroupSave
{
    /**
     * @var \IdeaInYou\ExtensionContentful\Model\CategoryGroup\CategoryGroupCollection
     */
    private \IdeaInYou\ExtensionContentful\Model\CategoryGroup\CategoryGroupCollection $categoryGroupCollection;

    /**
     * @param CategoryGroupCollection $categoryGroupCollection
     */
    public function __construct(
        CategoryGroupCollection $categoryGroupCollection
    ) {
        $this->categoryGroupCollection = $categoryGroupCollection;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     */
    public function saveAllEntity($type, $store){
        $this->categoryGroupCollection->saveAllCatGroupToContentful($type, $store);
    }

}
