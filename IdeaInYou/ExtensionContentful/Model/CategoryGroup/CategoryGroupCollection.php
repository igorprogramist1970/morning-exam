<?php

namespace IdeaInYou\ExtensionContentful\Model\CategoryGroup;

use IdeaInYou\ExtensionContentful\Model\CategoryGroupRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use IdeaInYou\SyncToContentful\Api\SyncCustomAdditionToContentfulInterface;

class CategoryGroupCollection
{
    /**
     * @var CategoryGroupRepository
     */
    private CategoryGroupRepository $categoryGroupRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var SyncCustomAdditionToContentfulInterface
     */
    private SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful;

    /**
     * @param CategoryGroupRepository $categoryGroupRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
     */
    public function __construct(
        CategoryGroupRepository                 $categoryGroupRepository,
        SearchCriteriaBuilder                   $searchCriteriaBuilder,
        SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
    ) {
        $this->categoryGroupRepository = $categoryGroupRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->syncCustomAdditionToContentful = $syncCustomAdditionToContentful;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     */
    public function saveAllCatGroupToContentful($type, $store)
    {
        $categoryGroups = $this->getCategoryGroup($store);
        /** @var $categoryGroup \IdeaInYou\ExtensionContentful\Model\CategoryGroup */
        foreach ($categoryGroups as $categoryGroup) {
            $categoryGroup->setStoreId($store->getId());
            $this->syncCustomAdditionToContentful->prepareSyncContentfulData($type, $categoryGroup);
        }
    }

    /**
     * @param $store
     * @return \Magento\Cms\Api\Data\PageInterface[]
     */
    public function getCategoryGroup($store)
    {
        return $this->categoryGroupRepository->getList($this->getSearchCriteria())->getItems();
    }

    /**
     * @return \Magento\Framework\Api\SearchCriteria
     */
    protected function getSearchCriteria()
    {
        return $this->searchCriteriaBuilder
            ->addFilter('is_enable', '1')
            ->create();
    }
}
