<?php

namespace IdeaInYou\ExtensionContentful\Model\CategoryGroup;

use IdeaInYou\ExtensionContentful\Model\ResourceModel\CategoryGroup\CollectionFactory;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Ui\DataProvider\Modifier\PoolInterface;

/**
 * Class DataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\ModifierPoolDataProvider
{
    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * Constructor
     *
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $blockCollectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     * @param PoolInterface|null $pool
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $categoryGroupCollectionFactory,
        DataPersistorInterface $dataPersistor,
        array $meta = [],
        array $data = [],
        PoolInterface $pool = null
    ) {
        $this->collection = $categoryGroupCollectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data, $pool);
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();
        foreach ($items as $categoryGroup) {
            $this->loadedData[$categoryGroup->getId()] = $categoryGroup->getData();
        }

        $data = $this->dataPersistor->get('ideainyou_categorygroup');
        if (!empty($data)) {
            $categoryGroup = $this->collection->getNewEmptyItem();
            $categoryGroup->setData($data);
            $this->loadedData[$categoryGroup->getId()] = $categoryGroup->getData();
            $this->dataPersistor->clear('ideainyou_categorygroup');
        }

        return $this->loadedData;
    }
}
