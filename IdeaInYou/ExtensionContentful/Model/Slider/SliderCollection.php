<?php

namespace IdeaInYou\ExtensionContentful\Model\Slider;

use IdeaInYou\SyncToContentful\Api\SyncCustomAdditionToContentfulInterface;
use IdeaInYou\ExtensionContentful\Model\SliderRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;


class SliderCollection
{
    /**
     * @var SliderRepository
     */
    private SliderRepository $sliderRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var SyncCustomAdditionToContentfulInterface
     */
    private SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful;

    /**
     * @param SliderRepository $sliderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
     */
    public function __construct(
        SliderRepository                        $sliderRepository,
        SearchCriteriaBuilder                   $searchCriteriaBuilder,
        SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
    ) {
        $this->sliderRepository = $sliderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->syncCustomAdditionToContentful = $syncCustomAdditionToContentful;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     */
    public function saveAllSlidersToContentful($type, $store)
    {
        $sliders = $this->getSliders($store);
        /** @var $page \IdeaInYou\ExtensionContentful\Model\Slider */
        foreach ($sliders as $slider) {
            $slider->setStoreId($store->getId());
            $this->syncCustomAdditionToContentful->prepareSyncContentfulData($type, $slider);
        }
    }

    /**
     * @param $store
     * @return \Magento\Cms\Api\Data\PageInterface[]
     */
    public function getSliders($store)
    {
        return $this->sliderRepository->getList($this->getSearchCriteria())->getItems();
    }

    /**
     * @return \Magento\Framework\Api\SearchCriteria
     */
    protected function getSearchCriteria()
    {
        return $this->searchCriteriaBuilder
            ->addFilter('is_enable', '1')
            ->create();
    }
}
