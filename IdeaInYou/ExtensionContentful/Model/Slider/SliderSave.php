<?php

namespace IdeaInYou\ExtensionContentful\Model\Slider;

class SliderSave
{
    /**
     * @var \IdeaInYou\ExtensionContentful\Model\Slider\SliderCollection
     */
    private \IdeaInYou\ExtensionContentful\Model\Slider\SliderCollection $sliderCollection;

    /**
     * @param SliderCollection $sliderCollection
     */
    public function __construct(
        SliderCollection $sliderCollection
    ) {
        $this->sliderCollection = $sliderCollection;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     */
    public function saveAllEntity($type, $store){
        $this->sliderCollection->saveAllSlidersToContentful($type, $store);
    }

}
