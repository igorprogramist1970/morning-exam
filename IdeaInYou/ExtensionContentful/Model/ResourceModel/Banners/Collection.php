<?php

namespace IdeaInYou\ExtensionContentful\Model\ResourceModel\Banners;

use IdeaInYou\ExtensionContentful\Model\Banner;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Banner::class,
            \IdeaInYou\ExtensionContentful\Model\ResourceModel\Banner::class
        );
    }
}
