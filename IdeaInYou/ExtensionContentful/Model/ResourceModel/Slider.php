<?php

namespace IdeaInYou\ExtensionContentful\Model\ResourceModel;

use IdeaInYou\ExtensionContentful\Api\SliderInterface;

class Slider extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init(SliderInterface::TABLE_NAME, SliderInterface::ENTITY_ID);
    }
}
