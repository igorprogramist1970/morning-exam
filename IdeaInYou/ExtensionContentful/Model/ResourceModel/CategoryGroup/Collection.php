<?php

namespace IdeaInYou\ExtensionContentful\Model\ResourceModel\CategoryGroup;

use IdeaInYou\ExtensionContentful\Model\CategoryGroup;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            CategoryGroup::class,
            \IdeaInYou\ExtensionContentful\Model\ResourceModel\CategoryGroup::class
        );
    }
}
