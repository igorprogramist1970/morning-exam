<?php

namespace IdeaInYou\ExtensionContentful\Model\ResourceModel;

use IdeaInYou\ExtensionContentful\Api\BannerInterface;

class Banner extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{

    protected function _construct()
    {
        $this->_init(BannerInterface::TABLE_NAME, BannerInterface::ENTITY_ID);
    }
}
