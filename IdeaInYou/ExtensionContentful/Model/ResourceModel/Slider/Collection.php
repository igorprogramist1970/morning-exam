<?php

namespace IdeaInYou\ExtensionContentful\Model\ResourceModel\Slider;

use IdeaInYou\ExtensionContentful\Model\Slider;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected $_idFieldName = 'entity_id';

    /**
     * Define resource model
     *
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            Slider::class,
            \IdeaInYou\ExtensionContentful\Model\ResourceModel\Slider::class
        );
    }

    /**
     * @return \Magento\Framework\DB\Select
     */
    public function joinBannerData()
    {
        $select = $this->getConnection()->select()
            ->joinLeft(
                ['ib' => $this->getTable('ideainyou_banners')],
                'main_table.banner_id_1 = ib.entity_id',
                ['title_banner', 'url']
            );
        $result = $this->getConnection()->fetchAll($select);

        return $result;

    }
}
