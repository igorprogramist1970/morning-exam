<?php

namespace IdeaInYou\ExtensionContentful\Model;

use IdeaInYou\ExtensionContentful\Api\CategoryGroupInterface;
use Magento\Framework\DataObject\IdentityInterface;
use Magento\Framework\Model\AbstractModel;

class CategoryGroup extends AbstractModel implements CategoryGroupInterface, IdentityInterface
{
    const  CACHE_TAG = 'ideainyou_categorygroup';

    /**
     * @var string
     */
    protected $_eventPrefix = 'ideainyou_categorygroup';

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(ResourceModel\CategoryGroup::class);
    }

    /**
     * Get identities
     *
     * @return array
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * @param $id
     * @return CategoryGroup
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }


}
