<?php

namespace IdeaInYou\ExtensionContentful\Model\Banners;

use IdeaInYou\SyncToContentful\Api\SyncCustomAdditionToContentfulInterface;
use IdeaInYou\ExtensionContentful\Model\BannerRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Store\Model\StoreManagerInterface;

class BannerCollection
{
    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;
    /**
     * @var BannerRepository
     */
    private BannerRepository $bannerRepository;
    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchCriteriaBuilder;

    /**
     * @var SyncCustomAdditionToContentfulInterface
     */
    private SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful;

    /**
     * @param StoreManagerInterface $storeManager
     * @param BannerRepository $bannerRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        BannerRepository      $bannerRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
    ) {
        $this->storeManager = $storeManager;
        $this->bannerRepository = $bannerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->syncCustomAdditionToContentful = $syncCustomAdditionToContentful;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     */
    public function saveAllBannersToContentful($type, $store)
    {
        $banners = $this->getBaners($store);
        /** @var $banner \IdeaInYou\ExtensionContentful\Model\Banner */
        foreach ($banners as $banner) {
            $banner->setStoreId($store->getId());
            $this->syncCustomAdditionToContentful->prepareSyncContentfulData($this, $banner);
        }
    }

    /**
     * @param $store
     * @return \Magento\Cms\Api\Data\PageInterface[]
     */
    public function getBaners($store)
    {
        return $this->bannerRepository->getList($this->getSearchCriteria())->getItems();
    }

    /**
     * @return \Magento\Framework\Api\SearchCriteria
     */
    protected function getSearchCriteria()
    {
        return $this->searchCriteriaBuilder
            ->addFilter('is_enable', '1')
            ->create();
    }
}
