<?php

namespace IdeaInYou\ExtensionContentful\Model\Banners;

use IdeaInYou\ExtensionContentful\Model\Banners\BannerCollection;


class BannerSave
{
    /**
     * @var \IdeaInYou\ExtensionContentful\Model\Banners\BannerCollection
     */
    private \IdeaInYou\ExtensionContentful\Model\Banners\BannerCollection $bannerCollection;

    /**
     * @param BannerCollection $bannerCollection
     */
    public function __construct(
        BannerCollection $bannerCollection
    ) {

        $this->bannerCollection = $bannerCollection;
    }

    /**
     * @param $type
     * @param $store
     * @return void
     */
    public function saveAllEntity($type, $store){
        $this->bannerCollection->saveAllBannersToContentful($type, $store);
    }

}
