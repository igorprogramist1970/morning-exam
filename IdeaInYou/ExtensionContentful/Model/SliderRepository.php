<?php

namespace IdeaInYou\ExtensionContentful\Model;

use IdeaInYou\ExtensionContentful\Model\ResourceModel\Slider as ResourceSlider;
use IdeaInYou\ExtensionContentful\Api\SliderRepositoryInterface;
use IdeaInYou\ExtensionContentful\Model\ResourceModel\Slider\CollectionFactory;
use IdeaInYou\ExtensionContentful\Model\SliderFactory;
use Magento\Cms\Api\Data\PageSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;

class SliderRepository implements SliderRepositoryInterface
{
    /**
     * @var \IdeaInYou\ExtensionContentful\Model\SliderFactory
     */
    private SliderFactory $sliderFactory;

    /**
     * @var ResourceSlider
     */
    private ResourceSlider $resource;

    /**
     * @var PageSearchResultsInterfaceFactory
     */
    private PageSearchResultsInterfaceFactory $searchCriteria;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;
    /**
     * @var CollectionProcessorInterface
     */
    private CollectionProcessorInterface $collectionProcessorInterfaceFactory;

    /**
     * @var ManagerInterface
     */
    private ManagerInterface $messageManager;

    /**
     * @param SliderFactory $sliderFactory
     * @param ResourceSlider $resource
     * @param PageSearchResultsInterfaceFactory $searchCriteria
     * @param CollectionFactory $collectionFactory
     * @param CollectionProcessorInterface $collectionProcessorInterfaceFactory
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        SliderFactory                     $sliderFactory,
        ResourceSlider                    $resource,
        PageSearchResultsInterfaceFactory $searchCriteria,
        CollectionFactory                 $collectionFactory,
        CollectionProcessorInterface      $collectionProcessorInterfaceFactory,
        ManagerInterface                  $messageManager
    ) {
        $this->resource = $resource;
        $this->sliderFactory = $sliderFactory;
        $this->searchCriteria = $searchCriteria;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessorInterfaceFactory = $collectionProcessorInterfaceFactory;
        $this->messageManager = $messageManager;
    }

    /**
     * @param $id
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $slider = $this->sliderFactory->create();
        $this->resource->load($slider, $id);
        if (!$slider->getId()) {
            throw new NoSuchEntityException(__('Slider with id "%1" does not exist.', $id));
        }
        return $slider;
    }

    /**
     * @param Slider $slider
     * @return Slider
     * @throws CouldNotSaveException
     */
    public function save(Slider $slider)
    {
        try {
            $this->resource->save($slider);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the slider: %1',
                $exception->getMessage()
            ));
        }
        return $slider;
    }

    /**
     * @param Slider $slider
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(Slider $slider)
    {
        try {
            $this->resource->delete($slider);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Slider: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @param $id
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        $this->delete($this->getById($id));
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Cms\Api\Data\PageSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchCriteria->create();
        $collection = $this->collectionFactory->create();
        $this->collectionProcessorInterfaceFactory->process($searchCriteria, $collection);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }
}
