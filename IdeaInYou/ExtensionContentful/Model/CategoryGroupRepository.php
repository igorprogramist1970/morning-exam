<?php

namespace IdeaInYou\ExtensionContentful\Model;

use IdeaInYou\ExtensionContentful\Model\CategoryGroupFactory;
use IdeaInYou\ExtensionContentful\Model\ResourceModel\CategoryGroup as ResourceCategoryGroup;
use IdeaInYou\ExtensionContentful\Model\ResourceModel\CategoryGroup\CollectionFactory;
use IdeaInYou\ExtensionContentful\Api\CategoryGroupRepositoryInterface;
use Magento\Cms\Api\Data\PageSearchResultsInterfaceFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Message\ManagerInterface;

class CategoryGroupRepository implements CategoryGroupRepositoryInterface
{
    /**
     * @var \IdeaInYou\ExtensionContentful\Model\CategoryGroupFactory
     */
    private CategoryGroupFactory $categoryGroupFactory;

    /**
     * @var ResourceCategoryGroup
     */
    private ResourceCategoryGroup $resource;

    /**
     * @var PageSearchResultsInterfaceFactory
     */
    private PageSearchResultsInterfaceFactory $searchCriteria;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private CollectionProcessorInterface $collectionProcessorInterfaceFactory;

    /**
     * @var ManagerInterface
     */
    private ManagerInterface $messageManager;

    /**
     * @param CategoryGroupFactory $categoryGroupFactory
     * @param ResourceCategoryGroup $resource
     * @param PageSearchResultsInterfaceFactory $searchCriteria
     * @param CollectionFactory $collectionFactory
     * @param CollectionProcessorInterface $collectionProcessorInterfaceFactory
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        CategoryGroupFactory              $categoryGroupFactory,
        ResourceCategoryGroup             $resource,
        PageSearchResultsInterfaceFactory $searchCriteria,
        CollectionFactory                 $collectionFactory,
        CollectionProcessorInterface      $collectionProcessorInterfaceFactory,
        ManagerInterface                  $messageManager
    ) {
        $this->resource = $resource;
        $this->categoryGroupFactory = $categoryGroupFactory;
        $this->searchCriteria = $searchCriteria;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessorInterfaceFactory = $collectionProcessorInterfaceFactory;
        $this->messageManager = $messageManager;
    }

    /**
     * @param $id
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getById($id)
    {
        $categoryGroup = $this->categoryGroupFactory->create();
        $this->resource->load($categoryGroup, $id);
        if (!$categoryGroup->getId()) {
            throw new NoSuchEntityException(__('CategoryGroup with id "%1" does not exist.', $id));
        }
        return $categoryGroup;
    }

    /**
     * @param \IdeaInYou\ExtensionContentful\Model\CategoryGroup $categoryGroup
     * @return \IdeaInYou\ExtensionContentful\Model\CategoryGroup
     * @throws CouldNotSaveException
     */
    public function save(CategoryGroup $categoryGroup)
    {
        try {
            $this->resource->save($categoryGroup);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the categoryGroup: %1',
                $exception->getMessage()
            ));
        }
        return $categoryGroup;
    }

    /**
     * @param \IdeaInYou\ExtensionContentful\Model\CategoryGroup $categoryGroup
     * @return bool
     * @throws CouldNotDeleteException
     */
    public function delete(CategoryGroup $categoryGroup): bool
    {
        try {
            $this->resource->delete($categoryGroup);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the CategoryGroup: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * @param $id
     * @throws CouldNotDeleteException
     * @throws NoSuchEntityException
     */
    public function deleteById($id)
    {
        $this->delete($this->getById($id));
    }

    /**
     * @param \Magento\Framework\Api\SearchCriteriaInterface $searchCriteria
     * @return \Magento\Cms\Api\Data\PageSearchResultsInterface
     */
    public function getList(\Magento\Framework\Api\SearchCriteriaInterface $searchCriteria)
    {
        $searchResult = $this->searchCriteria->create();
        $collection = $this->collectionFactory->create();
        $this->collectionProcessorInterfaceFactory->process($searchCriteria, $collection);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }
}
