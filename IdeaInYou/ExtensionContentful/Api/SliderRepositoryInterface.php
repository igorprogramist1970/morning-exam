<?php

namespace IdeaInYou\ExtensionContentful\Api;

use IdeaInYou\ExtensionContentful\Model\Slider;
use Magento\Framework\Api\SearchCriteriaInterface;

interface SliderRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param Slider $slider
     * @return mixed
     */
    public function save(Slider $slider);

    /**
     * @param Slider $slider
     * @return mixed
     */
    public function delete(Slider $slider);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
