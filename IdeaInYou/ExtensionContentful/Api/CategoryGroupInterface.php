<?php

namespace IdeaInYou\ExtensionContentful\Api;

interface CategoryGroupInterface
{
    const TABLE_NAME = 'ideainyou_categorygroup';
    const ENTITY_ID = 'entity_id';

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param $id
     * @return mixed
     */
    public function setId($id);


}
