<?php

namespace IdeaInYou\ExtensionContentful\Api;

interface BannerInterface
{
    const TABLE_NAME = 'ideainyou_banners';
    const ENTITY_ID = 'entity_id';

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param $id
     * @return mixed
     */
    public function setId($id);


}
