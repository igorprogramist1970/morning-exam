<?php

namespace IdeaInYou\ExtensionContentful\Api;

interface SliderInterface
{
    const TABLE_NAME = 'ideainyou_slider';
    const ENTITY_ID = 'entity_id';

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param $id
     * @return mixed
     */
    public function setId($id);


}
