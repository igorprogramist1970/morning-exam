<?php

namespace IdeaInYou\ExtensionContentful\Api;

use IdeaInYou\ExtensionContentful\Model\CategoryGroup;
use Magento\Framework\Api\SearchCriteriaInterface;

interface CategoryGroupRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param CategoryGroup $categoryGroup
     * @return mixed
     */
    public function save(CategoryGroup $categoryGroup);

    /**
     * @param CategoryGroup $categoryGroup
     * @return mixed
     */
    public function delete(CategoryGroup $categoryGroup);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
