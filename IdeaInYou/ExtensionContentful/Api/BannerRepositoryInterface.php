<?php

namespace IdeaInYou\ExtensionContentful\Api;

use IdeaInYou\ExtensionContentful\Model\Banner;
use Magento\Framework\Api\SearchCriteriaInterface;

interface BannerRepositoryInterface
{
    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param Banner $banner
     * @return mixed
     */
    public function save(Banner $banner);

    /**
     * @param Banner $banner
     * @return mixed
     */
    public function delete(Banner $banner);

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
