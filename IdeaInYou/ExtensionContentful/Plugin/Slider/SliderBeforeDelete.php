<?php

namespace IdeaInYou\ExtensionContentful\Plugin\Slider;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\ExtensionContentful\Model\SliderRepository;

class SliderBeforeDelete
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param SliderRepository $subject
     * @return void
     */
    public function beforeDelete(SliderRepository $subject)
    {
        $this->validateContentful->validator(SliderPlugin::TYPE_SLIDER);
    }
}
