<?php

namespace IdeaInYou\ExtensionContentful\Plugin\Banner;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\ExtensionContentful\Model\BannerRepository;

class BannerBeforeDelete
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param BannerRepository $subject
     * @return void
     */
    public function beforeDelete(BannerRepository $subject)
    {
        $this->validateContentful->validator(BannerPlugin::TYPE_BANNER);
    }
}
