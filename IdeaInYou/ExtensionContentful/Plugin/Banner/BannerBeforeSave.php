<?php

namespace IdeaInYou\ExtensionContentful\Plugin\Banner;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\ExtensionContentful\Model\BannerRepository;

class BannerBeforeSave
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param BannerRepository $subject
     * @return void
     */
    public function beforeSave(BannerRepository $subject)
    {
        $this->validateContentful->validator(BannerPlugin::TYPE_BANNER);
    }
}
