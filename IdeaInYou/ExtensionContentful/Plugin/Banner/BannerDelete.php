<?php

namespace IdeaInYou\ExtensionContentful\Plugin\Banner;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\SyncToContentful\Api\SyncCustomAdditionToContentfulInterface;
use IdeaInYou\ExtensionContentful\Model\BannerRepository;
use Magento\Framework\Model\AbstractModel;

class BannerDelete
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var SyncCustomAdditionToContentfulInterface
     */
    private SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
     */
    public function __construct(
        ValidateContentfulInterface    $validateContentful,
        SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
    ) {
        $this->validateContentful = $validateContentful;
        $this->syncCustomAdditionToContentful = $syncCustomAdditionToContentful;
    }

    /**
     * @param BannerRepository $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterDelete(BannerRepository $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->syncCustomAdditionToContentful->prepareToDeleteContentfulData(BannerPlugin::TYPE_BANNER, $object);
        return $result;
    }

}
