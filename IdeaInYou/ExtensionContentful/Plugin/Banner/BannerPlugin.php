<?php

namespace IdeaInYou\ExtensionContentful\Plugin\Banner;

use IdeaInYou\SyncToContentful\Api\SyncCustomAdditionToContentfulInterface;
use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\ExtensionContentful\Model\BannerRepository as BannerRepository;
use Magento\Framework\Model\AbstractModel;

class BannerPlugin
{
    const TYPE_BANNER = 'banner';

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var SyncCustomAdditionToContentfulInterface
     */
    private SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
    ) {
        $this->validateContentful = $validateContentful;
        $this->syncCustomAdditionToContentful = $syncCustomAdditionToContentful;
    }

    /**
     * @param BannerRepository $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterSave(BannerRepository $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->syncCustomAdditionToContentful->prepareSyncContentfulData(self::TYPE_BANNER , $object);
        return $result;
    }

}

