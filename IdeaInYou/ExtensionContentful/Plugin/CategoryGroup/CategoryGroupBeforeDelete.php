<?php

namespace IdeaInYou\ExtensionContentful\Plugin\CategoryGroup;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\ExtensionContentful\Model\CategoryGroupRepository;

class CategoryGroupBeforeDelete
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful
    ) {
        $this->validateContentful = $validateContentful;
    }

    /**
     * @param CategoryGroupRepository $subject
     * @return void
     */
    public function beforeSave(CategoryGroupRepository $subject)
    {
        $this->validateContentful->validator(CategoryGroupPlugin::TYPE_CATEGORY_GROUP);
    }
}
