<?php

namespace IdeaInYou\ExtensionContentful\Plugin\CategoryGroup;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\SyncToContentful\Api\SyncCustomAdditionToContentfulInterface;
use IdeaInYou\ExtensionContentful\Model\CategoryGroupRepository;
use Magento\Framework\Model\AbstractModel;

class CategoryGroupDelete
{
    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var SyncCustomAdditionToContentfulInterface
     */
    private SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
     */
    public function __construct(
        ValidateContentfulInterface    $validateContentful,
        SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
    ) {
        $this->validateContentful = $validateContentful;
        $this->syncCustomAdditionToContentful = $syncCustomAdditionToContentful;
    }

    /**
     * @param CategoryGroupRepository $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterDelete(CategoryGroupRepository $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->syncCustomAdditionToContentful->prepareToDeleteContentfulData(
            CategoryGroupPlugin::TYPE_CATEGORY_GROUP, $object);
        return $result;
    }
}
