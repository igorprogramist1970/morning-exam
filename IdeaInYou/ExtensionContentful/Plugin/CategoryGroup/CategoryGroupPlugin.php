<?php

namespace IdeaInYou\ExtensionContentful\Plugin\CategoryGroup;

use IdeaInYou\SyncToContentful\Api\ValidateContentfulInterface;
use IdeaInYou\ExtensionContentful\Controller\Adminhtml\CategoryGroup\Save;
use IdeaInYou\SyncToContentful\Api\SyncCustomAdditionToContentfulInterface;
use IdeaInYou\ExtensionContentful\Model\CategoryGroupRepository;
use Magento\Framework\Model\AbstractModel;

class CategoryGroupPlugin
{
    /**
     *
     */
    const TYPE_CATEGORY_GROUP = 'categoryGroup';

    /**
     * @var ValidateContentfulInterface
     */
    private ValidateContentfulInterface $validateContentful;

    /**
     * @var SyncCustomAdditionToContentfulInterface
     */
    private SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful;

    /**
     * @param ValidateContentfulInterface $validateContentful
     * @param SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
     */
    public function __construct(
        ValidateContentfulInterface $validateContentful,
        SyncCustomAdditionToContentfulInterface $syncCustomAdditionToContentful
    ) {
        $this->validateContentful = $validateContentful;
        $this->syncCustomAdditionToContentful = $syncCustomAdditionToContentful;
    }

    /**
     * @param CategoryGroupRepository $subject
     * @param $result
     * @param AbstractModel $object
     * @return mixed
     */
    public function afterSave(CategoryGroupRepository $subject, $result, AbstractModel $object)
    {
        if (!$this->validateContentful->isEnable()) return $result;
        $this->syncCustomAdditionToContentful->prepareSyncContentfulData(self::TYPE_CATEGORY_GROUP , $object);
        return $result;
    }

}
