<?php

namespace IdeaInYou\ExtensionContentful\Block\Adminhtml\CategoryGroup\Edit;

use IdeaInYou\ExtensionContentful\Api\CategoryGroupRepositoryInterface;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;
    private CategoryGroupRepositoryInterface $categoryGroupRepository;

    public function __construct(
        Context $context,
        CategoryGroupRepositoryInterface $categoryGroupRepository
    ) {
        $this->context = $context;
        $this->categoryGroupRepository = $categoryGroupRepository;
    }

    /**
     * Return CMS block ID
     *
     * @return int|null
     */
    public function getCategoryGroupId()
    {
        try {
            return $this->categoryGroupRepository->getById(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
