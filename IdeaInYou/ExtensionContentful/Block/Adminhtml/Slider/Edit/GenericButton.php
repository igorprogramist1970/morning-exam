<?php

namespace IdeaInYou\ExtensionContentful\Block\Adminhtml\Slider\Edit;

use IdeaInYou\ExtensionContentful\Api\SliderRepositoryInterface;
use Magento\Backend\Block\Widget\Context;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class GenericButton
 */
class GenericButton
{
    /**
     * @var Context
     */
    protected $context;
    private SliderRepositoryInterface $sliderRepository;

    public function __construct(
        Context $context,
        SliderRepositoryInterface $sliderRepository
    ) {
        $this->context = $context;
        $this->sliderRepository = $sliderRepository;
    }

    /**
     * Return CMS block ID
     *
     * @return int|null
     */
    public function getSliderId()
    {
        try {
            return $this->sliderRepository->getById(
                $this->context->getRequest()->getParam('id')
            )->getId();
        } catch (NoSuchEntityException $e) {
        }
        return null;
    }

    /**
     * Generate url by route and parameters
     *
     * @param   string $route
     * @param   array $params
     * @return  string
     */
    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }
}
