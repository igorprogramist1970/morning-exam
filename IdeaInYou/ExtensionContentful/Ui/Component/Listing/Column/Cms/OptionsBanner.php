<?php

namespace IdeaInYou\ExtensionContentful\Ui\Component\Listing\Column\Cms;

use IdeaInYou\ExtensionContentful\Controller\Adminhtml\Banners\Edit;
use IdeaInYou\ExtensionContentful\Model\BannerRepository;
use Magento\Framework\Escaper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\System\Store as SystemStore;
use Magento\Store\Ui\Component\Listing\Column\Store\Options as StoreOptions;

class OptionsBanner extends StoreOptions
{
    /** @var string */
    const ALL_STORE_VIEWS = '0';

    /** @var Edit */
    private Edit $edit;

    /** @var BannerRepository */
    private BannerRepository $bannerRepository;

    /** @var StoreManagerInterface */
    private StoreManagerInterface $storeManager;

    /**
     * @param SystemStore $systemStore
     * @param Escaper $escaper
     * @param Edit $edit
     * @param BannerRepository $bannerRepository
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        SystemStore           $systemStore,
        Escaper               $escaper,
        Edit                  $edit,
        BannerRepository      $bannerRepository,
        StoreManagerInterface $storeManager
    ) {
        parent::__construct($systemStore, $escaper);
        $this->edit = $edit;
        $this->bannerRepository = $bannerRepository;
        $this->storeManager = $storeManager;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function toOptionArray()
    {
        $bannerId = $this->edit->getRequest()->getParam("id");
        if (isset($bannerId)) {
            $ids = explode(",", $this->bannerRepository->getById($bannerId)->getData('store_id'));
        } else {
            $ids = null;
        }
        if ($this->options !== null) {
            return $this->options;
        }
        if (is_array($ids)) {
            foreach ($ids as $id) {
                $store = $this->storeManager->getStore((int)$id);
                $this->currentOptions['All Store Views']['label'] = __('All Store Views');
                $this->currentOptions['All Store Views']['value'] = self::ALL_STORE_VIEWS;
                $this->currentOptions[$store->getName()]['value'] = $store->getStoreId();
            }
        } else {
            $this->currentOptions['All Store Views']['label'] = __('All Store Views');
            $this->currentOptions['All Store Views']['value'] = self::ALL_STORE_VIEWS;
        }
        $this->generateCurrentOptions();
        $this->options = array_values($this->currentOptions);

        return $this->options;
    }
}
