<?php

namespace IdeaInYou\ExtensionContentful\Ui\Component\Listing\Column\Cms;

use IdeaInYou\ExtensionContentful\Controller\Adminhtml\CategoryGroup\Edit;
use IdeaInYou\ExtensionContentful\Model\CategoryGroupRepository;
use Magento\Framework\Escaper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Store\Model\System\Store as SystemStore;
use Magento\Store\Ui\Component\Listing\Column\Store\Options as StoreOptions;

class OptionsCategoryGroup extends StoreOptions
{
    /**
     *
     */
    const ALL_STORE_VIEWS = '0';

    /**
     * @var Edit
     */
    private Edit $edit;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var CategoryGroupRepository
     */
    private CategoryGroupRepository $categoryGroupRepository;

    /**
     * @param SystemStore $systemStore
     * @param Escaper $escaper
     * @param Edit $edit
     * @param CategoryGroupRepository $categoryGroupRepository
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        SystemStore             $systemStore,
        Escaper                 $escaper,
        Edit                    $edit,
        CategoryGroupRepository $categoryGroupRepository,
        StoreManagerInterface   $storeManager
    ) {
        parent::__construct($systemStore, $escaper);
        $this->edit = $edit;
        $this->storeManager = $storeManager;
        $this->categoryGroupRepository = $categoryGroupRepository;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function toOptionArray()
    {
        $catGroupId = $this->edit->getRequest()->getParam("id");
        if (isset($catGroupId)) {
            $ids = explode(",", $this->categoryGroupRepository->getById($catGroupId)->getData('store_id'));
        } else {
            $ids = null;
        }

        if ($this->options !== null) {
            return $this->options;
        }
        if (is_array($ids)) {
            foreach ($ids as $id) {
                $store = $this->storeManager->getStore((int)$id);
                $this->currentOptions['All Store Views']['label'] = __('All Store Views');
                $this->currentOptions['All Store Views']['value'] = self::ALL_STORE_VIEWS;
                $this->currentOptions[$store->getName()]['value'] = $store->getStoreId();
            }
        } else {
            $this->currentOptions['All Store Views']['label'] = __('All Store Views');
            $this->currentOptions['All Store Views']['value'] = self::ALL_STORE_VIEWS;
        }
        $this->generateCurrentOptions();
        $this->options = array_values($this->currentOptions);

        return $this->options;
    }
}
