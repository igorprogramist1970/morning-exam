<?php

namespace IdeaInYou\ExtensionContentful\Controller\Adminhtml\Slider;

use IdeaInYou\ExtensionContentful\Api\SliderRepositoryInterface;
use IdeaInYou\ExtensionContentful\Model\ResourceModel\Slider\CollectionFactory as SliderCollectionFactory;
use IdeaInYou\ExtensionContentful\Model\SliderFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Registry;

/**
 * Save CMS block action.
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    private Registry $_coreRegistry;
    /**
     * @var SliderFactory|mixed
     */
    private $sliderFactory;
    /**
     * @var SliderRepositoryInterface|mixed
     */
    private $sliderRepository;
    private SliderCollectionFactory $sliderCollectionFactory;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     * @param SliderCollectionFactory $sliderCollectionFactory
     * @param SliderFactory|null $sliderFactory
     * @param SliderRepositoryInterface|null $sliderRepository
     */
    public function __construct(
        Context                   $context,
        Registry                  $coreRegistry,
        DataPersistorInterface    $dataPersistor,
        SliderCollectionFactory   $sliderCollectionFactory,
        SliderFactory             $sliderFactory = null,
        SliderRepositoryInterface $sliderRepository = null
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->sliderFactory = $sliderFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(SliderFactory::class);
        $this->sliderRepository = $sliderRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(SliderRepositoryInterface::class);
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->sliderCollectionFactory = $sliderCollectionFactory;
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $data['store_id'] = implode(',',$data['store_id']);
        if ($data) {
            if (empty($data['entity_id'])) {
                $data['entity_id'] = null;
            }

            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->sliderFactory->create();

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $model = $this->sliderRepository->getById($id);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__('This slider no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);

            try {
                $this->sliderRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the slider.'));
                $this->dataPersistor->clear('ideainyou_slider');
                return $this->processBlockReturn($model, $data, $resultRedirect);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }
            $this->sliderCollectionFactory->create()->joinBannerData();
            $this->dataPersistor->set('ideainyou_slider', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process and set the block return
     *
     * @param \Magento\Cms\Model\Block $model
     * @param array $data
     * @param \Magento\Framework\Controller\ResultInterface $resultRedirect
     * @return \Magento\Framework\Controller\ResultInterface
     */
    private function processBlockReturn($model, $data, $resultRedirect)
    {
        $redirect = $data['back'] ?? 'close';

        if ($redirect === 'continue') {
            $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
        } else if ($redirect === 'close') {
            $resultRedirect->setPath('*/*/');
        }
        return $resultRedirect;
    }
}
