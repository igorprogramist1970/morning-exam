<?php

namespace IdeaInYou\ExtensionContentful\Controller\Adminhtml\Slider;

use IdeaInYou\ExtensionContentful\Api\SliderRepositoryInterface;
use IdeaInYou\ExtensionContentful\Model\ResourceModel\Slider\CollectionFactory;
use IdeaInYou\ExtensionContentful\Model\Slider;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class MassDelete
 */
class MassApprove extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;
    private SliderRepositoryInterface $sliderRepository;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param SliderRepositoryInterface $sliderRepository
     */
    public function __construct(
        Context $context,
        Filter $filter,
        CollectionFactory $collectionFactory,
        SliderRepositoryInterface $sliderRepository
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
        $this->sliderRepository = $sliderRepository;
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();
        try {
            foreach ($collection as $slider) {
                /** @var Slider $slider */
                $slider->setIsApproved(true);
                $this->sliderRepository->save($slider);
            }

            $this->messageManager->addSuccessMessage(__('A total of %1 slider(s) have been approved.', $collectionSize));
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }


        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
