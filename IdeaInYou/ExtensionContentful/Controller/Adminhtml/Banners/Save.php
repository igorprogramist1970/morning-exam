<?php

namespace IdeaInYou\ExtensionContentful\Controller\Adminhtml\Banners;

use IdeaInYou\ExtensionContentful\Model\BannerFactory;
use IdeaInYou\ExtensionContentful\Api\BannerRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Catalog\Model\ImageUploader;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Registry;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Stdlib\DateTime\Filter\Date;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Save CMS block action.
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    const IMAGE_TMP_PATH = "bannersImage/tmp";

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var Registry
     */
    private Registry $_coreRegistry;

    /**
     * @var BannerFactory|mixed
     */
    private $bannerFactory;

    /**
     * @var BannerRepositoryInterface|mixed
     */
    private $bannerRepository;

    /**
     * @var ImageUploader
     */
    private ImageUploader $imageUploader;

    /**
     * @var Date
     */
    private Date $date;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var Json
     */
    private Json $json;


    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     * @param BannerFactory|null $bannerFactory
     * @param BannerRepositoryInterface|null $bannerRepository
     * @param ImageUploader $imageUploader
     * @param StoreManagerInterface $storeManager
     * @param Date $date
     * @param Json $json
     * @param string $baseTmpPath
     */
    public function __construct(
        Context                   $context,
        Registry                  $coreRegistry,
        DataPersistorInterface    $dataPersistor,
        BannerFactory             $bannerFactory = null,
        BannerRepositoryInterface $bannerRepository = null,
        ImageUploader             $imageUploader,
        StoreManagerInterface     $storeManager,
        Date                      $date,
        Json                      $json,
                                  $baseTmpPath = self::IMAGE_TMP_PATH
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->bannerFactory = $bannerFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(BannerFactory::class);
        $this->bannerRepository = $bannerRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(BannerRepositoryInterface::class);
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
        $this->imageUploader = $imageUploader;
        $this->date = $date;
        $this->storeManager = $storeManager;
        $this->json = $json;
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();

        $data['store_id'] = implode(',', $data['store_id']);
        $data = $this->prepareDataImage($data);

        if ($data) {
            if (empty($data['entity_id'])) {
                $data['entity_id'] = null;
            }

            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->bannerFactory->create();

            $id = $this->getRequest()->getParam('entity_id');
            if ($id) {
                try {
                    $model = $this->bannerRepository->getById($id);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__('This banner no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);

            try {
                $this->bannerRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the banner.'));
                $this->dataPersistor->clear('ideainyou_banners');
                return $this->processBlockReturn($model, $data, $resultRedirect);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }

            $this->dataPersistor->set('ideainyou_banners', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process and set the block return
     *
     * @param \Magento\Cms\Model\Block $model
     * @param array $data
     * @param \Magento\Framework\Controller\ResultInterface $resultRedirect
     * @return \Magento\Framework\Controller\ResultInterface
     */
    private function processBlockReturn($model, $data, $resultRedirect)
    {
        $redirect = $data['back'] ?? 'close';

        if ($redirect === 'continue') {
            $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
        } else if ($redirect === 'close') {
            $resultRedirect->setPath('*/*/');
        }
        return $resultRedirect;
    }


    /**
     * @param $data
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    protected function prepareDataImage($data)
    {
        if (isset($data['image'][0]['name']) && isset($data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
            $uploader = $this->imageUploader;
            $uploader->setBaseTmpPath('bannersImage/tmp');
            $uploader->setBasePath('bannersImage');
            $uploader->moveFileFromTmp($data['image']);
        } elseif (isset($data['image'][0]['name']) && !isset($data['image'][0]['tmp_name'])) {
            $data['image'] = $data['image'][0]['name'];
        } else {
            $data['image'] = '';
        }

        return $data;
    }
}
