<?php

namespace IdeaInYou\ExtensionContentful\Controller\Adminhtml\Banners;

use IdeaInYou\ExtensionContentful\Model\BannerFactory;
use IdeaInYou\ExtensionContentful\Api\BannerRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Edit CMS block action.
 */
class Edit extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    protected PageFactory $resultPageFactory;
    private Registry $_coreRegistry;
    private BannerFactory $bannerFactory;
    private BannerRepositoryInterface $bannerRepository;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        BannerFactory $bannerFactory,
        BannerRepositoryInterface $bannerRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
        $this->bannerFactory = $bannerFactory;
        $this->bannerRepository = $bannerRepository;
    }

    /**
     * Edit CMS block
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');

        // 2. Initial checking
        if ($id) {
            try {
                $model = $this->bannerRepository->getById($id);
            } catch (NoSuchEntityException $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            $model = $this->bannerFactory->create();
        }

        $this->_coreRegistry->register('ideainyou_banners', $model);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('Banners: %1', $model->getId()) : __('New Banners'));
        return $resultPage;
    }
}
