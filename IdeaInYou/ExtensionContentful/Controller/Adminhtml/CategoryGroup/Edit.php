<?php

namespace IdeaInYou\ExtensionContentful\Controller\Adminhtml\CategoryGroup;

use IdeaInYou\ExtensionContentful\Model\CategoryGroupFactory;
use IdeaInYou\ExtensionContentful\Api\CategoryGroupRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpGetActionInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Edit CMS block action.
 */
class Edit extends \Magento\Backend\App\Action implements HttpGetActionInterface
{
    protected PageFactory $resultPageFactory;
    private Registry $_coreRegistry;
    private CategoryGroupFactory $categoryGroupFactory;
    private CategoryGroupRepositoryInterface $categoryGroupRepository;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
        Context $context,
        Registry $coreRegistry,
        PageFactory $resultPageFactory,
        CategoryGroupFactory $categoryGroupFactory,
        CategoryGroupRepositoryInterface $categoryGroupRepository
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreRegistry = $coreRegistry;
        parent::__construct($context);
        $this->categoryGroupFactory = $categoryGroupFactory;
        $this->categoryGroupRepository = $categoryGroupRepository;
    }

    /**
     * Edit CMS block
     *
     * @return \Magento\Framework\Controller\ResultInterface
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function execute()
    {
        // 1. Get ID and create model
        $id = $this->getRequest()->getParam('id');

        // 2. Initial checking
        if ($id) {
            try {
                $model = $this->categoryGroupRepository->getById($id);
            } catch (NoSuchEntityException $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
                $resultRedirect = $this->resultRedirectFactory->create();
                return $resultRedirect->setPath('*/*/');
            }
        } else {
            $model = $this->categoryGroupFactory->create();
        }

        $this->_coreRegistry->register('ideainyou_categorygroup', $model);

        // 5. Build edit form
        /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
        $resultPage = $this->resultPageFactory->create();
        $resultPage->getConfig()->getTitle()->prepend($model->getId() ? __('CategoryGroup: %1', $model->getId()) : __('New CategoryGroup'));
        return $resultPage;
    }
}
