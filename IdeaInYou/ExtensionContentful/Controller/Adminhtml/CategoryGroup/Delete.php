<?php

namespace IdeaInYou\ExtensionContentful\Controller\Adminhtml\CategoryGroup;

use IdeaInYou\ExtensionContentful\Api\CategoryGroupRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class MassDelete
 */
class Delete extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    private CategoryGroupRepositoryInterface $categoryGroupRepository;

    /**
     * @param Context $context
     * @param CategoryGroupRepositoryInterface $categoryGroupRepository
     */
    public function __construct(
        Context $context,
        CategoryGroupRepositoryInterface $categoryGroupRepository
    ) {
        parent::__construct($context);
        $this->categoryGroupRepository = $categoryGroupRepository;
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $categoryGroupId = $this->getRequest()->getParam('id');

        if ($categoryGroupId) {
            try {
                $this->categoryGroupRepository->deleteById($categoryGroupId);
                $this->messageManager->addSuccessMessage(__('CategoryGroup (ID: %1) have been deleted.', $categoryGroupId));
            } catch (\Exception $exception) {
                $this->messageManager->addErrorMessage($exception->getMessage());
            }
        }

        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
