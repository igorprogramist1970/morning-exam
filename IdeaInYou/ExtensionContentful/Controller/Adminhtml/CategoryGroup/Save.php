<?php

namespace IdeaInYou\ExtensionContentful\Controller\Adminhtml\CategoryGroup;

use IdeaInYou\ExtensionContentful\Model\CategoryGroupFactory;
use IdeaInYou\ExtensionContentful\Api\CategoryGroupRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Registry;

/**
 * Save CMS block action.
 */
class Save extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;
    private Registry $_coreRegistry;
    /**
     * @var CategoryGroupFactory|mixed
     */
    private $categoryGroupFactory;
    /**
     * @var CategoryGroupRepositoryInterface|mixed
     */
    private $categoryGroupRepository;

    /**
     * @param Context $context
     * @param Registry $coreRegistry
     * @param DataPersistorInterface $dataPersistor
     * @param CategoryGroupFactory|null $categoryGroupFactory
     * @param CategoryGroupRepositoryInterface|null $categoryGroupRepository
     */
    public function __construct(
        Context                          $context,
        Registry                         $coreRegistry,
        DataPersistorInterface           $dataPersistor,
        CategoryGroupFactory             $categoryGroupFactory = null,
        CategoryGroupRepositoryInterface $categoryGroupRepository = null
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->categoryGroupFactory = $categoryGroupFactory
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(CategoryGroupFactory::class);
        $this->categoryGroupRepository = $categoryGroupRepository
            ?: \Magento\Framework\App\ObjectManager::getInstance()->get(CategoryGroupRepositoryInterface::class);
        parent::__construct($context);
        $this->_coreRegistry = $coreRegistry;
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $data['store_id'] = implode(',',$data['store_id']);
        if ($data) {
            if (empty($data['entity_id'])) {
                $data['entity_id'] = null;
            }

            /** @var \Magento\Cms\Model\Block $model */
            $model = $this->categoryGroupFactory->create();

            $id = $this->getRequest()->getParam('id');
            if ($id) {
                try {
                    $model = $this->categoryGroupRepository->getById($id);
                } catch (\Exception $e) {
                    $this->messageManager->addErrorMessage(__('This categoryGroup no longer exists.'));
                    return $resultRedirect->setPath('*/*/');
                }
            }

            $model->setData($data);

            try {
                $this->categoryGroupRepository->save($model);
                $this->messageManager->addSuccessMessage(__('You saved the categoryGroup.'));
                $this->dataPersistor->clear('ideainyou_categorygroup');
                return $this->processBlockReturn($model, $data, $resultRedirect);
            } catch (\Exception $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            }

            $this->dataPersistor->set('ideainyou_categorygroup', $data);
            return $resultRedirect->setPath('*/*/edit', ['id' => $id]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    /**
     * Process and set the block return
     *
     * @param \Magento\Cms\Model\Block $model
     * @param array $data
     * @param \Magento\Framework\Controller\ResultInterface $resultRedirect
     * @return \Magento\Framework\Controller\ResultInterface
     */
    private function processBlockReturn($model, $data, $resultRedirect)
    {
        $redirect = $data['back'] ?? 'close';

        if ($redirect === 'continue') {
            $resultRedirect->setPath('*/*/edit', ['id' => $model->getId()]);
        } else if ($redirect === 'close') {
            $resultRedirect->setPath('*/*/');
        }
        return $resultRedirect;
    }
}
