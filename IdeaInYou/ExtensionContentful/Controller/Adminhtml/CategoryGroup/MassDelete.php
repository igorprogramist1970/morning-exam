<?php

namespace IdeaInYou\ExtensionContentful\Controller\Adminhtml\CategoryGroup;

use IdeaInYou\ExtensionContentful\Model\ResourceModel\CategoryGroup\CollectionFactory;
use IdeaInYou\ExtensionContentful\Api\CategoryGroupRepositoryInterface;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Ui\Component\MassAction\Filter;

/**
 * Class MassDelete
 */
class MassDelete extends \Magento\Backend\App\Action implements HttpPostActionInterface
{
    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    /**
     * @var CategoryGroupRepositoryInterface
     */
    private CategoryGroupRepositoryInterface $categoryGroupRepository;

    /**
     * @param Context $context
     * @param Filter $filter
     * @param CollectionFactory $collectionFactory
     * @param CategoryGroupRepositoryInterface $categoryGroupRepository
     */
    public function __construct(
        Context                          $context,
        Filter                           $filter,
        CollectionFactory                $collectionFactory,
        CategoryGroupRepositoryInterface $categoryGroupRepository
    ) {
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        parent::__construct($context);
        $this->categoryGroupRepository = $categoryGroupRepository;
    }

    /**
     * Execute action
     *
     * @return \Magento\Backend\Model\View\Result\Redirect
     * @throws \Magento\Framework\Exception\LocalizedException|\Exception
     */
    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());
        $collectionSize = $collection->getSize();
        try {
            foreach ($collection as $categoryGroup) {
                $this->categoryGroupRepository->delete($categoryGroup);
            }
            $this->messageManager->addSuccessMessage(__('A total of %1 categoryGroup(s) have been deleted.', $collectionSize));
        } catch (\Exception $exception) {
            $this->messageManager->addErrorMessage($exception->getMessage());
        }


        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);

        return $resultRedirect->setPath('*/*/');
    }
}
